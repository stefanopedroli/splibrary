# SPLibrary

This Library is only for projects:
## Travaj





# AlertManager
### You can display custom alert.
When you import the SPLibrary you need to implement a xib named "AlertView".xib. Start from an example in one app. 
Than you can customize your alert UI and use it.
### How to use?
You can use custom alert passing from AlertManager class. Here some example:

`
AlertManager.normal.confirm(title: "Anntention", message: "Are you sure you want make logout?", action: .init(title: "Logout", action: { (action) in
    // logout()
})).show()
`
Don't forget  `.show()` at the end for showing alert.
You can also change some properties like background of alert by changing  `customBackgroundAlertColor`
### Others custom alert?
If you want to implement others alert you need to create your classes and relatives xib that need to implement protocol  `Alert`  or `Advise`.
Add your custom xib under  `AlertManager` class for use it from there.
Let's see another example of new custom alert:
`
AlertManager.pulseAlert.ok(type: .success, title: "Data updated!", message: "Your profile data has been succesfully updated") { (alert) in
 // action here   
}.show()
`
