import XCTest

import SPLibraryTests

var tests = [XCTestCaseEntry]()
tests += SPLibraryTests.allTests()
XCTMain(tests)
