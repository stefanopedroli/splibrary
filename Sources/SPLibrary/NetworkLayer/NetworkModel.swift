//
//  NetworkModel.swift
//  NetworkLayer Stefano P
//
//  Created by Stefano on 29/08/2019.
//  Copyright © 2019 Stefano. All rights reserved.
//

import Foundation
import Alamofire

public typealias HTTPMethodRequest = HTTPMethod

public protocol API {
    associatedtype PathParameters       // parameters you may have to pass on the path
    associatedtype Input: Encodable     // the body/input/params
    associatedtype Output: Decodable    // the output
    
    static func path(_ input: PathParameters?) -> String
    static var mockName: String { get }
    static var mockEnable: Bool { get }
    static var requiresPayloadWrapping: Bool { get }
    static var method: HTTPMethodRequest { get }
    static var authType: APIAuthType { get }
    static var pathType: PathType { get }
}

public extension API {
    static var mockName: String { return Self.path(nil).components(separatedBy: "/").last ?? String() }
    static var mockEnable: Bool { return false }
    static var requiresPayloadWrapping: Bool { return false }
    static var pathType: PathType { return .relative }
    
    static func request(input: Input, pathParams: PathParameters? = nil) -> NetworkLayer.Request<Input, Output> {
        return NetworkLayer.Request(input: input,
                                    wrappingPayload: Self.requiresPayloadWrapping,
                                    path: Self.path(pathParams),
                                    pathType: Self.pathType,
                                    mockName: Self.mockName,
                                    mockEnable: Self.mockEnable,
                                    method: Self.method,
                                    authType: Self.authType,
                                    isAutenticationSetter: (Self.self as? AutenticationSetter.Type) != nil)
    }
}

/// - description: This protocol indicates that the class that implement self is an autenticated API, so after this you are logged and Session have accessToken
public protocol AutenticationSetter {}

public extension NetworkLayer {
    
    struct Request<Input: Encodable, Output: Decodable> {
        let input: Input
        let wrappingPayload: Bool
        let path: String
        let pathType: PathType
        let mockName: String
        let mockEnable: Bool
        let method: HTTPMethod
        let authType: APIAuthType
        let isAutenticationSetter: Bool
    }
    
    enum Result<Output, Error> {
        case success(Output)
        case failure(Error)
    }
    
    enum Environment {
        case system
        case production
        
        static var current: Environment {
            #if DEBUG
            return .system
            #else
            return .production
            #endif
        }
        
        static var updateRequired: Bool = false
    }
}

public extension NetworkLayer.Environment {

    func apiURL() throws -> URL {
        switch self {
//        case .staging:
//            if let endpoint = SPLibrary.configurations.networkConfiguration.endpointStaging, let url = URL(string: endpoint) {
//                return url
//            } else {
//                throw Network.Error.endpointError
//            }
        case .system:
            if let endpoint = SPLibrary.configurations.networkConfiguration.endpointDebug, let url = URL(string: endpoint) {
                return url
            } else {
                throw Network.Error.endpointError
            }
        case .production:
            if let endpoint = SPLibrary.configurations.networkConfiguration.endpointRelease, let url = URL(string: endpoint) {
                return url
            } else {
                throw Network.Error.endpointError
            }
        }
    }
}

public enum APIAuthType {
    case `public`, authenticated, temporary
}

public protocol Params: class {
    static func nullable() -> Params
}

public enum PathType {
    /// - description: You're implementing a path that is called without adding endpoint
    case absolute
    /// - description: You're implementing a path that is called adding endpoint
    case relative
}

public protocol APIEntity {
    var id: String { get set }
    var created_at: DateSince1970? { get set }
    var updated_at: DateSince1970? { get set }
}

public protocol OptionalAPIEntity {
    var id: String? { get set }
    var created_at: DateSince1970? { get set }
    var updated_at: DateSince1970? { get set }
}

public extension OptionalAPIEntity {
    var forcedId: String { return self.id! }
}
