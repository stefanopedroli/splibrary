//
//  Logger.swift
//  NetworkLayer Stefano P
//
//  Created by Stefano on 02/09/2019.
//  Copyright © 2019 Stefano. All rights reserved.
//

import Foundation

public func print(_ items: Any..., separator: String = "\n", terminator: String = String()) {
    #if DEBUG
    // print only if is debug mode
    Swift.print(items
        .map { item in String(describing: item) }
        .joined(separator: separator)
        .appending(terminator))
    #endif
}

#if RELEASE
#else

let splibrary_error_header: String = "\(EmojiService.warning) [SPLibrary error]:"

public func printError(_ message: String) {
    print(splibrary_error_header + " \(message)")
}

func printRequest(url: URL, json: String) {
    print("\(EmojiService.request) \(url) \(DateFormatter.checkTimeFormatter.string(from: Date()))")
    let data = json.data(using: .utf8)?.prettyPrintedJSONString ?? json
    print("REQUEST:\n\(data)")
}

func printResponse(url: URL, json: Data, type: Network.ExitCode = .ok) {
    print("\(type.emojiString) \(url)")
    print("RESPONSE:\n\(json.prettyPrintedJSONString)")
}

func printResponse(url: URL, error: Error) {
    print("\(Network.ExitCode.ko.emojiString) \(url)")
    print("\(EmojiService.warning) \(error)")
}

func printResponse(url: URL, error: Network.Error) {
    print("\(Network.ExitCode.ko.emojiString) \(url)")
    if !error.formattedDescription.isEmpty {
        print(error.formattedDescription)
    } else {
        print(error.emoji + "\n" + "\n\tmessage: \(error.message),\n\tstatusCode: \(error.statusCode?.description ?? "nil")")
    }
}

public extension Encodable {
    var formattedDescription: String {
        do {
            let data = try JSONEncoder().encode(self)
            let string = data.prettyPrintedJSONString
            return string
        } catch {
            print("\(EmojiService.warning) \(error)")
            return ""
        }
    }
}

public extension Data {
    var prettyPrintedJSONString: String { /// NSString gives us a nice sanitized debugDescription
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
            let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
            let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else {
                return String(data: self, encoding: .utf8)?.replacingOccurrences(of: "\\/", with: "/") ?? String() }
        return String(prettyPrintedString).replacingOccurrences(of: "\\/", with: "/")
    }
}

#endif

extension DateFormatter {
    static let checkTimeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        formatter.locale = Locale(identifier: "it_IT")
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        return formatter
    }()
}
