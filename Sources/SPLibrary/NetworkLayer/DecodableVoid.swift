// Created by Stefano Pedroli on 30/11/2019.

import Foundation

public struct DecodableVoid: Decodable {
    public init() {}
    public init(from decoder: Decoder) throws {}
}

public typealias VoidHandler = () -> Swift.Void
public typealias ErrorHandler = (_ error: Swift.Error) -> Swift.Void

