//
//  EmojiService.swift
//  NetworkLayer Stefano P
//
//  Created by Stefano on 02/09/2019.
//  Copyright © 2019 Stefano. All rights reserved.
//

import Foundation

public final class EmojiService {
    
    static var request: String { return "⬆️ 📡" }
    static var ok: String { return Network.ExitCode.ok.emojiString }
    static var ko: String { return Network.ExitCode.ko.emojiString }
    static var demo: String { return "🕹" }
    static var demoOK: String { return "🕹 ✅" }
    static var demoKO: String { return "🕹 ❌" }
    
    static var deviceToken: String { return "📱" }
    static var warning: String { return "⚠️" }
}

public extension Network.ExitCode {
    var emojiString: String {
        switch self {
        case .ok: return "⬇️ ✅"
        case .ko: return "⬇️ ❌"
        }
    }
}

public extension Network.Error {
    var emoji: String {
        switch type {
        case .noConnectivity: return "\(EmojiService.warning) 🔌"
        default: return EmojiService.warning
        }
    }
}
