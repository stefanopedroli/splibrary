//
//  NetworkError.swift
//  NetworkLayer Stefano P
//
//  Created by Stefano on 29/08/2019.
//  Copyright © 2019 Stefano. All rights reserved.
//

import Foundation
import Alamofire

public extension NetworkLayer {
    func handleError(for response: DataResponse<Any>, error: Error) -> Network.Error {
        let statusCode = response.response?.statusCode ?? 0
        if error.isNoConnectivity { return Network.Error.noConnectivity }
        let parsingError = Network.Error(statusCode: statusCode)
        return parsingError
    }
    
//    // call this method for automatically do some default actions for specific errors
//    func loadError(error: Network.Error) {
//        self.logoutIfNeeded(error: error)
//    }
//    func loadError<Output>(results: Result<Output, Network.Error>) {
//        switch results {
//        case .failure(let resultingError):
//            self.loadError(error: resultingError)
//        default:
//            break
//        }
//    }
}

public extension Network {
    
    struct Error: Swift.Error, Codable {
        
        public let message: String
        public var statusCode: Int? {
            didSet {
                guard let code = self.statusCode else { return }
                type = ApiError(rawValue: code) ?? (isBackendError ? .custom : .none)
            }
        }
        /// - description: A describing type of statusCode
        public var type: ApiError = .none
        /// - description: Determinate if is a succesfully decoded error from backend.
        /// - warning: If is *false* means that is locally created only from statusCode information or something else
        public var isBackendError: Bool = false
        
        public var exitCode: ExitCode {
            switch statusCode {
            case .some(200): return .ok
            default: return .ko
            }
        }
        
        var localizedDescription: String {
            return type.localizedDescription
        }
        
        public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: Keys.self)
            var _messages: [String] = []
            if let messages = try? container.decode(Array<String>.self, forKey: .errors) {
                _messages = messages
            } else if let object = try? container.decode(Dictionary<String, String>.self, forKey: .errors) {
                for element in object {
                    _messages.append(element.value)
                }
            } else {
                throw Network.ApiError.parsingDataError
            }
            message = _messages.joined(separator: "\n")
            type = .custom
            isBackendError = true
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: Keys.self)
            try container.encode(message, forKey: .message)
            try container.encodeIfPresent(statusCode, forKey: .statusCode)
            try container.encodeIfPresent(String(describing: type), forKey: .type)
        }
        
        public init(statusCode code: Int) {
            self.statusCode = code
            self.type = ApiError.init(rawValue: code) ?? .none
            self.message = type.localizedDescription
        }
        
        public init(message: String, statusCode code: Int? = nil) {
            self.message = message
            self.type = .custom
            self.statusCode = code
        }
        
        internal init(_ type: ApiError) {
            self.type = type
            self.message = type.localizedDescription
            self.statusCode = type.rawValue
        }
        
        public enum Keys: String, CodingKey { case errors, message, statusCode, type }
    }
    
    enum ApiError: Int, Swift.Error, Codable {
        
        case invalidExitCode
        case parsingDataError
        case headerError
        case mockError
        case payloadError
        case unauthorized = 401
        case wrongPassword = 403
        case notFoundResource = 404
        case internalServerError = 500
        case noConnectivity
        case endpointError
        case pathError
        case custom
        case none
        
        var localizedDescription: String {
            switch self {
            case .invalidExitCode: return "Invalid exit code"
            case .parsingDataError: return "PARSING_DATA_ERROR".localizedInternal
            case .headerError: return "Invalid Header"
            case .mockError: return "No mock file is available. Please cheeck the Mocks.xcassets catalog."
            case .payloadError: return "WRAPPING_PAYLOAD_ERROR".localizedInternal
            case .noConnectivity: return "NO_CONNECTION_DESCRIPTION".localizedInternal
            case .unauthorized: return "NO_CONNECTION_DESCRIPTION".localizedInternal
            case .wrongPassword: return "NOT_AUTHORIZED_DESCRIPTION".localizedInternal
            case .notFoundResource: return "RESOURCES_NOT_FOUND_DESCRIPTION".localizedInternal
            case .pathError: return "PATH_ERROR".localizedInternal
            case .endpointError:
                #if RELEASE
                return "You not configure enpoint for release"
                #elseif STAGING
                return "You not configure enpoint for staging"
                #else
                return "You not configure enpoint for debug"
                #endif
            default: return "GENERIC_ERROR_DESCRIPTION".localizedInternal
            }
        }
    }
}

public extension Network.Error {
    static var invalidExitCode: Network.Error { return .init(.invalidExitCode) }
    static var parsingDataError: Network.Error { return .init(.parsingDataError) }
    static var mockError: Network.Error { return .init(.mockError) }
    static var payloadError: Network.Error { return .init(.payloadError) }
    static var noConnectivity: Network.Error { return .init(.noConnectivity) }
    static var generic: Network.Error { return .init(.none) }
    static var endpointError: Network.Error { return .init(.endpointError) }
    static var pathError: Network.Error { return .init(.pathError)}
    static var headerError: Network.Error { return .init(.headerError) }
}

public extension Error {
    var isNoConnectivity: Bool {
        let error = self as NSError
        return error.domain == NSURLErrorDomain && (
            error.code == NSURLErrorNotConnectedToInternet ||
                error.code == NSURLErrorTimedOut
        )
    }
}

import UIKit
public extension UIViewController {
    func handleError(error: Network.Error) {
        if SPLibrary.configurations.networkConfiguration.automaticallyLogoutOnError &&
            error.statusCode == SPLibrary.configurations.networkConfiguration.logoutOnStatusCode {
            DispatchQueue.main.async {
                UIApplication.appCoordinator?.logout()
            }
        } else {
            self.alertErrorMessage(message: error.message)
        }
    }
    
    // if you whant only manage this error and not others
    func handleLogoutError(error: Network.Error, passed: VoidHandler) {
        if SPLibrary.configurations.networkConfiguration.automaticallyLogoutOnError &&
            error.statusCode == SPLibrary.configurations.networkConfiguration.logoutOnStatusCode {
            DispatchQueue.main.async {
                UIApplication.appCoordinator?.logout()
            }
        } else {
            passed()
        }
    }
}
