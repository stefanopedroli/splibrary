//
//  NetworkKey.swift
//  NetworkLayer Stefano P
//
//  Created by Stefano on 29/08/2019.
//  Copyright © 2019 Stefano. All rights reserved.
//

import Foundation

// All Environment Key 🗝🗝🗝

protocol KeyProtocol {
    static var id_key: String { get }
}

enum EnvironmentKey {
    case id_key
}

extension EnvironmentKey {
    
    private struct ProductionKey: KeyProtocol {
        static var id_key: String {
            return String()
        }
    }
    
    private struct SystemKey: KeyProtocol {
        static var id_key: String {
            return String()
        }
    }
    
    private struct UpdateRequiredKey: KeyProtocol {
        static var id_key: String {
            return String()
        }
    }
    
    func key(environment: NetworkLayer.Environment, upgradeRequired: Bool) -> String {
        switch environment {
            
        case .production:
            switch self {
            case .id_key: return ProductionKey.id_key
            }
            
        case .system:
            
            #if DEBUG
            if upgradeRequired {
                switch self {
                case .id_key: return UpdateRequiredKey.id_key
                }
            }
            #endif
            
            switch self {
            case .id_key: return SystemKey.id_key
            }
        }
    }
}

extension NetworkLayer.Environment {
    private func getKey(for type: EnvironmentKey) -> String {
        return type.key(environment: self, upgradeRequired: NetworkLayer.Environment.updateRequired)
    }
    
    var key: String { //ID_KEY
        return getKey(for: .id_key)
    }
}
