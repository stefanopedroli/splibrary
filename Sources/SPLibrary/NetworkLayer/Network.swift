//
//  Network.swift
//  NetworkLayer Stefano P
//
//  Created by Stefano on 29/08/2019.
//  Copyright © 2019 Stefano. All rights reserved.
//

import Foundation
import Alamofire

public enum Network {

    public enum ExitCode: String, Decodable {
        case ok = "OK"
        case ko = "KO"
    }
    
    public struct Request<Payload: Encodable>: Encodable {
        let payload: Payload
    }
    
    public struct Response<Payload: Decodable>: Decodable {
        let exitCode: ExitCode?
        let payload: Payload
        
        public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: Keys.self)
            exitCode = try container.decode(ExitCode.self, forKey: .exitCode)
            payload = try container.decode(Payload.self, forKey: .payload)
            if exitCode != .ok { throw Network.Error.invalidExitCode }
        }
        
        enum Keys: String, CodingKey { case exitCode, payload }
    }
    
    public struct Configuration {
        public var endpointRelease: String?
        public var endpointDebug: String?
//        public var endpointStaging: String?
        
        /// Automatically make logout on specific statusCode, check "logoutOnStatusCode" to change the code
        public var automaticallyLogoutOnError: Bool = true
        /// Automatically set on 401
        public var logoutOnStatusCode: Int = 401
        
        public var allMockEnable: Bool = false
    }
}

public extension DataResponse {
    var exitCode: Network.ExitCode {
        if let statusCode = self.response?.statusCode, (200 ..< 299).contains(statusCode) {
            return .ok
        } else { return .ko }
    }
}
