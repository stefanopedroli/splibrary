//  Created by Stefano Pedroli on 10/06/2020.

import UIKit
import Alamofire

public struct MultipartFile {
    
    /// - description: encoding key for your file
    public let key: String
    public let name: String
    public let `extension`: String
    public let mimetype: String?
    public let data: Data
    
    public init(data: Data, key: String, name: String? = nil, extension suffix: URLExtensionType) {
        self.key = key
        self.name = name ?? DateFormatter.fileFormatter.string(from: Date())
        self.extension = suffix.rawValue
        self.mimetype = suffix.mimetype
        self.data = data
    }
    
    public init(data: Data, key: String, name: String? = nil, extension suffix: URLExtensionType, mimetype: String) {
        self.key = key
        self.name = name ?? DateFormatter.fileFormatter.string(from: Date())
        self.extension = suffix.rawValue
        self.mimetype = mimetype
        self.data = data
    }
    
    /// - warning: Is bettere use init with extension enum because is more safe
    public init(data: Data, key: String, name: String? = nil, extension suffix: String) {
        self.key = key
        self.name = name ?? DateFormatter.fileFormatter.string(from: Date())
        self.extension = suffix
        self.mimetype = suffix.mimeType()
        self.data = data
    }
    
    public init(data: Data, key: String, name: String? = nil, extension suffix: String, mimetype: String) {
        self.key = key
        self.name = name ?? DateFormatter.fileFormatter.string(from: Date())
        self.extension = suffix
        self.mimetype = mimetype
        self.data = data
    }
}

public extension NetworkLayer {
    
    func multipart<Input, Output>(files: [MultipartFile], request: Request<Input, Output>, completion: @escaping (Result<Output, Network.Error>) -> Void) {
        
        let completion: (Result<Output, Network.Error>) -> Void = { result in
            DispatchQueue.main.async { completion(result) }
        }
        
        let encodedInput: Data
        do {
            if request.wrappingPayload {
                encodedInput = try JSONEncoder().encode(Network.Request(payload: request.input))
            } else {
                encodedInput = try JSONEncoder().encode(request.input)
            }
        } catch {
            return completion(.failure(Network.Error.payloadError))
        }
        
        // Convert it to a string in order to be able to encrypt it
        guard let body = String(data: encodedInput, encoding: .utf8) else {
            return completion(.failure(Network.Error.parsingDataError))
        }
        
        guard let params = try? JSONSerialization.jsonObject(with: encodedInput, options: []) as? [String: Any] else {
            return completion(.failure(Network.Error.parsingDataError))
        }
        
        let _url: URL?
        switch request.pathType {
        case .relative:
            _url = try? environment.apiURL().appendingPathComponent(request.path)
        case .absolute:
            _url = URL(string: request.path)
        }
        guard let url = _url else { return completion(.failure(Network.Error.pathError)) }
        
        // setup header
        var headers: HTTPHeaders
            switch request.authType {
            case .temporary: headers = Header.temporary
            case .public: headers = Header.default
            case .authenticated: headers = Header.auth
        }
        
        headers["Content-type"] = "multipart/form-data"
        headers["Content-Disposition"] = "form-data"
        
        #if DEBUG
        printRequest(url: url, json: body)
        #endif
        
        // Check mock if are enable
        if NetworkLayer.isMockEnable || request.mockEnable {
            guard let asset = NSDataAsset(name: request.mockName) else {
                assertionFailure("No mock file is available for \(request.path). Please add \"\(request.mockName)\" using the Mocks.xcassets catalog.")
                print("No mock file is available for \(request.path). Please add \'\(request.mockName)\' using the Mocks.xcassets catalog.")
                return completion(.failure(Network.Error.mockError))
            }
            let mockData = asset.data
            self.parsingMock(url: url, request: request, response: mockData, completion: completion)
        } else {
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                // add files
                for file in files {
                    guard let mimetype = file.mimetype else { return print("⚠️ MISSING MIMETYPE for file named: \(file.name)")}
                    multipartFormData.append(file.data, withName: file.key, fileName: "\(file.name).\(file.extension)", mimeType: mimetype)
                }
                // add params
                for (key, value) in params {
                    if let content = (value as? String)?.data(using: .utf8) {
                        multipartFormData.append(content, withName: key)
                    } else {
                        print("⚠️ LOSS DATA: \"\(key)\":\(value)")
                    }
                }
                
            }, to: url, method: request.method, headers: headers) { (multipartFormDataEncodingResult) in
                switch multipartFormDataEncodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        // decode output here
                        guard let data = response.data else { return completion(.failure(Network.Error.parsingDataError))}
                        do {
                            let json = try JSONDecoder().decode(Output.self, from: data)
                            #if DEBUG
                            printResponse(url: url, json: data)
                            #endif
                            completion(.success(json))
                        } catch let decodingError {
                            self.parsingError(catch: decodingError, url: url, oauthResponse: nil, output: Output.self, alamofireResponse: response, completion: completion)
                        }
                    }
                case .failure(let encodingError):
                    printResponse(url: url, error: encodingError)
                    completion(.failure(Network.Error.parsingDataError))
                }
            }
        }
    }
}
