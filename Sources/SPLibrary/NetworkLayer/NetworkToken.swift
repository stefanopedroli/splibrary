//
//  NetworkToken.swift
//  NetworkLayer Stefano P
//
//  Created by Stefano on 29/08/2019.
//  Copyright © 2019 Stefano. All rights reserved.
//

import Foundation

public enum AuthenticationLevel {
    case `public`
    case `private`
}

public struct Token {
    
    let value: String
    let authentication: AuthenticationLevel
    
    private init(value: String, authentication: AuthenticationLevel) {
        self.value = value
        self.authentication = authentication
    }
    
    static var `public`: Token {
        return Token(value: String(), authentication: .public)
    }
    
    static func `private`(value: String) -> Token {
        return Token(value: value, authentication: .private)
    }
}

public protocol TokenContainer {
    var accessToken: String { get set }
    var client: String? { get set }
    var uid: String? { get set }
    var expiry: Date? { get set }
}

extension AuthenticationLevel: Equatable, Comparable {
    
    public static func <(lhs: AuthenticationLevel, rhs: AuthenticationLevel) -> Bool {
        switch (lhs, rhs) {
        case (.private, .public):
            return true
        case (.public, .private), (.public, .public), (.private, .private):
            return false
        }
    }
}

public protocol AuthTokenContainter {
    var authenticationToken: String { get }
}
