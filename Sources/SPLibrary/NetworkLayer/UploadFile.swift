//  Created by Stefano Pedroli on 20/04/2020.

import Foundation
import Alamofire

public protocol UploadProtocol: Encodable {
    var data: Data { get set }
    var mimetype: String { get set }
}

public protocol UploadAPI {
    associatedtype PathParameters       // parameters you may have to pass on the path
    associatedtype Input: UploadProtocol     // the body/input/params
    
    static func path(_ input: PathParameters) -> String
    static var method: HTTPMethod { get }
    static var authType: APIAuthType { get }
    static var pathType: PathType { get }
}

public extension UploadAPI {
    
    static var pathType: PathType { return .relative }
    
    static func request(input: Input, pathParams: PathParameters) -> NetworkLayer.RequestUpload<Input> {
        return NetworkLayer.RequestUpload(input: input,
                                    path: Self.path(pathParams),
                                    pathType: Self.pathType,
                                    method: Self.method,
                                    authType: Self.authType)
    }
}

public extension NetworkLayer {

    struct RequestUpload<Input: UploadProtocol> {
        let input: Input
        let path: String
        let pathType: PathType
        let method: HTTPMethod
        let authType: APIAuthType
    }

    enum ResultUpload<Error> {
        case success
        case failure(Error)
    }
}

public extension NetworkLayer {
    
    func upload<Input: UploadProtocol>(request: RequestUpload<Input>, completion: @escaping (ResultUpload<Network.Error>) -> Void) {
        
        let completion: (ResultUpload<Network.Error>) -> Void = { result in
            DispatchQueue.main.async { completion(result) }
        }
        
        let _url: URL?
        switch request.pathType {
        case .relative:
            _url = try? environment.apiURL().appendingPathComponent(request.path)
        case .absolute:
            _url = URL(string: request.path)
        }
        guard let url = _url else { return completion(.failure(Network.Error.pathError)) }
        
        // setup header
        var headers: HTTPHeaders
            switch request.authType {
            case .temporary: headers = Header.temporary
            case .public: headers = Header.default
            case .authenticated: headers = Header.auth
        }
        
        headers["Content-type"] = request.input.mimetype
        
        #if DEBUG
        printRequest(url: url, json: request.input.data.description)
        #endif
    
        Alamofire.upload(request.input.data, to: url, method: request.method, headers: headers).validate().responseJSON { (response) in
            
            guard let data = response.data else { return completion(.failure(Network.Error.parsingDataError))}

            if response.exitCode == .ok {
                #if DEBUG
                printResponse(url: url, json: data)
                #endif
                completion(.success)
            } else {
                if var serverError = try? JSONDecoder().decode(Network.Error.self, from: data) {
                    serverError.statusCode = response.response?.statusCode
                    #if DEBUG
                    printResponse(url: url, error: serverError)
                    #endif
                    completion(.failure(serverError))
                } else {
                    if let html = data.html { print("IS HTML PAGE ERROR \n\(html)") }
                    completion(.failure(Network.Error.parsingDataError))
                }
            }
        }
    }
}
