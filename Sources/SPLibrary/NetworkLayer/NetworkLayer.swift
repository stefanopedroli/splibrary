//
//  NetworkLayer.swift
//  NetworkLayer Stefano P
//
//  Created by Stefano on 29/08/2019.
//  Copyright © 2019 Stefano. All rights reserved.
//

import UIKit
import Alamofire

public final class NetworkLayer: NSObject {
    
    public static var isMockEnable: Bool = false
    
    public var environment: Environment
    private (set) var token: Token = .public
    private (set) var simulationToken: Token?
    
    private var authorization: String {
        switch token.authentication {
        case .public:  return String()
        case .private: return "Bearer"
        }
    }
    
    private let bundle: Bundle = .main
    private let defaults: UserDefaults = .standard
    private let processInfo: ProcessInfo = .processInfo
    
    init(environment: Environment = .current) {
        self.environment = environment
    }
    
    public func perform<Input, Output>(request: Request<Input, Output>, completion: @escaping (Result<Output, Network.Error>) -> Void) {
        
        let completion: (Result<Output, Network.Error>) -> Void = { result in
            // load some default actions relative for specific error
//            self.loadError(results: result)
            DispatchQueue.main.async { completion(result) }
        }
        
        let encodedInput: Data
        do {
            if request.wrappingPayload {
                encodedInput = try JSONEncoder().encode(Network.Request(payload: request.input))
            } else {
                encodedInput = try JSONEncoder().encode(request.input)
            }
        } catch {
            return completion(.failure(Network.Error.payloadError))
        }
        
        // Convert it to a string in order to be able to encrypt it
        guard let body = String(data: encodedInput, encoding: .utf8) else {
            return completion(.failure(Network.Error.parsingDataError))
        }
        
        guard let params = try? JSONSerialization.jsonObject(with: encodedInput, options: []) as? [String: Any] else {
            return completion(.failure(Network.Error.parsingDataError))
        }
        
        do {
            
            let _url: URL?
            switch request.pathType {
            case .relative:
                _url = try environment.apiURL().appendingPathComponent(request.path)
            case .absolute:
                _url = URL(string: request.path)
            }
            guard let url = _url else { return completion(.failure(Network.Error.pathError)) }
            
            // setup header
            let headers: HTTPHeaders
                switch request.authType {
                case .temporary: headers = Header.temporary
                case .public: headers = Header.default
                case .authenticated: headers = Header.auth
            }
            
            #if DEBUG
            printRequest(url: url, json: body)
            #endif
            
            // Check mock if are enable
            if NetworkLayer.isMockEnable || request.mockEnable {
                
                guard let asset = NSDataAsset(name: request.mockName) else {
                    assertionFailure("No mock file is available for \(request.path). Please add \"\(request.mockName)\" using the Mocks.xcassets catalog.")
                    print("No mock file is available for \(request.path). Please add \'\(request.mockName)\' using the Mocks.xcassets catalog.")
                    return completion(.failure(Network.Error.mockError))
                }
                let mockData = asset.data
                self.parsingMock(url: url, request: request, response: mockData, completion: completion)

            } else {
                
                // You make real backend call
                Alamofire.request(url, method: request.method, parameters: params, headers: headers).validate().responseJSON(completionHandler: { (response) in
                    
                    guard let data = response.data else { return completion(.failure(Network.Error.parsingDataError))}
                    
                    let oauthResponse: TokenResponse?
                    if let header = response.response?.allHeaderFields, let headerData = try? JSONSerialization.data(withJSONObject: header) {
                        oauthResponse = try? JSONDecoder().decode(TokenResponse.self, from: headerData)
                    } else { oauthResponse = nil }
                    do {
                        if response.exitCode == .ok {
                            // decoding Output<DecodableVoid> type for an .ko exitCode always succeed
                            // That's the reason of the prevously condition
                            let json = try JSONDecoder().decode(Output.self, from: data)
                            
                            if let authResponse = json as? TokenAuthenticationTemporary {
                                guard let token = authResponse.token, let uid = authResponse.uid, let client = authResponse.client else {
                                    return
                                }
                                Session.temporaryAuth = TemporaryAuthObject(token: token, uid: uid, client: client)
                            }
                            
                            if request.authType == .temporary { Session.temporaryAuth = nil }
                            
                            if let oauthResponse = oauthResponse {
                                 Session.saveAauthHeader(output: oauthResponse)
                            } else if request.isAutenticationSetter {
                                // The API give autentication information, and if you can't retrieve it
                                // you cannot continue using app
                                #if DEBUG
                                print(response.response?.allHeaderFields ?? headers)
                                #endif
                                return completion(.failure(Network.Error.headerError))
                            }
                    
                            #if DEBUG
                            printResponse(url: url, json: data)
                            #endif
                            completion(.success(json))
                        } else {
                            if var serverError = try? JSONDecoder().decode(Network.Error.self, from: data) {
                                serverError.statusCode = response.response?.statusCode
                                #if DEBUG
                                printResponse(url: url, error: serverError)
                                #endif
                                completion(.failure(serverError))
                            } else {
                                if let html = data.html { print("IS HTML PAGE ERROR \n\(html)") }
                                completion(.failure(Network.Error.parsingDataError))
                            }
                        }
                    } catch {
                        // case statusCode 204 where body is nil, not empty, is impossible to decoding
                        if data.isEmpty && ((Output.self as? DecodableVoid.Type) != nil) {
                            if let oauthResponse = oauthResponse {
                                 Session.saveAauthHeader(output: oauthResponse)
                            }
                            #if DEBUG
                            printResponse(url: url, json: data)
                            #endif
                            completion(.success(DecodableVoid() as! Output))
                        } else if var serverError = try? JSONDecoder().decode(Network.Error.self, from: data) {
                            serverError.statusCode = response.response?.statusCode
                            #if DEBUG
                            printResponse(url: url, error: serverError)
                            #endif
                            completion(.failure(serverError))
                        } else {
                            if let html = data.html {
                                print("IS HTML PAGE ERROR\n\(html)")
                            }
                            #if DEBUG
                            printResponse(url: url, error: error)
                            #endif
                            completion(.failure(self.handleError(for: response, error: error)))
                        }
                    }
                })
            }
        } catch let error {
            // You forgot to set your endpoint
            print("\(EmojiService.ko) \(error.localizedDescription)")
            completion(.failure(.endpointError))
        }
    }
    
    // is not public cause is automatically managed
    func clear() {
        self.token = .public
        Session.clear()
    }
}

// Logout
extension NetworkLayer {
//    func logoutIfNeeded(error: Network.Error) {
//        if SPLibrary.configurations.networkConfiguration.automaticallyLogoutOnError {
//            if error.statusCode == SPLibrary.configurations.networkConfiguration.logoutOnStatusCode {
//                DispatchQueue.main.async {
//                    UIApplication.appCoordinator?.logout()
//                }
//            }
//        }
//    }
    
    func parsingError<Output: Decodable>(catch error: Swift.Error, url: URL, oauthResponse: TokenResponse?, output: Output.Type, alamofireResponse: DataResponse<Any>, completion: @escaping (Result<Output, Network.Error>) -> Void) {
        guard let data = alamofireResponse.data else { return completion(.failure(Network.Error.parsingDataError))}
        // case statusCode 204 where body is nil, not empty, is impossible to decoding
        if data.isEmpty && ((Output.self as? DecodableVoid.Type) != nil) {
            if let oauthResponse = oauthResponse {
                 Session.saveAauthHeader(output: oauthResponse)
            }
            #if DEBUG
            printResponse(url: url, json: data)
            #endif
            completion(.success(DecodableVoid() as! Output))
        } else if var serverError = try? JSONDecoder().decode(Network.Error.self, from: data) {
            serverError.statusCode = alamofireResponse.response?.statusCode
            #if DEBUG
            printResponse(url: url, error: serverError)
            #endif
            completion(.failure(serverError))
        } else {
            if let html = data.html {
                print("IS HTML PAGE ERROR\n")
                print(html)
            }
            #if DEBUG
            printResponse(url: url, error: error)
            #endif
            completion(.failure(self.handleError(for: alamofireResponse, error: error)))
        }
    }
    
    func parsingMock<Input, Output>(url: URL, request: Request<Input, Output>, response: Data, completion: @escaping (Result<Output, Network.Error>) -> Void) {
        do {
            let json = try JSONDecoder().decode(Output.self, from: response)
            #if DEBUG
            printResponse(url: url, json: response)
            #endif
            completion(.success(json))
        } catch {
            // case statusCode 204 where body is nil, not empty, is impossible to decoding
            if response.isEmpty && ((Output.self as? DecodableVoid.Type) != nil) {
                #if DEBUG
                printResponse(url: url, json: response)
                #endif
                completion(.success(DecodableVoid() as! Output))
            } else if let serverError = try? JSONDecoder().decode(Network.Error.self, from: response) {
                #if DEBUG
                printResponse(url: url, error: serverError)
                #endif
                completion(.failure(serverError))
            } else {
                #if DEBUG
                printResponse(url: url, error: error)
                #endif
                completion(.failure(Network.Error.generic))
            }
        }
    }
}
