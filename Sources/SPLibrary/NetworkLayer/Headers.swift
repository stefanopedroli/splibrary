//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 13/01/2020.
//

import UIKit
import Alamofire

public struct TemporaryAuthObject: Codable {
    let token: String
    let uid: String
    let client: String
}

public class Header {
    public static var `default`: HTTPHeaders {
        var header: HTTPHeaders = .init()
        header["language-code"] = NSLocale.language
//        header["Content-Type"] = "text/plain; charset=utf-8"
        header["app-version"] = UIApplication.appMajorMinorVersionRelease
        return header
    }
    
    public static var auth: HTTPHeaders {
        var header: HTTPHeaders = Header.default
        header["token-type"] = "Bearer"
        header["access-token"] = Session.accessToken ?? ""
        header["client"] = Session.authClient ?? ""
        header["uid"] = Session.authUid ?? ""
        return header
    }
    
    public static var temporary: HTTPHeaders {
        var header: HTTPHeaders = Header.default
        header["access-token"] = Session.temporaryAuth?.token ?? ""
        header["client"] = Session.temporaryAuth?.client ?? ""
        header["uid"] = Session.temporaryAuth?.uid ?? ""
        return header
    }
}

public protocol TokenAuthenticationTemporary {
    var client: String? { set get }
    var token:  String? { set get }
    var uid:    String? { set get }
}

public struct TokenResponse: Codable, TokenContainer {
    public var accessToken: String
    public var client: String?
    public var uid: String?
    public var expiry: Date?
    
    enum Keys: String, CodingKey {
        case accessToken = "access-token"
        case client = "client"
        case uid = "uid"
        case expiry = "expiry"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Keys.self)
        accessToken = try container.decode(String.self, forKey: .accessToken)
        client = try? container.decodeIfPresent(String.self, forKey: .client)
        uid = try? container.decodeIfPresent(String.self, forKey: .uid)
        if let timeinterval = try? container.decodeIfPresent(TimeInterval.self, forKey: .expiry) {
            expiry = Date(timeIntervalSince1970: timeinterval)
        }
    }
}
