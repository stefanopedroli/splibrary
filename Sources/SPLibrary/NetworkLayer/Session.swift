//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 13/01/2020.
//

import Foundation

// Session is just a class to store Network informations and nothing else, is not public
public final class Session {
    
    private init() {}
//    private static let sharedDefaults = UserDefaults(suite) ?? UserDefaults.standard
//    private static var keychain: KeychainService = KeychainService(accessGroup: .accessGroup)

    public static var accessToken: String? {
        get { CacheManager.get(type: String.self, for: .accessToken) }
        set { CacheManager.set(object: newValue, for: .accessToken, shared: true) }
    }

    public static var authClient: String? {
        get { CacheManager.get(type: String.self, for: .authClient) }
        set { CacheManager.set(object: newValue, for: .authClient, shared: true) }
    }

    public static var authUid: String? {
        get { CacheManager.get(type: String.self, for: .authUid) }
        set { CacheManager.set(object: newValue, for: .authUid, shared: true) }
    }

    public static var authExpiry: Date? {
        get { CacheManager.get(type: Date.self, for: .authExpiry) }
        set { CacheManager.set(object: newValue, for: .authExpiry, shared: true) }
    }
    
    public static var temporaryAuth: TemporaryAuthObject? {
        get { return CacheManager.get(type: TemporaryAuthObject.self, for: .temporaryAuthObjKey) }
        set { CacheManager.set(object: newValue, for: .temporaryAuthObjKey) }
    }
    
    // is not public beacause is manage automatically
    static func clear() {
        accessToken = nil
        authClient = nil
        authUid = nil
        authExpiry = nil
        temporaryAuth = nil
    }
}

public extension Session {
    static func saveAauthHeader(output: TokenContainer) {
        Session.accessToken = output.accessToken
        Session.authUid = output.uid
        Session.authClient = output.client
        Session.authExpiry = output.expiry
    }
}

private extension String {
    static var accessToken: String { return #function }
    static var authClient: String { return #function }
    static var authUid: String { return #function }
    static var authExpiry: String { return #function }
    static var temporaryAuth: String { return #function }
    static var temporaryAuthObjKey: String { return #function }
}
