//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 15/01/21.
//

import UIKit

internal var gradientColorOne: CGColor = UIColor(white: 0.85, alpha: 1.0).cgColor
internal var gradientColorTwo: CGColor = UIColor(white: 0.95, alpha: 1.0).cgColor

public extension UIView {
    
    func shimmering() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.colors = [gradientColorOne, gradientColorTwo, gradientColorOne]
        gradientLayer.locations = [0.0, 0.5, 1.0]
        gradientLayer.name = "shimmer"
        self.layer.addSublayer(gradientLayer)
        let animation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = [-1.0, -0.5, 0.0]
        animation.toValue = [1.0, 1.5, 2.0]
        animation.repeatCount = .infinity
        animation.duration = 0.9
        gradientLayer.add(animation, forKey: "shimmer")
    }
    
    func removeShimmer() {
        guard let sublayers = self.layer.sublayers else { return }
        for layer in sublayers {
             if layer.name == "shimmer" {
                  layer.removeFromSuperlayer()
             }
         }
    }
}
