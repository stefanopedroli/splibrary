//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 16/11/2019.
//

import UIKit

public class SPColors: Colors {
    
    public var secondPaletteColors: Palette {
        return Palette(dark: mainSecondPaletteDark,
                       normal: mainSecondPaletteNormal,
                       medium: mainSecondPaletteMedium,
                       light: mainSecondPaletteLight,
                       extraLight: mainSecondPaletteExtraLight)
    }
    
    public init() {}
    
    // MARK: - Main Theme Palette
    public var mainPaletteDark: UIColor =               #colorLiteral(red: 1, green: 0.7647058824, blue: 0.3411764706, alpha: 1) // #FFC357 -> main theme
    public var mainPaletteMedium: UIColor =             #colorLiteral(red: 1, green: 0.8352941176, blue: 0.537254902, alpha: 1) // #FFD589
    public var mainPaletteLight: UIColor =              #colorLiteral(red: 1, green: 0.9058823529, blue: 0.7333333333, alpha: 1) // #FFE7BB
    public var mainPaletteExtraLight: UIColor =         #colorLiteral(red: 1, green: 0.9764705882, blue: 0.9333333333, alpha: 1) // #FFF9EE
    
    // MARK: - Second Main Theme Palette
    private var mainSecondPaletteDark: UIColor =         #colorLiteral(red: 0.003921568627, green: 0.4980392157, blue: 0.4784313725, alpha: 1) // #5BA3A0
    private var mainSecondPaletteNormal: UIColor =       #colorLiteral(red: 0.4470588235, green: 0.8, blue: 0.7843137255, alpha: 1) // #72CCC8 -> second main theme
    private var mainSecondPaletteMedium: UIColor =       #colorLiteral(red: 0.6117647059, green: 0.8588235294, blue: 0.8470588235, alpha: 1) // #9CDBD8
    private var mainSecondPaletteLight: UIColor =        #colorLiteral(red: 0.7764705882, green: 0.9176470588, blue: 0.9137254902, alpha: 1) // #C6EAE9
    private var mainSecondPaletteExtraLight: UIColor =   #colorLiteral(red: 0.9058823529, green: 0.9803921569, blue: 0.9529411765, alpha: 1) // #E7FAF3
    
    //MARK: - Principal Colors
    public var mainBlackDark: UIColor =                 #colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1568627451, alpha: 1) // #282828
    public var mainGray: UIColor =                      #colorLiteral(red: 0.4156862745, green: 0.4117647059, blue: 0.4078431373, alpha: 1) // #6A6968
    public var mainGrayLight: UIColor =                 #colorLiteral(red: 0.7294117647, green: 0.7176470588, blue: 0.7058823529, alpha: 1) // #BAB7B4
    public var mainGrayExtraLight: UIColor =            #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1) // #F0F0F0
    public var mainFakeWhite: UIColor =                 #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9803921569, alpha: 1) // #FAFAFA
    public var mainWhite: UIColor =                     #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) // #FFFFFF
    public var mainRedColor: UIColor =                  #colorLiteral(red: 0.9607843137, green: 0.3960784314, blue: 0.3568627451, alpha: 1) // #F5655B
    public var outcomePositive: UIColor =               #colorLiteral(red: 0.4470588235, green: 0.8, blue: 0.7843137255, alpha: 1) // #72CCC8
    public var outcomeNegative: UIColor =               #colorLiteral(red: 0.9607843137, green: 0.3960784314, blue: 0.3568627451, alpha: 1) // #F5655B
    public var mainPurple: UIColor =                    #colorLiteral(red: 0.3490196078, green: 0.3019607843, blue: 0.5098039216, alpha: 1) // #594D82 -> third main theme
    
    public var thirdTheme: UIColor =                    #colorLiteral(red: 0.09019607843, green: 0.8352941176, blue: 0.537254902, alpha: 1) // #17D589
    
    var secondGradientColor: UIColor = #colorLiteral(red: 1, green: 0.7647058824, blue: 0.3411764706, alpha: 1) // #FFC357
    var firstGradientColor: UIColor = #colorLiteral(red: 0.4470588235, green: 0.8, blue: 0.7843137255, alpha: 1) // #72CCC8
     
    public var gradients: [UIColor]? = [#colorLiteral(red: 0.4470588235, green: 0.8, blue: 0.7843137255, alpha: 1), #colorLiteral(red: 1, green: 0.7647058824, blue: 0.3411764706, alpha: 1)]
    public var mainNavigationBarColor: UIColor? =        #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) // #FFFFFF
}

public protocol Colors {
    var mainPaletteDark: UIColor { get }
    var mainPaletteMedium: UIColor { get }
    var mainPaletteLight: UIColor { get }
    var mainPaletteExtraLight: UIColor { get }
    
    var secondPaletteColors: Palette { get }
    
    var mainBlackDark: UIColor { get }
    var mainGray: UIColor { get }
    var mainGrayLight: UIColor { get }
    var mainGrayExtraLight: UIColor { get }
    var mainFakeWhite: UIColor { get }
    var mainWhite: UIColor { get }
    var mainRedColor: UIColor { get }
    var outcomePositive: UIColor { get }
    var outcomeNegative: UIColor { get }
    var mainPurple: UIColor { get }
    
    var gradients: [UIColor]? { get }
    var mainNavigationBarColor: UIColor? { get }
}

public extension UIColor {
    static var thirdsTheme: UIColor { return SPColors().thirdTheme }
}
