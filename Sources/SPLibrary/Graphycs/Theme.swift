//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 16/11/2019.
//

import UIKit

public enum AppTheme {
    case defaultTheme
}

public extension UIColor {
    
    private static var theme: AppTheme = .defaultTheme
    private static var colors: Colors {
        return SPLibrary.shared.colors
    }
    
    static func changeTheme(_ theme: AppTheme) {
        self.theme = theme
    }
    
    //MARK: - Main colors
    static var mainTheme: UIColor {
        switch theme {
        case .defaultTheme: return colors.mainPaletteDark
        }
    }
    static var mainThemeDark: UIColor {
        switch theme {
        case .defaultTheme: return colors.mainPaletteDark
        }
    }
    static var mainThemeMedium: UIColor {
        switch theme {
        case .defaultTheme: return colors.mainPaletteMedium
        }
    }
    static var mainThemeLight: UIColor {
        switch theme {
        case .defaultTheme: return colors.mainPaletteLight
        }
    }
    static var mainThemeExtraLight: UIColor {
        switch theme {
        case .defaultTheme: return colors.mainPaletteExtraLight
        }
    }
    
    // MARK: - Second Main Theme
    static var secondTheme = Palette(dark: colors.secondPaletteColors.dark,
                                     normal: colors.secondPaletteColors.normal,
                                     medium: colors.secondPaletteColors.medium,
                                     light: colors.secondPaletteColors.light,
                                     extraLight: colors.secondPaletteColors.extraLight)

    // MARK: - White and Dark
    static var mainThemeWhite: UIColor {
        switch theme {
        case .defaultTheme: return colors.mainWhite
        }
    }
    static var mainThemeBlack: UIColor {
        switch theme {
        case .defaultTheme: return colors.mainBlackDark
        }
    }
    static var mainThemeGray: UIColor {
        switch theme {
        case .defaultTheme: return colors.mainGray
        }
    }
    static var mainThemePurple: UIColor {
        switch theme {
        case .defaultTheme: return colors.mainPurple
        }
    }
       
       // MARK: - Red
       static var mainThemeRed: UIColor {
           switch theme {
           case .defaultTheme: return colors.mainRedColor
           }
       }
       
       //MARK: - Navigation Bar
       static var navigationBarColor: UIColor {
           switch theme {
           case .defaultTheme: return colors.mainNavigationBarColor ?? UIColor.mainThemeBlack
           }
       }
       
       // MARK: - Separators
       static var separatorColor: UIColor {
           return #colorLiteral(red: 0.7843137255, green: 0.7803921569, blue: 0.8, alpha: 1) // #c8c7cc
       }
       
       static var alertBackgroundColor: UIColor {
           return colors.mainBlackDark
       }
       
       // The color of text tint or icon set on a background of main theme color
       static var tintMainThemeColor: UIColor {
           return mainThemeWhite
       }

       // MARK: - Gradient Colors
       static var gradientColors: [UIColor] {
            return colors.gradients ?? [UIColor.mainTheme, UIColor.mainThemeMedium]
       }
}

// Palette
public extension UIColor {
    
    enum ColorHeavy {
        case dark, normal, medium, light, extraLight
    }
    
    // GRAY
    static func grayPalette(heavy: ColorHeavy) -> UIColor {
        switch heavy {
        case .dark: return UIColor.gray // da definire
        case .normal: return colors.mainGray
        case .medium: return colors.mainGrayLight
        case .light: return colors.mainGrayExtraLight
        case .extraLight: return colors.mainFakeWhite
        }
    }
    
    static func textPalette(heavy: ColorHeavy) -> UIColor {
        switch heavy {
        case .dark: return colors.mainBlackDark
        case .normal: return colors.mainBlackDark.withAlphaComponent(0.70)
        case .medium: return colors.mainBlackDark.withAlphaComponent(0.6)
        case .light: return colors.mainBlackDark.withAlphaComponent(0.5) // #949494
        case .extraLight: return colors.mainGrayExtraLight
        }
    }
}

public class Palette {
    public let dark: UIColor
    public let normal: UIColor
    public let medium: UIColor
    public let light: UIColor
    public let extraLight: UIColor
    
    public init(dark: UIColor, normal: UIColor, medium: UIColor, light: UIColor, extraLight: UIColor) {
        self.dark = dark
        self.normal = normal
        self.medium = medium
        self.light = light
        self.extraLight = extraLight
    }
}
