//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 16/11/2019.
//

import UIKit

public typealias GradientPoint = (x: CGPoint, y: CGPoint)

public enum GradientOrientation {
    case leftRight
    case rightLeft
    case topBottom
    case bottomTop
    case topLeftBottomRight
    case bottomRightTopLeft
    case topRightBottomLeft
    case bottomLeftTopRight
    
    public func draw() -> GradientPoint {
        switch self {
        case .leftRight:
            return (x: CGPoint(x: 0, y: 0.5), y: CGPoint(x: 1, y: 0.5))
        case .rightLeft:
            return (x: CGPoint(x: 1, y: 0.5), y: CGPoint(x: 0, y: 0.5))
        case .topBottom:
            return (x: CGPoint(x: 0.5, y: 0), y: CGPoint(x: 0.5, y: 1))
        case .bottomTop:
            return (x: CGPoint(x: 0.5, y: 1), y: CGPoint(x: 0.5, y: 0))
        case .topLeftBottomRight:
            return (x: CGPoint(x: 0, y: 0), y: CGPoint(x: 1, y: 1))
        case .bottomRightTopLeft:
            return (x: CGPoint(x: 1, y: 1), y: CGPoint(x: 0, y: 0))
        case .topRightBottomLeft:
            return (x: CGPoint(x: 1, y: 0), y: CGPoint(x: 0, y: 1))
        case .bottomLeftTopRight:
            return (x: CGPoint(x: 0, y: 1), y: CGPoint(x: 1, y: 0))
        }
    }
}

public extension UIView {
    
    func applyGradient(withColours colours: [UIColor], locations: [NSNumber]? = nil) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.name = "gradientLayer"
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func applyGradient(withColours colours: [UIColor], gradientOrientation orientation: GradientOrientation) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.name = "gradientLayer"
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = orientation.draw().x
        gradient.endPoint = orientation.draw().y
        gradient.frame = self.layer.bounds
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    @discardableResult
    func applyGradient(gradientStyle: GradientStyle = .defaultStyle, gradientOrientation orientation: GradientOrientation = .topLeftBottomRight, customShape: CAShapeLayer? = nil) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.name = "gradientLayer"
        gradient.frame = self.bounds
        gradient.colors = gradientStyle.colors.map { $0.cgColor }
        gradient.zPosition = -1
        gradient.startPoint = orientation.draw().x
        gradient.endPoint = orientation.draw().y
        gradient.mask = customShape
        self.layer.insertSublayer(gradient, at: 0)
        self.clipsToBounds = true
        return gradient
    }
    
    func applyDiagonalGradientShape(gradientStyle: GradientStyle = .defaultStyle, gradientOrientation gradienOrientation: GradientOrientation = .topLeftBottomRight, minPercentage: CGFloat, maxPercentage: CGFloat, diagonalOrientation: DiagonalMaskOrientation = .leftToRight, customShape: CAShapeLayer? = nil) {
        
        let layerHeight = self.layer.frame.height
        let layerWidth = self.layer.frame.width
        
        var maskMinHeight: CGFloat = layerHeight
        var maskMaxHeight: CGFloat = layerHeight
        // Create Path
        let bezierPath = UIBezierPath()
        
        if minPercentage >= 0 && minPercentage <= 1 {
            maskMinHeight = layerHeight * minPercentage
        }
        
        if maxPercentage >= 0 && maxPercentage <= 1 {
            maskMaxHeight = layerHeight * maxPercentage
        }
        
        //  Points
        //if orientation == .bottom {
        let pointA = CGPoint(x: 0, y: 0)
        let pointB = CGPoint(x: layerWidth, y: 0)
        let pointC = CGPoint(x: layerWidth, y: maskMaxHeight)
        let pointD = CGPoint(x: 0, y: maskMinHeight)
        
        // Draw the path
        bezierPath.move(to: pointA)
        bezierPath.addLine(to: pointB)
        bezierPath.addLine(to: pointC)
        bezierPath.addLine(to: pointD)
        bezierPath.close()
        // Mask to Path
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = bezierPath.cgPath
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.name = "gradientLayer"
        gradient.frame = self.bounds
        gradient.colors = gradientStyle.colors.map { $0.cgColor }
        gradient.startPoint = gradienOrientation.draw().x
        gradient.endPoint = gradienOrientation.draw().y
        
        layer.insertSublayer(gradient, at: 0)
        self.layer.mask = customShape ?? shapeLayer
    }
}

public extension UIView {
    
    @discardableResult
    func blurred(style: UIBlurEffect.Style = .light) -> UIVisualEffectView {
        let blurEffect = UIBlurEffect(style: style)
        let blurBackground = UIVisualEffectView(effect: blurEffect)
//        insertSubview(blurBackground, at: 0)
        addSubview(blurBackground)
        blurBackground.translatesAutoresizingMaskIntoConstraints = false
        blurBackground.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        blurBackground.topAnchor.constraint(equalTo: topAnchor).isActive = true
        blurBackground.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        blurBackground.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        return blurBackground
    }
    
    /// Remove UIBlurEffect from UIView
    func removeBlurEffect() {
        let blurredEffectViews = self.subviews.filter{$0 is UIVisualEffectView}
        blurredEffectViews.forEach{ blurView in
            blurView.removeFromSuperview()
        }
    }
    
    func removeGradient() {
        if let sublayers = layer.sublayers {
            for layer in sublayers where layer.name == "gradientLayer" {
                layer.removeFromSuperlayer()
            }
        }
    }
    
    func removeShadow(name: String = "shadowLayer") {
        if let sublayers = layer.sublayers {
            for layer in sublayers where layer.name == name {
                layer.removeFromSuperlayer()
            }
        }
    }
}

}

public enum GradientStyle {
    case defaultStyle
    
    public var colors: [UIColor] {
        switch self {
        case .defaultStyle:
            return UIColor.gradientColors
        }
    }
    
    public var tintColor: UIColor {
        switch self {
        case .defaultStyle:
            return UIColor.mainThemeBlack
        }
    }
    
    public var first: UIColor { return self.colors[0] }
    public var second: UIColor { return self.colors[1] }
}

public enum CornerRadiusStyle {
    case half
    case softCorner
    case heavyCorner
    case custom(value: CGFloat)
    
    public func value(height: CGFloat? = nil) -> CGFloat {
        switch self {
        case .half:
            guard let height = height else {
                return CGFloat.defauldCornerRadius
            }
            return height/2
        case .softCorner:
            return CGFloat.defauldCornerRadius
        case .heavyCorner:
            return CGFloat.heavyCornerRadius
        case .custom(let value):
            return value
        }
    }
}

enum ColorType {
    case gradient, color(value: UIColor)
}

public extension CGFloat {
    static var defauldCornerRadius: CGFloat { return SPLibrary.configurations.defauldCornerRadius }
    static var heavyCornerRadius: CGFloat { return SPLibrary.configurations.heavyCornerRadius }
    static var titleFontSize: CGFloat { return SPLibrary.configurations.titleFontSize }
    static var defaulfFontSize: CGFloat { return SPLibrary.configurations.defaulfFontSize }
    static var smallfFontSize: CGFloat { return SPLibrary.configurations.smallfFontSize }
    static var menuButtonSize: CGFloat { return SPLibrary.configurations.menuButtonSize }
}

public extension CGSize {
    static var one: CGSize { return CGSize(width: 1, height: 1) }
}

public extension UIFont {
    static var defaultFont: UIFont {
        return UIFont.libraryFont(ofSize: CGFloat.defaulfFontSize, weight: .regular)
    }
}

public extension UICollectionView {
    func setDefaultLayout(direction: ScrollDirection, itemForRow: CGFloat = 2, height: CGFloat? = nil) {
        let layout = UICollectionViewFlowLayout()
        let width = self.frame.width
        let height = height ?? (self.frame.height)
        layout.scrollDirection = direction
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        layout.itemSize = CGSize(width: (width / itemForRow) - 10, height: height)
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 5
        self.collectionViewLayout = layout
    }
}

public extension UIView {
    func rounded(style: CornerRadiusStyle = .softCorner, withShadow: Bool = false) {
        if withShadow {
            if let cell = self as? UICollectionViewCell {
                cell.setShadowStyle(radius: style.value(height: self.frame.height))
            } else {
                self.defaultShadow(radius: style.value(height: self.frame.height))
            }
        } else {
            self.layer.cornerRadius = style.value(height: self.frame.height)
        }
    }
    
    func defaultShadow(opacity: Float = 0.3, radius: CGFloat) {
        addShadow(shadowOpacity: opacity, radius: radius)
    }
    
    func addShadow(shadowLayerName: String = "shadowLayer", shadowFillColor: CGColor? = nil, shadowColor: CGColor =  UIColor.grayPalette(heavy: .normal).cgColor, shadowOffset: CGSize = .zero, shadowOpacity: Float, radius: CGFloat) {
        if let imageView = self as? UIImageView {
//            self.clipsToBounds = true
            let tag: Int = 9282
            self.layer.masksToBounds = false
            self.layer.shadowColor = shadowColor
            self.layer.shadowOpacity = 1
            self.layer.shadowOffset = shadowOffset
            self.layer.shadowRadius = radius
            self.layer.cornerRadius = radius
            self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: radius).cgPath
            self.layer.cornerRadius = radius
            let _imageView: UIImageView = .init(frame: self.bounds)
            _imageView.image = imageView.image
            _imageView.tag = tag
            imageView.image = nil
            self.addSubview(_imageView)
            _imageView.clipsToBounds = true
            _imageView.layer.cornerRadius = radius
        } else {
            self.clipsToBounds = false
            let shadowLayer = CAShapeLayer()
            shadowLayer.name = shadowLayerName
            shadowLayer.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: radius).cgPath
            shadowLayer.fillColor = shadowFillColor ?? self.backgroundColor?.cgColor ?? UIColor.white.cgColor
            shadowLayer.shadowColor = shadowColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = shadowOffset
            shadowLayer.shadowOpacity = shadowOpacity
            shadowLayer.shadowRadius = radius
            shadowLayer.cornerRadius = radius
            layer.cornerRadius = radius
//       layer.insertSublayer(shadowLayer, at: 0)
            layer.insertSublayer(layer: shadowLayer, replaceName: shadowLayerName, at: 0)
        }
    }
    
    func border(width: CGFloat = 1, color: UIColor, rounded: CornerRadiusStyle? = nil) {
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
        if let cornerStyle = rounded {
            self.rounded(style: cornerStyle)
        }
    }
    
    func removeBorder() {
        self.layer.borderWidth = 0
    }
    
    func addOpacityLayer(alpha: CGFloat = 0.35, color: UIColor = .mainThemeBlack) {
        let newView = UIView()
        newView.backgroundColor = color.withAlphaComponent(alpha)
        self.addSubview(newView)
        newView.translatesAutoresizingMaskIntoConstraints = false
        newView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        newView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        newView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        newView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    }
    
    func addSubview(_ view: UIView, replaceTag: Int) {
        if let oldSubview = self.subviews.first(where: { $0.tag == replaceTag } ) {
            oldSubview.removeFromSuperview()
        }
        self.addSubview(view)
    }
}

public extension CALayer {
    /// - description: replce layer: if find name, add it at 0 if not find name
    func insertSublayer(layer: CALayer, replaceName: String, at: UInt32) {
        if let _layers = self.sublayers, let oldLayer = _layers.first(where: { $0.name == replaceName } ) {
            self.replaceSublayer(oldLayer, with: layer)
        } else {
            self.insertSublayer(layer, at: 0)
        }
    }
}

public extension UICollectionViewCell {
    func setShadowStyle(radius: CGFloat) {
        self.contentView.layer.cornerRadius = CGFloat.defauldCornerRadius
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true
        self.layer.shadowColor = UIColor.grayPalette(heavy: .normal).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = 0.3
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        self.layer.masksToBounds = false
    }
}

public extension UICollectionView {
    
    var defaultMargin: CGFloat { return SPLibrary.configurations.collectionViewDefaultMargin }
    private var itemWidthDifference: CGFloat { return defaultMargin*3 }
    var halfHeight: CGFloat { return (bounds.height-defaultMargin)/2 }
    var halfWidth: CGFloat { return (self.bounds.width-itemWidthDifference)/2 }
    var fullWidth: CGFloat { return self.bounds.width - (defaultMargin*2) }
    
    @discardableResult
    /// - description: "collectionViewIsInsideCell" è necessario perchè se la collectionView fosse dentro una cella le dimensioni di width e height verrebbero prese in base a come è disegnato lo xib della collectionView e non il futuro ma effettivo valore
    func setHorizzontalLayout(collectionViewIsInsideCell: Bool, width: CGFloat, topBottomMargin: CGFloat? = nil, leftRightMargin: CGFloat? = nil, itemSpacing: CGFloat? = nil) -> CGSize {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let topMargin = topBottomMargin ?? defaultMargin
        let sideMargin = leftRightMargin ?? defaultMargin
        let itemHeight = bounds.height-(topMargin*2)
        let itemWidth = width-(sideMargin*2)
        layout.sectionInset = UIEdgeInsets(top: topMargin, left: sideMargin, bottom: topMargin, right: sideMargin)
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        layout.minimumInteritemSpacing = itemSpacing ?? defaultMargin
        layout.minimumLineSpacing = itemSpacing ?? defaultMargin
        self.collectionViewLayout = layout
        self.showsHorizontalScrollIndicator = false
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    @discardableResult
    /// - description: "collectionViewIsInsideCell" è necessario perchè se la collectionView fosse dentro una cella le dimensioni di width e height verrebbero prese in base a come è disegnato lo xib della collectionView e non il futuro ma effettivo valore
    func setVerticalLayout(collectionFrame: CGRect? = nil, itemForRow: CGFloat = 2, height: CGFloat? = nil, topInset: CGFloat = 0, bottomInset: CGFloat = 0) -> CGSize {
        let _width: CGFloat = collectionFrame?.width ?? UIScreen.main.bounds.width //: self.bounds.width
        let itemWidth: CGFloat
        if itemForRow == 1 {
            itemWidth = (_width - (defaultMargin*2))/itemForRow
        } else {
            itemWidth = (_width - (defaultMargin*(itemForRow+1)))/itemForRow
        }
        let itemHeight: CGFloat = height ?? itemWidth
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: topInset, left: defaultMargin, bottom: bottomInset, right: defaultMargin)
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        layout.minimumInteritemSpacing = defaultMargin
        layout.minimumLineSpacing = defaultMargin
        self.collectionViewLayout = layout
        self.showsVerticalScrollIndicator = false
        
        // Section Header
        layout.sectionHeadersPinToVisibleBounds = true
        
        return CGSize(width: itemWidth, height: itemHeight)
    }
}

public extension CAGradientLayer {
    convenience init(frame: CGRect, gradientStyle: GradientStyle, gradientOrientation: GradientOrientation) {
        self.init()
        self.frame = frame
        self.colors = gradientStyle.colors.map { $0.cgColor }
        startPoint = gradientOrientation.draw().x
        endPoint = gradientOrientation.draw().y
    }
    
    func createGradientImage() -> UIImage? {
        var image: UIImage? = nil
        UIGraphicsBeginImageContext(bounds.size)
        if let context = UIGraphicsGetCurrentContext() {
            render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
        return image
    }
}

public extension UINavigationBar {
    func transparentNavigationBar() {
        
        if #available(iOS 13.0, *) {
//            navBarAppearance.configureWithDefaultBackground()
            standardAppearance.configureWithTransparentBackground()
            compactAppearance?.configureWithTransparentBackground()
            scrollEdgeAppearance?.configureWithTransparentBackground()
            isTranslucent = true
        } else {
            self.setBackgroundImage(UIImage(), for: .default)
            self.shadowImage = UIImage()
            self.isTranslucent = true
        }
    }
    
    func setGradientBackground(gradientStyle: GradientStyle, gradientOrientation: GradientOrientation) {
        var updatedFrame = bounds
        updatedFrame.size.height += self.frame.origin.y
        let gradientLayer = CAGradientLayer(frame: updatedFrame, gradientStyle: gradientStyle, gradientOrientation: gradientOrientation)
        setBackgroundImage(gradientLayer.createGradientImage(), for: UIBarMetrics.default)
        self.tintColor = gradientStyle.tintColor
    }
    
    func removeTrasparent() {
        self.isTranslucent = false
    }
    
    func removeGradient(replace color: UIColor) {
        self.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.backgroundColor = color
    }
}

public enum DiagonalMaskOrientation {
    case leftToRight
    case rightToLeft
}

public extension UIImageView {
    func loadInitiallyNameLetters(name: String, backgroundColor: UIColor) {
        let view = UIView(frame: self.bounds)
        let label: UILabel = .init(frame: view.frame)
        label.text = name.initials
        label.font = UIFont.libraryFont(ofSize: 18, weight: .semibold)
        label.textColor = .mainThemeWhite
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        label.center = view.center
        label.font.withSize(30)
        view.addSubview(label)
        view.backgroundColor = backgroundColor
        self.addSubview(view)
    }
}

public extension UIColor {
    
    var isLight: Bool {
        guard let components = cgColor.components, components.count > 2 else { return false }
        let brightness = ((components[0] * 299) + (components[1] * 587) + (components[2] * 114)) / 1000
        if brightness > 0.5 {
            return true
        } else {
            return false
        }
    }
    
    /// - description: Automatically calculate tintColor white or dark based on bacgkgroundColor  (*self*)
    var automaticallyTintedColor: UIColor {
        if self.isLight {
            return .mainThemeBlack
        } else {
            return .mainThemeWhite
        }
    }
    
    func automaticallyCustomTintedColor(dark: UIColor, light: UIColor) -> UIColor {
        precondition(!dark.isLight, "is not a dark color")
        precondition(light.isLight, "is not a light color")
        if self.isLight { return dark } else { return light }
    }
}

public extension UIImage {
    
    func tinted(_ tintColor: UIColor) -> UIImage {
        if self.size.width == 0 || self.size.height == 0 { return self }
        let rect: CGRect = CGRect(origin: CGPoint(x: 0.0, y: 0.0), size: CGSize(width: self.size.width, height: self.size.height))
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: rect)
        guard let context = UIGraphicsGetCurrentContext() else { return self }
        context.setBlendMode(CGBlendMode.sourceIn)
        tintColor.setFill()
        UIColor.green.setStroke()
        let shape = UIBezierPath.init(rect: rect)
        shape.fill()
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image ?? UIImage.init()
    }
    
    func bordered(width: CGFloat, color: UIColor) -> UIImage {
        if self.size.width == 0 || self.size.height == 0 { return self }
//        let square = CGSize(width: min(size.width, size.height) +  2, height: min(size.width, size.height) +  2)
        let rect: CGRect = CGRect(origin: CGPoint(x: 0.0, y: 0.0), size: CGSize(width: self.size.width + width * 2, height: self.size.height + width * 2))
        let imageView = UIImageView(frame: rect)
        imageView.contentMode = .center
        imageView.image = self
        imageView.layer.borderWidth = width
        imageView.layer.borderColor = color.cgColor
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return self }
        imageView.layer.render(in: context)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result ?? self
    }
}

public extension UIButton {
    func applyBorderGradient(colors: [UIColor]) {
        let gradient = CAGradientLayer()
        gradient.name = "gradientLayer"
        gradient.frame =  CGRect(origin: CGPoint.zero, size: self.frame.size)
        gradient.colors = colors
        let shape = CAShapeLayer()
        shape.lineWidth = self.layer.borderWidth
        shape.path = UIBezierPath(rect: self.bounds).cgPath
//        shape.strokeColor = UIColor.black.cgColor
        shape.fillColor = UIColor.clear.cgColor
        gradient.mask = shape
        self.layer.addSublayer(gradient)
    }
    
    func applyBorderGradient(gradientStyle: GradientStyle) {
        self.applyBorderGradient(colors: gradientStyle.colors)
    }
}
