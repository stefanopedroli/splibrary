//  Created by Stefano Pedroli on 02/10/2020.

import UIKit

open class PulseAnimation: CALayer {

    private var animationGroup = CAAnimationGroup()
    public var animationDuration: TimeInterval = 1.5
    private var radius: CGFloat = 200
    private var numebrOfPulse: Float = Float.infinity
    
    override init(layer: Any) {
        super.init(layer: layer)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public init(numberOfPulse: Float = Float.infinity, radius: CGFloat){
        super.init()
//        self.backgroundColor = UIColor.black.cgColor
        self.contentsScale = UIScreen.main.scale
        self.opacity = 1
        self.radius = radius
        self.numebrOfPulse = numberOfPulse
        self.frame = CGRect(x: 0, y: 0, width: radius*2, height: radius*2)
        self.cornerRadius = radius
        
        DispatchQueue.global(qos: .default).async {
            self.setupAnimationGroup()
            DispatchQueue.main.async {
                self.add(self.animationGroup, forKey: "pulse")
           }
        }
    }
    
    public func scaleAnimation() -> CABasicAnimation {
        let scaleAnimaton = CABasicAnimation(keyPath: "transform.scale.xy")
        scaleAnimaton.fromValue = NSNumber(value: 0)
        scaleAnimaton.toValue = NSNumber(value: 1)
        scaleAnimaton.duration = animationDuration
        return scaleAnimaton
    }
    
    public func createOpacityAnimation() -> CAKeyframeAnimation {
        let opacityAnimiation = CAKeyframeAnimation(keyPath: "opacity")
        opacityAnimiation.duration = animationDuration
        opacityAnimiation.values = [0.4,0.8,0]
        opacityAnimiation.keyTimes = [0,0.3,1]
        return opacityAnimiation
    }
    
    public func setupAnimationGroup() {
        self.animationGroup.duration = animationDuration
        self.animationGroup.repeatCount = numebrOfPulse
        let defaultCurve = CAMediaTimingFunction(name: CAMediaTimingFunctionName.default)
        self.animationGroup.timingFunction = defaultCurve
        self.animationGroup.animations = [scaleAnimation(),createOpacityAnimation()]
    }
}

public extension UIView {
    func pulseAnimation(color: UIColor, numberOfPulse: Float = Float.infinity, radius: CGFloat, center: CGPoint?) {
        let pulse = PulseAnimation(numberOfPulse: numberOfPulse, radius: radius)
        pulse.animationDuration = 1.0
        pulse.backgroundColor = color.cgColor
        pulse.position = center ?? self.center
        self.layer.insertSublayer(pulse, below: self.layer)
    }
    
    func pulsate() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 1
        pulse.fromValue = 0.7
        pulse.toValue = 0.8
        pulse.autoreverses = true
        pulse.repeatCount = .infinity
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        layer.add(pulse, forKey: "pulse")
    }
    
    func pulsateLinear(numberOfPulse: Float = Float.infinity, color: UIColor? = nil) {
        
        func scaleAnimation() -> CABasicAnimation {
            let scaleAnimaton = CABasicAnimation(keyPath: "transform.scale.xy")
            scaleAnimaton.fromValue = NSNumber(value: 0)
            scaleAnimaton.toValue = NSNumber(value: 1)
            scaleAnimaton.duration = animationDuration
            return scaleAnimaton
        }
        
        func createOpacityAnimation() -> CAKeyframeAnimation {
            let opacityAnimiation = CAKeyframeAnimation(keyPath: "opacity")
            opacityAnimiation.duration = animationDuration
            opacityAnimiation.values = [0.4,0.8,0]
            opacityAnimiation.keyTimes = [0,0.3,1]
            return opacityAnimiation
        }

        let animationGroup = CAAnimationGroup()
        let animationDuration: TimeInterval = 1.5
        
        animationGroup.duration = animationDuration
        animationGroup.repeatCount = numberOfPulse
        let defaultCurve = CAMediaTimingFunction(name: CAMediaTimingFunctionName.default)
        animationGroup.timingFunction = defaultCurve
        animationGroup.animations = [scaleAnimation(),createOpacityAnimation()]
        if let background = color { self.backgroundColor = background }
        layer.add(animationGroup, forKey: "pulse")
    }
}



