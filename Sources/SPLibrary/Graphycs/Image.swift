//  Created by Stefano Pedroli on 27/11/2019.

import UIKit
import Nuke

public extension UIImage {
    static func color(_ color: UIColor, size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContext(size)
        defer {
            UIGraphicsEndImageContext()
        }
        let rect = CGRect(origin: .zero, size: size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    convenience init?(view: UIView) {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
        guard let graphicsContext = UIGraphicsGetCurrentContext() else { return nil }
        view.layer.render(in: graphicsContext)
        guard let image = UIGraphicsGetImageFromCurrentImageContext()?.cgImage else { return nil }
        UIGraphicsEndImageContext()
        self.init(cgImage: image)
    }
}

public extension UIView {
    func renderedImage() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0.0)
        guard let graphicsContext = UIGraphicsGetCurrentContext() else { return nil }
        self.layer.render(in: graphicsContext)
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return image
    }
}

public extension UIImageView {
    
    enum Error: Swift.Error {
        case noUrl
        case failedLoadingData
        case noConnection
        
        var localizedDescription: String {
            switch self {
            case .noUrl: return "Url non valido"
            case .failedLoadingData: return "Immagine non trovata"
            case .noConnection: return "No connection"
            }
        }
    }
    
    func download(from url: URL?, defaultImage: UIImage) {
        download(from: url) { (error) in
            self.image = defaultImage
            self.layoutIfNeeded()
            UIAccessibility.post(notification: UIAccessibility.Notification.screenChanged, argument: self)
        }
    }
    
    func download(path: String, defaultImage: UIImage? = nil) {
        guard let url = URL(string: path) else {
            self.image = defaultImage
            self.layoutIfNeeded()
            UIAccessibility.post(notification: UIAccessibility.Notification.screenChanged, argument: self)
            return
        }
        download(from: url) { (error) in
            self.image = defaultImage
            self.layoutIfNeeded()
            UIAccessibility.post(notification: UIAccessibility.Notification.screenChanged, argument: self)
        }
    }
    
    private func setupEmptyImage() {
        DispatchQueue.main.async() {
            self.image = UIImage.color(.white, size: self.bounds.size) // default white image
            self.layoutIfNeeded()
            UIAccessibility.post(notification: UIAccessibility.Notification.screenChanged, argument: self)
        }
    }
    
    @objc internal func reloadImage(notification: NSNotification) {
        guard let path = notification.object as? String else { return }
        self.download(path: path)
    }
    
    /// - description: Automatically download and cache images permanently in you app
    func download(from url: URL?, error: ((_ error: Swift.Error?) -> Swift.Void)? = nil) {
        guard let url = url else {
            if let error = error { error(UIImageView.Error.noUrl) } else { self.setupEmptyImage() }
            return
        }
        if let cachedImage = FileManager.default.retrieveImage(url: url) {
            self.image = cachedImage
            self.layoutIfNeeded()
            UIAccessibility.post(notification: UIAccessibility.Notification.screenChanged, argument: self)
        }
        if requiredReloadImage(from: url) {
            if ConnectivityService.unreachable {
                self.addReloadConnectionObserver(selector: #selector(self.reloadImage(notification:)), object: url.absoluteString)
                if let error = error { error(UIImageView.Error.noConnection) } else { self.setupEmptyImage() }
                return
            }
            URLSession.shared.dataTask(with: url) { data, response, _error in
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let data = data, _error == nil,
                    let image = UIImage(data: data)
                else {
                    self.addReloadConnectionObserver(selector: #selector(self.reloadImage(notification:)), object: url.absoluteString)
                    if let error = error { error(_error) } else { self.setupEmptyImage() }
                    return
                }
                DispatchQueue.main.async() {
                    self.setTimerForImages(from: url)
                    FileManager.default.saveImage(image: image, url: url)
                    self.image = image
                    self.layoutIfNeeded()
                    UIAccessibility.post(notification: UIAccessibility.Notification.screenChanged, argument: self)
                }
            }.resume()
        }
    }
    
    private func setTimerForImages(from url: URL) {
        let timestamp: String = Date.today.timestampDescription
        CacheManager.cache.set(object: NSString(string: timestamp), forKey: url.absoluteString)
    }
    
    /// - description: This function search a timestamp informations relative for a specific image url, than if is present and is not expired (less than one day) the image will never reloaded, if is expired or cache was deleted beacause is just temporary cache (if you close your app you lost this informations) the image will be reloaded
    private func requiredReloadImage(from url: URL) -> Bool {
        guard let timer = CacheManager.cache.get(type: .text, key: url.absoluteString) as? String else {
            return true
        }
        guard let date = timer.timestampDate else { return true }
        return date.differenceDay(from: Date.today) > 1
    }
    
    func enableZoom() {
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(startZooming(_:)))
        isUserInteractionEnabled = true
        addGestureRecognizer(pinchGesture)
    }
    
    @objc private func startZooming(_ sender: UIPinchGestureRecognizer) {
        if sender.state == .ended {
            
        }
        let scaleResult = sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale)
        guard let scale = scaleResult, scale.a > 1, scale.d > 1 else { return }
        sender.view?.transform = scale
        sender.scale = 1
    }
}

public extension UIImage {
    class func getIcon<T: RawRepresentable>(withType: T) -> UIImage where T.RawValue == String {
        guard let image = UIImage(named: withType.rawValue) else {
            return UIImage()
        }
        return image
    }
}

public extension UIImage {
    func template() -> UIImage {
        return self.withRenderingMode(.alwaysTemplate)
    }
}

public extension UIImage {
    static func generatePallino(container: UIView) -> UIImage {
        let _pallino: UIView = .init(frame: CGRect(x: 0, y: 0, width: container.bounds.width/2, height: container.bounds.height/2))
        _pallino.backgroundColor = .mainThemeWhite
        _pallino.rounded(style: .half)
        let containerView: UIView = UIView(frame: container.bounds)
        _pallino.center = containerView.center
        containerView.addSubview(_pallino)
        return UIImage.init(view: containerView)!
    }
}

// MARK: Nuke Extensions
public extension UIImageView {
    func nuke(load url: URL, options: ImageLoadingOptions? = nil) {
        let options = ImageLoadingOptions(placeholder: nil, transition: .fadeIn(duration: 0.3), failureImage: nil, contentModes: .init(success: self.contentMode, failure: self.contentMode, placeholder: self.contentMode))
        Nuke.loadImage(with: url, options: options, into: self)
    }
}

public extension ImageLoadingOptions {

    static var defaultImageLoadingOptions: ImageLoadingOptions = ImageLoadingOptions(placeholder: nil,
                                                                                     transition: .fadeIn(duration: 0.3),
                                                                                     failureImage: nil,
                                                                                     contentModes: .init(
                                                                                        success: .scaleAspectFill,
                                                                                        failure: .scaleAspectFill,
                                                                                        placeholder: .scaleAspectFill))

}
