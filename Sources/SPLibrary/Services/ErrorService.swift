//  Created by Stefano Pedroli on 30/11/2019.

import UIKit

public protocol ErrorServiceDelegate: class {
    func errorServiceDidInvalidateDevice(_ errorService: ErrorService)
    func errorServiceDidInvalidateSession(_ errorService: ErrorService)
}

public final class ErrorService {
    
    weak var delegate: ErrorServiceDelegate?

//    private func handle(_ error: NetworkLayer.Error, from presentingViewController: UIViewController) {
//        switch error {
//        case .network, .noConnectivity: break
//        case .server(let error): break
//        }
//    }
}
