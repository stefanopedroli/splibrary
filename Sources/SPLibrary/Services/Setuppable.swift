//  Created by Stefano Pedroli on 02/12/2019.
//
import UIKit

// MARK: General Setup Protocol
public protocol Setup: StoryboardInstantiable {
    func setupNavigation()
    static var storyboardName: String { get }
}

//MARK: - StoryboardInstantiable
public extension Setup where Self: UIViewController {
    static var sceneStoryboard: UIStoryboard {
        return UIStoryboard(name: storyboardName, bundle: nil)
    }
}

// MARK: Collection View
public protocol CollectionViewSetup: class {
    var collectionView: UICollectionView! { get set }
    func setupCollectionView()
    func registerCells()
    func customSetupCollectionView()
}

public protocol StaticCollectionDelegate {
    associatedtype TypeOfSection = CellType
    var cells: [TypeOfSection] { get set }
}

public protocol CollectionCellType {
    var collectionCellTypes: [(UICollectionViewCell & NibReusable).Type] { get }
}

public extension CollectionViewSetup where Self: UICollectionViewDelegate, Self: UICollectionViewDataSource {
    func setupCollectionView() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.registerCells()
        self.customSetupCollectionView()
    }
    
    func customSetupCollectionView() {}
}

public extension CollectionCellType where Self: UICollectionViewDelegate, Self: UICollectionViewDataSource, Self: CollectionViewSetup {
    func registerCells() {
        for cell in collectionCellTypes {
            self.collectionView.register(cell.nib, forCellWithReuseIdentifier: cell.reuseIdentifier)
        }
    }
}

// Collection typealias
public typealias CollectionViewProtocol = CollectionViewSetup & CollectionCellType & UICollectionViewDelegate & UICollectionViewDataSource
public typealias StaticCollectionViewProtocol = CollectionViewProtocol & StaticCollectionDelegate

// MARK: - Table View
public protocol TableViewSetup: class {
    var tableView: UITableView! { get set }
    func setupTableView()
    func registerCells()
    func customSetupTableView()
}

public protocol StaticTableDelegate: class {
    associatedtype TypeOfSection = CellType
    var cells: [TypeOfSection] { get set }
}

public protocol StaticSectionsTableDelegate: class {
    associatedtype TypeOfRow = CellType
    associatedtype TypeOfSections = CellSectionsType
    var sections: [(type: TypeOfSections, rows: [TypeOfRow])] { get set }
}

public protocol TableCellType {
    var tableCellTypes: [(UITableViewCell & NibReusable).Type] { get }
}

public extension TableViewSetup where Self: UITableViewDelegate, Self: UITableViewDataSource {
    func setupTableView() {
        precondition(tableView != nil, "You forgot to connect tableView!")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.registerCells()
        self.tableView.tableFooterView = UIView()
        self.customSetupTableView()
    }
    
    func customSetupTableView() {}
}

public extension TableCellType where Self: UITableViewDelegate, Self: UITableViewDataSource, Self: TableViewSetup {
    func registerCells() {
        for cell in tableCellTypes {
            self.tableView.register(cell.nib, forCellReuseIdentifier: cell.reuseIdentifier)
        }
    }
}

// TableView typealias
public typealias TableViewProtocol = TableViewSetup & TableCellType & UITableViewDelegate & UITableViewDataSource
public typealias StaticTableViewProtocol  = TableViewProtocol & StaticTableDelegate
public typealias SectionsTableViewProtocol = TableViewProtocol & StaticSectionsTableDelegate

// MARK: Extensions and Models
public extension NSObject {
    func loadAllSetup() {
        if let view = self as? TableViewSetup {
            view.setupTableView()
        }
        if let view = self as? Setup {
            view.setupNavigation()
        }
        if let view = self as? CollectionViewSetup {
            view.setupCollectionView()
        }
        
        if let refreshable = self as? Refreshable & TableViewSetup {
            if let refresh = refreshable.setupRefreshLoader() {
                refreshable.addRefresh(refreshControl: refresh)
            } else {
                refreshable.addRefresh(refreshControl: .default)
            }
        }
    }
}

public protocol CellType {} // servirà in futuro
public protocol CellSectionsType {} // servirà in futuro
