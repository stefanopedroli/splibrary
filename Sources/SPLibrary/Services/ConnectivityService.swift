//  Created by Stefano Pedroli on 09/12/20.

import Foundation
import Reachability

internal let reachability: Reachability = try! Reachability.init()

public protocol ConnectivityServiceDelegate: class {
    func connectivityService(didChangeReachability status: ConnectivityStatus, previouslyStatus: ConnectivityStatus?, mode: ConnectivityMode?)
}

/// - description: Class for connectivity
/// - description: If you use coordinator, **start** this on main coordinator and use notifications for all others part in app
public final class ConnectivityService: NSObject {
    
    private weak var delegate: ConnectivityServiceDelegate?
        
    public func start(delegate: ConnectivityServiceDelegate) {
        self.delegate = delegate
        NotificationCenter.default.addObserver(self, selector: #selector(checkForReachability(notification:)), name: Notification.Name.reachabilityChanged, object: reachability)
        try? reachability.startNotifier()
        self.delegate?.connectivityService(didChangeReachability: status, previouslyStatus: nil, mode: mode)
        self.previouslyState = self.status
    }
    
    public var status: ConnectivityStatus {  return ConnectivityStatus(networkStaus: reachability.connection) }
    private var previouslyState: ConnectivityStatus?
    private var mode: ConnectivityMode? { return ConnectivityMode(networkStaus: reachability.connection) }
    public var noConnection: Bool { return self.status == .unreachable }
    
    @objc private func checkForReachability(notification: NSNotification) {
        // Remove the next two lines of code. You cannot instantiate the object
        // you want to receive notifications from inside of the notification
        // handler that is meant for the notifications it emits.
        guard let networkReachability = notification.object as? Reachability else { return assertionFailure("error posting Reachability") }
        let reachabilityStatus = networkReachability.connection
        let status = ConnectivityStatus(networkStaus: reachabilityStatus)
        let mode = ConnectivityMode(networkStaus: reachabilityStatus)
        DispatchQueue.main.async {
            self.delegate?.connectivityService(didChangeReachability: status, previouslyStatus: self.previouslyState, mode: mode)
            self.previouslyState = self.status
        }
    }
    
    public static var unreachable: Bool {
        return reachability.connection == .unavailable
    }
}

public extension NSObject {
    func addReloadConnectionObserver(selector: Selector, object: String) {
        NotificationCenter.default.addObserver(self, selector: selector, name: Notification.Name.reachabilityChanged, object: object)
    }
}

public enum ConnectivityStatus {
    case unreachable, reachable
    
    init(networkStaus: Reachability.Connection) {
        switch networkStaus {
        case .unavailable: self = .unreachable
        case .wifi: self = .reachable
        case .cellular: self = .reachable
        case .none: self = .unreachable
        }
    }
}

public enum ConnectivityMode {
    case wwan, wifi
    
    init?(networkStaus: Reachability.Connection) {
        switch networkStaus {
        case .unavailable: return nil
        case .wifi: self = .wifi
        case .cellular: self = .wwan
        case .none: return nil
        }
    }
}
