//  Created by Stefano Pedroli on 17/04/2020.

import UIKit
import AVKit
import Photos

final public class PermissionService {
    
    public typealias PermissionHandler = (_ status: PermissionsStatus) -> Swift.Void
    
    public static func checkPhotoPermission(from presentingViewController: UIViewController, completion: @escaping PermissionHandler) {
        
        let completion: PermissionHandler = { status in
            DispatchQueue.main.async { completion(status) }
        }
        
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .authorized: completion(.authorized)
        case .denied:
            showRequiredSettingsPermission(from: presentingViewController, type: .camera)
            completion(.denied)
        default:
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
                if granted == true {
                    // User granted
                     completion(.authorized)
                } else {
                    // User rejected
                     completion(.denied)
                }
            })
        }
    }

    public static func checkPhotoLibraryPermission(from presentingViewController: UIViewController, completion: @escaping PermissionHandler) {
        
        let completion: PermissionHandler = { status in
            DispatchQueue.main.async { completion(status) }
        }
        
        switch PHPhotoLibrary.authorizationStatus() {
        case .authorized: completion(.authorized)
        case .denied, .restricted: showRequiredSettingsPermission(from: presentingViewController, type: .photoLibrary)
        default:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                case .authorized: completion(.authorized)
                case .denied, .restricted: completion(.denied)
                default: completion(.notDeterminated)
                }
            }
        }
    }
    
    private static func showRequiredSettingsPermission(from presenting: UIViewController, type: PermissionsType) {
        let alert = UIAlertController(title: "IMPORTANT", message: type.message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow \(type.title)", style: .cancel, handler: { (alert) -> Void in
            if let settings = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(settings)
            }
        }))
        presenting.present(alert, animated: true, completion: nil)
    }
    
    public enum PermissionsStatus {
        case authorized
        case denied
        case notDeterminated
        case restricted
    }
    
    public enum PermissionsType {
        case camera, photoLibrary
        
        var title: String {
            switch self {
            case .camera: return "Camera"
            case .photoLibrary: return "Photo Library"
            }
        }
        
        var message: String {
            switch self {
            case .camera: return "Camera access required for capturing photos!"
            case .photoLibrary: return "Photo Library access required to provide main features of application"
            }
        }
    }
}
