//  Created by Stefano Pedroli on 30/11/2019.

import UIKit

public protocol NotificationServiceDelegate: class {
    /// - description: Call backend register device token
    func handleNotification(userInfo: [AnyHashable: Any])
    /// - warning: when is called *UIApplication.shared.applicationIconBadgeNumber* is already updated
    func notifyUpdateBadgeNotification(number: Int)
}

/// - description: The Service that manage entirly your notification in app (register, handle, show, revice, token...)
public final class NotificationService: NSObject {
    
    public var defaults: UserDefaults = .standard
    private let notificationCenter = UNUserNotificationCenter.current()
    public var delegate: NotificationServiceDelegate?
    
    public init(networkLayer: NetworkLayer) {
        self.networkLayer = networkLayer
        super.init()
        self.notificationCenter.delegate = self
    }
    
    var networkLayer: NetworkLayer
    
    public var pushToken: String? {
        get { return defaults.object(forKey: .pushToken) as? String }
        set { defaults.set(newValue, forKey: .pushToken) }
    }
    
    /// - description: properties that say if permission has already requested
    /// - warning: If you want know the notification status you necessary call *checkUserSettings* method
    public private(set) var hasAlreadyAskedForUserAuthorization: Bool {
        get { return defaults.bool(forKey: .kHasAlreadyAskedForUserAuthorization) }
        set { defaults.set(newValue, forKey: .kHasAlreadyAskedForUserAuthorization) }
    }
    
    /// - description: properties that save notification status from *checkUserSettings* method
    public var isNotificationActive: Bool {
        get { return defaults.bool(forKey: .isNotificationActive) }
        set { defaults.set(newValue, forKey: .isNotificationActive) }
    }
    
    /// - description: properties that say if user has already acept custom permission vc (is true also if user refuse apple default alert permission)
    public var acceptedCustomPermissionViewController: Bool {
        get { return defaults.bool(forKey: .presentedPermissionViewController) }
        set { defaults.set(newValue, forKey: .presentedPermissionViewController) }
    }
    
    public var alreadyRegisterDevice: Bool {
        get { return defaults.bool(forKey: .alreadyRegisterDevice) }
        set { defaults.set(newValue, forKey: .alreadyRegisterDevice) }
    }
    
    public var deregisterNotificationTokenFailed: Bool {
        get { CacheManager.get(type: Bool.self, for: .deregisterNotificationTokenFailed) ?? false }
        set { CacheManager.set(object: newValue, for: .deregisterNotificationTokenFailed) }
    }
    
    func clear() {
        self.alreadyRegisterDevice = false
        self.deregisterNotificationTokenFailed = false
        self.pushToken = String()
    }
    
    /// - description: Save Token on device
    /// - warning: you need to implement your custom backend call for saving token  by using "completion"  param
    public func registerDevice(token: String) {
        print("\(EmojiService.deviceToken) DEVICE TOKEN: \(token)")
        var oldToken = self.pushToken
        if token != oldToken {
            self.pushToken = token
        } else {
            oldToken = nil
        }
        
        // chiamata al backend per dargli il token
        self.performSynchNotification()
    }
    
    private func performSynchNotification() {
        guard let token = self.pushToken else { return }
        self.registerNotificationToken(token: token, completion: { success in
            if success {
                self.alreadyRegisterDevice = true
            } else {
                self.alreadyRegisterDevice = false
            }
        })
    }
    
    public typealias PreAutorizationRequestCallBack = (_ preautorizationViewController: UIViewController) -> Swift.Void
    private var preAutorizationViewController: UIViewController?
    
    public func preAutorizationRequestIfNeeded(presentCustomPermission: @escaping (_ callback: @escaping PreAutorizationRequestCallBack) -> Void, onComplete: PermissionHandler? = nil) {
        if self.acceptedCustomPermissionViewController == false {
            presentCustomPermission() { controller in
                self.preAutorizationViewController = controller
                self.acceptedCustomPermissionViewController = true
                self.askForUserAuthorization(onComplete: onComplete)
            }
        } else {
            self.askForUserAuthorization(onComplete: onComplete)
        }
    }
    
    /// - description: Ask notifications permission. If you have already asked this will never request
    /// - warning: You have to use this method also for update notification status. Implement this on UIApplication.willEnterForegroundNotification notification
    internal func askForUserAuthorization(onComplete: PermissionHandler? = nil) {
        // If previously deviceToken or Profiles goes in KO you could try to re-make them
        if !self.alreadyRegisterDevice { self.performSynchNotification() }
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert]) { (granted, error) in
                self.hasAlreadyAskedForUserAuthorization = true
                self.isNotificationActive = true
                DispatchQueue.main.async { self.preAutorizationViewController?.close() }
                if granted {
                    let categories : Set<UNNotificationCategory> = []
                    UNUserNotificationCenter.current().setNotificationCategories(categories)
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
                onComplete?(granted)
            }
        } else {
            DispatchQueue.main.async { self.preAutorizationViewController?.close() }
            self.hasAlreadyAskedForUserAuthorization = true
            self.isNotificationActive = true
            let categories : Set<UIMutableUserNotificationCategory> = []
            let notificationSettings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: categories)
            UIApplication.shared.registerUserNotificationSettings(notificationSettings)
            UIApplication.shared.registerForRemoteNotifications()
            onComplete?(true)
        }
    }
    
    public func checkUserSettings(_ completion: @escaping PermissionHandler) {
        // If previously deviceToken or Profiles goes in KO you could try to re-make them
        if self.alreadyRegisterDevice == false && self.hasAlreadyAskedForUserAuthorization == true {
            // device token goes in error but you ask already permission
            self.performSynchNotification()
        }
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings() { (notificationSettings) in
                let hasAutorization = notificationSettings.authorizationStatus == .authorized
                DispatchQueue.main.async {
                    self.isNotificationActive = hasAutorization
                    completion(hasAutorization)
                }
            }
        } else if let notificationTypes = UIApplication.shared.currentUserNotificationSettings?.types, notificationTypes != [] {
            DispatchQueue.main.async {
                self.isNotificationActive = true
                completion(true)
            }
        } else {
            DispatchQueue.main.async {
                self.isNotificationActive = false
                completion(false)
            }
        }
    }
    
    public func openNotificationSettings() {
        DispatchQueue.main.async {
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return printError("notification url not found")
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    UIApplication.shared.openURL(settingsUrl)
                }
            }
        }
    }
    
    public func registerNotificationToken(token: String, completion: @escaping PermissionHandler) {
        guard let token = self.pushToken else { return }
        let param: DeviceToken.PathParameters = .init()
        let input: DeviceToken.Input = .init(token: token)
        let request = DeviceToken.request(input: input, pathParams: param)
        self.networkLayer.perform(request: request) { (result) in
            switch result {
            case .failure( _):
                completion(false)
            case .success( _):
                completion(true)
            }
        }
    }
    
    public func deregisterNotificationTokenIfNeeded(completion: @escaping VoidHandler) {
        if self.alreadyRegisterDevice || self.deregisterNotificationTokenFailed {
            guard let token = self.pushToken else { return completion() }
            let param: DeleteToken.PathParameters = .init(token: token)
            let input: DeleteToken.Input = .init()
            let request = DeleteToken.request(input: input, pathParams: param)
            self.networkLayer.perform(request: request) { (result) in
                switch result {
                case .failure( _):
                    self.deregisterNotificationTokenFailed = true
                case .success( _):
                    self.deregisterNotificationTokenFailed = false
                }
                completion()
            }
        } else { completion() }
    }
    
    public func sendInternalPushNotification<PassedPushNotificationType: Decodable & RawRepresentable, T: Decodable>(title: String, body: String, scheduler: Date, identifier: String, service_id: PassedPushNotificationType, payload: T, notification_id: String?) {
        
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = .default
        content.badge = 1
        
//        content.userInfo = ["data"] {
//
//        }
        
        let pushData: PushData<T, PassedPushNotificationType> = .init(service_id: service_id, payload: payload, notification_id: notification_id)
        
        content.userInfo["data"] = pushData
        
        // Configure the recurring date.
        var dateComponents = DateComponents()
        dateComponents.calendar = Calendar.current
        dateComponents.year = scheduler.year
        dateComponents.month = scheduler.month
        dateComponents.hour = scheduler.hour
        dateComponents.minute = scheduler.minutes
        dateComponents.second = 0
        
        // Create the trigger as a repeating event.
//        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: scheduler, repeats: false)
        let trigger = UNCalendarNotificationTrigger.init(dateMatching: dateComponents, repeats: false)
        // Create the request
//        let uuidString = UUID().uuidString
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        // Schedule the request with the system.
        self.notificationCenter.add(request) { (error) in
            if let error = error {
                printError("Failed scheduling push notification, \(error.localizedDescription)")
            } else {
                print("Notification scheduled at: \(scheduler.getFormattedDate(format: .all))")
            }
        }
    
//        let snoozeAction = UNNotificationAction(identifier: "Apri", title: "Apri", options: [])
//        let deleteAction = UNNotificationAction(identifier: "Annulla", title: "Annulla", options: [.destructive])
//        let category = UNNotificationCategory(identifier: identifier, actions: [snoozeAction, deleteAction], intentIdentifiers: [], options: [])
//        notificationCenter.setNotificationCategories([category])
    }
    
    public func removePendingInternalPushNotification(from identifier: String) {
        self.notificationCenter.removePendingNotificationRequests(withIdentifiers: [identifier])
    }
}

extension NotificationService: UNUserNotificationCenterDelegate {
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if let badge = notification.request.content.badge {
            UIApplication.shared.applicationIconBadgeNumber = Int(truncating: badge)
            self.delegate?.notifyUpdateBadgeNotification(number: Int(truncating: badge))
        }
        completionHandler([.alert, .sound])
    }
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        self.delegate?.handleNotification(userInfo: response.notification.request.content.userInfo)
        completionHandler()
    }
}
