//  Created by Stefano Pedroli on 18/02/2020.

import Foundation

public class NotificationCenterService<NotificationIdentifier: RawRepresentable> where NotificationIdentifier.RawValue == String {
    
    public static func post(notification type: NotificationIdentifier, object anObject: Any? = nil, userInfo aUserInfo: [AnyHashable : Any]? = nil) {
        let notification = NSNotification.Name(rawValue: type.rawValue)
        NotificationCenter.default.post(name: notification, object: anObject, userInfo: aUserInfo)
    }
    
    public static func add(_ observer: Any, type: NotificationIdentifier, action: Selector) {
        let name = NSNotification.Name(rawValue: type.rawValue)
        NotificationCenter.default.addObserver(observer, selector: action, name: name, object: nil)
    }
    
    public static func adds(_ observer: Any, notifications: [(type: NotificationIdentifier, action: Selector)]) {
        for notification in notifications {
            add(observer, type: notification.type, action: notification.action)
        }
    }
}
