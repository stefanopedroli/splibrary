//  Created by Stefano Pedroli on 30/11/2019.

import LocalAuthentication

public enum BiometryType: CustomStringConvertible {
    case none, touchID, faceID
    
    public var description: String {
        switch self {
        case .none: return ""
        case .faceID: return "FaceID"
        case .touchID: return "TouchID"
        }
    }
    
    public var usageDescription: String {
        switch self {
        case .none: return ""
        case .faceID: return "Utilizza il tuo volto per accedere"
        case .touchID: return "Utilizza la tua impronta per accedere"
        }
    }
    
    public var activationDescription: String {
        switch self {
        case .none: return ""
        case .faceID: return "..."
        case .touchID: return "..."
        }
    }
}

public final class BiometryService {
    
    public enum Result {
        case confirmed, cancelled, fallback
    }
    
    public func canEvaluatePolicy() -> Bool {
        var error: NSError?
        if LAContext().canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            return error == nil
        } else {
            return false
        }
    }
    
    public var biometryType: BiometryType {
        guard #available(iOS 11.0, *) else {
            return canEvaluatePolicy() ? .touchID : .none
        }
        
        // Always ensure that `canEvaluatePolicy:error:` is called before
        // accessing the context's `biometryType`.
        let context = LAContext()
        _ = context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
        switch context.biometryType {
        case .none: return .none
        case .faceID: return .faceID
        case .touchID: return .touchID
        @unknown default: return .none
        }
    }
    
    public var usesBiometricType: Bool {
        switch biometryType {
        case .none:    return false
        case .touchID: return true
        case .faceID:  return true
        }
    }
    
    public func evaluatePolicy(withMessage localizedReason: String, completion: @escaping (Result) -> Void) {
        // Use a new context for each call in order to force the system to
        // prompt the user each time for biometric authentication.
        LAContext().evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: localizedReason) { succeeded, error in
            DispatchQueue.main.async {
                guard let error = error as? LAError else { return completion(.confirmed) }
                
                switch error.code {
                case .userCancel:
                    completion(.cancelled)
                default:
                    completion(.fallback)
                }
            }
        }
    }
}
