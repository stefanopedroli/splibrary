//  Created by Stefano Pedroli on 30/03/2020.

import UIKit

public final class NotifyService<NotificationIdentifier: RawRepresentable>  where NotificationIdentifier.RawValue == String {
    
    public static func post(notification type: NotificationIdentifier, object anObject: Any? = nil, userInfo aUserInfo: [AnyHashable : Any]? = nil) {
        let notification = NSNotification.Name(rawValue: type.rawValue)
        NotificationCenter.default.post(name: notification, object: anObject, userInfo: aUserInfo)
    }
    
    public static func add(_ observer: Any, type: NotificationIdentifier, action: Selector, object: Any? = nil) {
        let name = NSNotification.Name(rawValue: type.rawValue)
        NotificationCenter.default.addObserver(observer, selector: action, name: name, object: nil)
    }
    
    public static func adds(_ observer: Any, notifications: [(type: NotificationIdentifier, action: Selector)]) {
        for notification in notifications {
            add(observer, type: notification.type, action: notification.action)
        }
    }
    
    public static func screenChange(argument view: Any?) {
         UIAccessibility.post(notification: UIAccessibility.Notification.screenChanged, argument: view)
    }
}

public typealias TravajNotifyService = NotifyService<NotifyServiceType>

public enum NotifyServiceType: StringLiteralType {
    case userReloaded
    case displayedNotificationUpdated
    case hideTabBar
    case showTabBar
    case reloadHome
    case satispayCallbackRecived
    case updateNotificationSettings
}
