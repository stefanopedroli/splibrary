//  Created by Stefano Pedroli on 30/11/2019.

import UIKit

public class Services {
    
//    let networkService: NetworkLayer
    public let biometryService: BiometryService
    public let errorService: ErrorService
    public let preferencesService: PreferencesService
    public let notificationService: NotificationService
    public let networkLayer: NetworkLayer
    public let connectivityService: ConnectivityService
    
    public init(preferencesService: PreferencesService = PreferencesService()) {
        self.preferencesService = preferencesService
        self.networkLayer = NetworkLayer()
        self.errorService = ErrorService()
        self.biometryService = BiometryService()
        self.notificationService = NotificationService(networkLayer: self.networkLayer)
        self.connectivityService = ConnectivityService()
    }
    
    public func clearDevice() {
        // Clear all persisted local settings
        preferencesService.clear()
        // Clear the current data service access token
        // notification service
        notificationService.clear()
        // Network service session, tokens
        networkLayer.clear()
        // clear document files cached
        FileManager.default.clearCacheFiles()
    }
}

public protocol ServicesUser {
    var services: Services { get }
}

public extension ServicesUser {
    var network: NetworkLayer { return services.networkLayer }
    var errorService: ErrorService { return services.errorService }
    var biometryService: BiometryService { return services.biometryService }
    var preferencesService: PreferencesService { return services.preferencesService }
    var connectivityService: ConnectivityService { return services.connectivityService }
}

public extension String {
    static var pushToken: String { return #function }
    static var kHasAlreadyAskedForUserAuthorization: String { return #function }
    static var isNotificationActive: String { return #function }
    static var presentedPermissionViewController: String { return #function }
    static var alreadyRegisterDevice: String { return #function }
    static var deregisterNotificationTokenFailed: String { return #function }
    static var needRequestDeviceToken: String { return #function }
}

public final class UpgradeRequiredManager {
    public static var shared: UpgradeRequiredManager = UpgradeRequiredManager()
    public var updateRequired: Bool = false
}
