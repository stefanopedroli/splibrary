//  Created by Stefano Pedroli on 30/11/2019.

import Foundation

// All changes to the properties of this class are automatically persisted on
// the user defaults.
public final class PreferencesService {
    
    public init() {}
    
    public var user_id: String? {
        get { CacheManager.get(type: String.self, for: .useridkey) }
        set { CacheManager.set(object: newValue, for: .useridkey, shared: true) }
    }
        
    func clear() {
        // nothing here
    }
    
    public var isLogged: Bool {
        return Session.accessToken != nil
    }
    
    public var isWalkthroughtPresented: Bool {
//        get { UserDefaults.standard.bool(forKey: .isWalkthroughtPresentedKey) }
//        set { UserDefaults.standard.set(newValue, forKey: .isWalkthroughtPresentedKey) }
        get { CacheManager.get(type: Bool.self, for: .isWalkthroughtPresentedKey) ?? false }
        set { CacheManager.set(object: newValue, for: .isWalkthroughtPresentedKey, shared: true) }
    }
}

private extension String {
    static var name: String { return #function }
    static var gender: String { return #function }
    static var authentication: String { return #function }
    static var isWalkthroughtPresentedKey: String { return #function }
    static var useridkey: String { return #function }
}
