//  Created by Stefano Pedroli on 17/11/2019.

import UIKit

open class Configurations {
    
    public init() {}
    
    public var defauldCornerRadius: CGFloat = 6
    public var heavyCornerRadius:   CGFloat = 30
    public var titleFontSize:       CGFloat = 22
    public var defaulfFontSize:     CGFloat = 14
    public var smallfFontSize:      CGFloat = 11
    public var menuButtonSize:      CGFloat = 100
    
    public var collectionViewDefaultMargin: CGFloat = 20
    
    public var intelligentBack_closeIcon: UIImage =             UIImage.getIcon(sp_type: .sp_close)
    public var intelligentBack_backIcon: UIImage =              UIImage.getIcon(sp_type: .sp_back)
    public var intelligentBack_closeSecondaryIcon: UIImage =    UIImage.getIcon(sp_type: .sp_back)
    public var check_box_image: UIImage =                       UIImage.getIcon(sp_type: .sp_checkbox_on)
    public var loaderJSONFileName: String?
    
    public var networkConfiguration: Network.Configuration = .init()
    
    public var internationalization: InternazionalizationType = .all
    
    public var sharedUserDefaultBundleName: String?
    
    public var selectionTableViewStyle: UITableViewCell.SelectionStyle = .none
}
