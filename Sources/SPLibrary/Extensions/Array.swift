//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 07/01/2020.
//

import Foundation

public extension Array where Element == String {
    init(numericRange: ClosedRange<Int>) {
        self = []
        for index in numericRange {
            self.append(String(index))
        }
    }
}

public extension Array where Element == Int {
    init(numericRange: ClosedRange<Int>) {
        self = []
        for index in numericRange {
            self.append(index)
        }
    }
}

public extension Collection {
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
