//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 04/12/2019.
//

import Foundation

public extension Array where Element: Equatable {
    mutating func remove(element: Self.Element) {
        self.removeAll { (comparable) -> Bool in
            return element == comparable
        }
    }
    
    mutating func remove(elements: [Self.Element]) {
        for element in elements {
            self.removeAll { (comparable) -> Bool in
                return element == comparable
            }
        }
    }
}

public extension Sequence {
    func group<GroupingType: Hashable>(by key: (Iterator.Element) -> GroupingType) -> [[Iterator.Element]] {
        var groups: [GroupingType: [Iterator.Element]] = [:]
        var groupsOrder: [GroupingType] = []
        forEach { element in
            let key = key(element)
            if case nil = groups[key]?.append(element) {
                groups[key] = [element]
                groupsOrder.append(key)
            }
        }
        return groupsOrder.map { groups[$0]! }
    }
}
