//  Created by Stefano Pedroli on 17/12/2019.

import UIKit

public extension UIButton {
    func underline(style: NSUnderlineStyle = .single, color: UIColor? = nil, underlineColor: UIColor? = nil) {
        guard let text = self.titleLabel?.text else { return }
        let yourAttributesUnderline: [NSAttributedString.Key: Any] = [
            .font: self.titleLabel?.font ?? UIFont.libraryFont(ofSize: .defaulfFontSize),
        .foregroundColor: color ?? self.titleColor(for: .normal) ?? UIColor.mainThemeBlack,
            .underlineStyle: style.rawValue, .underlineColor: underlineColor ?? .mainTheme]
        let attributeString = NSMutableAttributedString(string: text, attributes: yourAttributesUnderline)
        DispatchQueue.main.async {
            self.setAttributedTitle(attributeString, for: .normal)
        }
    }
}

public extension UILabel {
    /// description: This function underline your entire label
    func underline(style: NSUnderlineStyle = .single, color: UIColor? = nil, underlineColor: UIColor? = nil) {
        guard let text = self.text else { return }
        let yourAttributesUnderline: [NSAttributedString.Key: Any] = [
            .font: self.font ?? UIFont.libraryFont(ofSize: .defaulfFontSize),
        .foregroundColor: color ?? self.textColor ?? UIColor.mainThemeBlack,
        .underlineStyle: style.rawValue, .underlineColor: underlineColor ?? .mainTheme]
        let attributeString = NSMutableAttributedString(string: text, attributes: yourAttributesUnderline)
        DispatchQueue.main.async {
            self.attributedText = attributeString
        }
    }
    
    /// description: This function underline only your substring if finded and let other text extactly in his style
    func underline(substring: String, color: UIColor? = nil, textColor: UIColor? = nil) {
        guard let string = text else { return }
        let range = (string as NSString).range(of: substring)
        let attributedString: NSMutableAttributedString
        if let attrText = attributedText {
            attributedString = NSMutableAttributedString(attributedString: attrText)
        } else {
            attributedString = NSMutableAttributedString(string: string)
        }
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: 2, range: range)
        attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: color ?? self.textColor ?? .black, range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: textColor ?? self.textColor ?? .mainThemeBlack, range: range)
        self.attributedText = attributedString
    }
    
    func removeUnderline() {
        guard let string = text else { return }
        let range = (string as NSString).range(of: string)
        guard let attrText = attributedText else { return }
        let attributedString: NSMutableAttributedString = .init(attributedString: attrText)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: 0, range: range)
        self.attributedText = attributedString
    }
}
