//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 25/11/2019.
//

import UIKit

public extension NSAttributedString {
    
    func withBoldText(text: String, font: UIFont? = nil, color: UIColor? = nil, link: String? = nil) -> NSAttributedString {
        let _fontBold = font?.withWeight(.bold) ?? UIFont.libraryFont(ofSize: .defaulfFontSize, weight: .bold)
        let fullString = NSMutableAttributedString(attributedString: self)
        var boldFontAttribute: [NSAttributedString.Key: Any] = [:]
        if let _color = color {
            boldFontAttribute = [NSAttributedString.Key.font: _fontBold,
                                 NSAttributedString.Key.foregroundColor: _color]
        } else {
            boldFontAttribute = [NSAttributedString.Key.font: _fontBold]
        }
        let range = (self.string as NSString).range(of: text)
        fullString.addAttributes(boldFontAttribute, range: range)
        if let _link = link { fullString.addAttribute(.link, value: _link, range: range) }
        return fullString
    }
}

public extension UILabel {
    func bold(text: String, color: UIColor? = nil) {
        guard let labelText = self.text ?? self.attributedText?.string else { return } // questo penso sia inutile
        let boldText = NSAttributedString(string: labelText).withBoldText(text: text, font: self.font, color: color)
        self.attributedText = boldText
    }
    
    func setIcon(image: UIImage, with text: String, size: CGFloat? = nil, adjustYvalue: CGFloat = -2) {
        let attachment = NSTextAttachment()
        attachment.image = image
        attachment.adjustsImageSizeForAccessibilityContentSizeCategory = true
        // ADJUST THIS VALUE IF NEEDED --> "-2"
        attachment.proportionalAspectRatio(height: size ?? self.font.pointSize, adjustYvalue: adjustYvalue)
//        attachment.bounds = CGRect(x: 0, y: adjustYvalue, width: size ?? self.font.pointSize, height: size ?? self.font.pointSize)
        let attachmentStr = NSAttributedString(attachment: attachment)
        let mutableAttributedString = NSMutableAttributedString()
        mutableAttributedString.append(attachmentStr)
        let textString = NSAttributedString(string: "  " + text, attributes: [.font: self.font as Any])
        mutableAttributedString.append(textString)
        self.attributedText = mutableAttributedString
    }
    
    func strikethrough() {
        let attributeString: NSMutableAttributedString = .init(attributedString: self.dynamicAttributedText)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        self.attributedText = attributeString
    }
    
    func removeStrikethrough() {
        let attributeString: NSMutableAttributedString = .init(attributedString: self.dynamicAttributedText)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 0, range: NSMakeRange(0, attributeString.length))
        self.attributedText = attributeString
    }
    
    /// - description: Return attributedText if is presented, otherwise return normal text converted to NSAttributedString
    var dynamicAttributedText: NSAttributedString {
        if let attrText = self.attributedText  {
            return attrText
        } else { return NSAttributedString(string: self.text ?? String()) }
    }
    
    func addSuperScriptText(text: String) {
        guard let fontNormal = self.font else { return }
        let delta: CGFloat = {
            let proportion = (fontNormal.pointSize*70)/100 // 70% of font
            return proportion < 7 ? 7 : proportion // max 7 pt
        }()
        let superFont = fontNormal.withSize(delta)
        let attributeString: NSMutableAttributedString = .init(string: self.text ?? String(), attributes: [.font: fontNormal])
        let superScriptString: NSAttributedString = .init(string: text)
        attributeString.append(superScriptString)
        let range = (attributeString.string as NSString).range(of: superScriptString.string)
        attributeString.setAttributes([.font: superFont, .baselineOffset: delta/2], range: range)
        self.attributedText = attributeString
    }
    
    func addSubScriptText(text: String) {
        guard let fontNormal = self.font else { return }
        let delta: CGFloat = {
            let proportion = (fontNormal.pointSize*70)/100 // 70% of font
            return proportion < 7 ? 7 : proportion // max 7 pt
        }()
        let superFont = fontNormal.withSize(delta)
        let attributeString: NSMutableAttributedString = .init(string: self.text ?? String(), attributes: [.font: fontNormal])
        let superScriptString: NSAttributedString = .init(string: text)
        attributeString.append(superScriptString)
        let range = (attributeString.string as NSString).range(of: superScriptString.string)
        attributeString.setAttributes([.font: superFont, .baselineOffset: -(delta/2)], range: range)
        self.attributedText = attributeString
    }
}

public extension NSTextAttachment {
    func proportionalAspectRatio(height: CGFloat, adjustYvalue: CGFloat) {
        guard let image = self.image else { return }
        let ratio = image.size.width / image.size.height
        self.bounds = CGRect(x: self.bounds.origin.x, y: self.bounds.origin.y + adjustYvalue, width: ratio * height, height: height)
    }
}

public extension NSMutableAttributedString {
    
    @discardableResult func bold(_ text: String, fontSize: CGFloat = .defaulfFontSize) -> NSMutableAttributedString {
        let attrs = [NSAttributedString.Key.font : UIFont.libraryFont(ofSize: fontSize, weight: .bold)]
        let boldString = NSMutableAttributedString(string: text, attributes: attrs)
        append(boldString)
        return self
    }

    @discardableResult func normal(_ text: String, fontSize: CGFloat = .defaulfFontSize) -> NSMutableAttributedString {
        let attrs = [NSAttributedString.Key.font : UIFont.libraryFont(ofSize: fontSize)]
        let normal = NSAttributedString(string: text, attributes: attrs)
        append(normal)
        return self
    }
}

public extension String {
    
    func attributedString(nonBoldRange: NSRange?) -> NSAttributedString {
        let fontSize: CGFloat = .defaulfFontSize
        let attrs = [
            NSAttributedString.Key.font: UIFont.libraryFont(ofSize: fontSize, weight: .bold),
            NSAttributedString.Key.foregroundColor: UIColor.black
        ]
        let nonBoldAttribute = [
            NSAttributedString.Key.font: UIFont.libraryFont(ofSize: fontSize)
        ]
        let attrStr = NSMutableAttributedString(string: self, attributes: attrs)
        if let range = nonBoldRange {
            attrStr.setAttributes(nonBoldAttribute, range: range)
        }
        return attrStr
    }
    
    func withBoltText(text: String, font: UIFont? = nil, color: UIColor? = nil) -> NSAttributedString {
        return NSAttributedString(string: self).withBoldText(text: text, font: font, color: color, link: nil)
    }
    
    // MARK: - HTML
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            let htmlAttributedString = try NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            htmlAttributedString.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: .defaulfFontSize)], range: NSRangeFromString(self))
            return htmlAttributedString
        } catch {
            return NSAttributedString()
        }
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    func withBoldText(boldPartsOfString: Array<NSString>, fontSize: CGFloat) -> NSAttributedString {
        let nonBoldFontAttribute = [NSAttributedString.Key.font: UIFont.libraryFont(ofSize: fontSize)]
        let boldFontAttribute = [NSAttributedString.Key.font: UIFont.libraryFont(ofSize: fontSize, weight: .bold)]
        let boldString = NSMutableAttributedString(string: self as String, attributes:nonBoldFontAttribute)
        for i in 0 ..< boldPartsOfString.count {
            boldString.addAttributes(boldFontAttribute, range: (self as NSString).range(of: boldPartsOfString[i] as String))
        }
        return boldString
    }
    
    /**
     Return the CGSize of a string limited by a width
     - Returns: the CGSize of the string
     */
    static func size(of string: String, with font: UIFont, constrainedTo width: Double) -> CGSize {
        return NSString(string: string).boundingRect(with: CGSize(width: width, height: .greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil).size
    }
}

