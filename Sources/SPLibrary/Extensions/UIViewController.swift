//  Created by Stefano Pedroli on 28/11/2019.

import UIKit

public extension UIViewController {
    
    func addCustomBackButton<T: RawRepresentable>(icon: T) where T.RawValue == String {
        navigationController?.navigationBar.backIndicatorImage = UIImage.getIcon(withType: icon)
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage.getIcon(withType: icon)
        navigationController?.navigationBar.backItem?.accessibilityLabel = "indietro".localized
        navigationController?.navigationBar.topItem?.title = ""
        navigationController?.navigationBar.topItem?.accessibilityLabel = "indietro".localized
    }
    
    func hideBackButton() {
        navigationItem.setHidesBackButton(true, animated: false)
    }
    
    @objc func goRoot() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func back() {
        self.navigationController?.popViewController(animated: true)
    }
    
//    @available(*, deprecated, message: "You can override now both actions separated")
    func addIntelligentBack(action: Selector? = nil) {
        hideBackButton()
        let backButton = getIntelligentBack(action: action)
        // ON TRAVAJ CLOSE ITEM IS ON THE RIGHT, BACK ON THE LEFT
        if self.navigationController?.viewControllers[0] == self {
            navigationItem.rightBarButtonItem = backButton
        } else {
            navigationItem.leftBarButtonItem = backButton
        }
    }
    
    func addClose(position: Navigation.NavigationPosition = .left, action: Selector? = nil) {
        switch position {
        case .left: self.navigationItem.leftBarButtonItem = getNormalClose(action: action)
        case .right: self.navigationItem.rightBarButtonItem = getNormalClose(action: action)
        }
    }
    
    func addNormalClose(position: Navigation.NavigationPosition = .left, action: Selector? = nil) {
        if self.navigationController?.viewControllers[0] == self {
            switch position {
            case .left: self.navigationItem.leftBarButtonItem = getNormalClose(action: action)
            case .right: self.navigationItem.rightBarButtonItem = getNormalClose(action: action)
            }
        }
    }
    
    func addNormalBack(action: Selector? = nil) {
        self.hideBackButton()
        self.navigationItem.leftBarButtonItem = getNormalBack(action: action)
    }
    
    /// - description: "You can override now both actions separated"
    func addIntelligentBack(overrideBackAction: Selector?, overrideCloseAction: Selector?) {
        hideBackButton()
        let backButton = getIntelligentBack(overrideBackAction: overrideBackAction, overrideCloseAction: overrideCloseAction)
        navigationItem.leftBarButtonItem = backButton
    }

    // perform a pop action on UINavigationController stack
    func back(animated: Bool = true) {
        navigationController?.popViewController(animated: animated)
    }

    // perform a modal dismissing
    func closeModal(_ animated: Bool = true, completion: (() -> Void)?) {
        dismiss(animated: animated, completion: completion)
    }
    
    @objc func close() {
        dismiss(animated: true, completion: nil)
    }
    
    func systemAlert(title: String, message: String?, titleAction: String, action: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction.init(title: titleAction, style: .default, handler: action)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}

public extension UIViewController {
    
    func modalPushStyle() {
        let transition = CATransition()
        transition.duration = 0.2
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window?.layer.add(transition, forKey: kCATransition)
    }
    
    /// - description: Function used for replace current viewController with another viewController in navigation stack
    func replaceViewController(with newViewController: UIViewController, animated: Bool) {
        var _viewControllers = self.navigationController?.viewControllers ?? []
        _viewControllers.removeLast()
        _viewControllers.append(newViewController)
        DispatchQueue.main.async {
            self.navigationController?.setViewControllers(_viewControllers, animated: animated)
        }
    }
}

public extension UINavigationController {
    func lastPresenting() -> UIViewController? {
        if self.presentingViewController != nil {
            _ = self.lastPresenting()
            return nil
        } else {
            return self
        }
    }
}

public extension UIViewController {
    func popToThisViewController(animated: Bool, completion: (() -> Void)? = nil) {
        guard let navigation = self.navigationController else { return }
        // take last navigation stack beacause if I dismiss self.presentingViewController, but is not the last nav stack, the last nav stack will remain active.
        let lastPresentingVC = navigation.lastPresenting()
        // dismiss all navigations stack in the middle
        self.presentingViewController?.dismiss(animated: animated, completion: completion)
        // dismiss the last navigation that remain active
        lastPresentingVC?.dismiss(animated: animated, completion: completion)
        //back to current vc
        navigation.popToViewController(self, animated: true)
    }
}

public extension UIApplication {
    // return the top view controller from UINavigationController or UITabBarController stacks.
    static func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
