//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 16/11/2019.
//

import UIKit

// MARK: - Loading

// MARK: - StoryboardInitialViewController
public protocol StoryboardRootInstatiable: class {
    static var sceneStoryboard: UIStoryboard { get }
}

public extension StoryboardRootInstatiable {
    static var sceneStoryboard: UIStoryboard {
        return UIStoryboard(name: String(describing: self), bundle: Bundle(for: self))
    }
}

public extension StoryboardRootInstatiable where Self: UIViewController {
    
    static func instantiate() -> Self {
        guard let viewController = sceneStoryboard.instantiateInitialViewController() else {
            preconditionFailure("You forgot to set the \"Is initial view controller\" flag in your storyboard file \(sceneStoryboard).")
        }
        guard let typedViewController = viewController as? Self else {
            preconditionFailure("You forgot/mistook the type for the initial view controller in the storyboard file \(sceneStoryboard). You were expecting a view controller of type \(type(of: self)).")
        }
        return typedViewController
    }
}

// MARK: - StoryboardViewController
public protocol StoryboardInstantiable: class {
    static var sceneStoryboard: UIStoryboard { get }
    static var sceneIdentifier: String { get }
}

public extension StoryboardInstantiable {
    static var sceneIdentifier: String {
        return String(describing: self)
    }
}

public extension StoryboardInstantiable where Self: UIViewController {
    
    static func instantiate() -> Self {
        guard let viewController = sceneStoryboard.instantiateViewController(withIdentifier: sceneIdentifier) as? Self else {
            preconditionFailure("You forgot/mistook the type for the view controller with identifier \(sceneIdentifier) in the storyboard file \(sceneStoryboard). You were expecting a view controller of type \(type(of: self)).")
        }
        return viewController
    }
}

// MARK: - NibLoadable
public protocol NibLoadable: class {
    static var nib: UINib { get }
}

public extension NibLoadable {
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
}

public extension NibLoadable where Self: UIView {
    static func loadFromNib() -> Self {
        guard let view = nib.instantiate(withOwner: nil, options: nil).first as? Self else {
            preconditionFailure("You made a mistake while setting the type for your view in the nib file \(nib). You were expecting a view of type \(type(of: self))")
        }
        return view
    }
}

// MARK: - Reusing

// MARK: - Reusable and NibReusable
public protocol Reusable: class {
    static var reuseIdentifier: String { get }
}

public typealias NibReusable = Reusable & NibLoadable

public extension Reusable {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

// MARK: - UITableView
public extension UITableView {
    
    // MARK: - Cells
    final func register<T: UITableViewCell>(cellType: T.Type) where T: Reusable & NibLoadable {
        self.register(cellType.nib, forCellReuseIdentifier: cellType.reuseIdentifier)
    }
    
    final func register<T: UITableViewCell>(cellType: T.Type) where T: Reusable {
        self.register(cellType.self, forCellReuseIdentifier: cellType.reuseIdentifier)
    }
    
    final func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath, cellType: T.Type = T.self) -> T where T: Reusable {
        guard let cell = self.dequeueReusableCell(withIdentifier: cellType.reuseIdentifier, for: indexPath) as? T else {
            preconditionFailure("Failed to dequeue a cell with identifier \(cellType.reuseIdentifier) matching type \(cellType). Check that the reuseIdentifier is set properly in your XIB/Storyboard and that you registered the cell for reuse.")
        }
        cell.selectionStyle = SPLibrary.configurations.selectionTableViewStyle
        return cell
    }
    
    // MARK: - Header/footer views
    final func register<T: UITableViewHeaderFooterView>(headerFooterViewType: T.Type)
        where T: Reusable & NibLoadable {
            self.register(headerFooterViewType.nib, forHeaderFooterViewReuseIdentifier: headerFooterViewType.reuseIdentifier)
    }
    
    final func register<T: UITableViewHeaderFooterView>(headerFooterViewType: T.Type) where T: Reusable {
        self.register(headerFooterViewType.self, forHeaderFooterViewReuseIdentifier: headerFooterViewType.reuseIdentifier)
    }
    
    final func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>(_ viewType: T.Type = T.self) -> T? where T: Reusable {
        guard let view = self.dequeueReusableHeaderFooterView(withIdentifier: viewType.reuseIdentifier) as? T? else {
            preconditionFailure("Failed to dequeue a header/footer with identifier \(viewType.reuseIdentifier) matching type \(viewType). Check that the reuseIdentifier is set properly in your XIB/Storyboard and that you registered the header/footer for reuse.")
        }
        return view
    }
}

// MARK: - UICollectionView
public extension UICollectionView {
    
    // MARK: - Cells
    final func register<T: UICollectionViewCell>(cellType: T.Type) where T: Reusable & NibLoadable {
        self.register(cellType.nib, forCellWithReuseIdentifier: cellType.reuseIdentifier)
    }
    
    final func register<T: UICollectionViewCell>(cellType: T.Type) where T: Reusable {
        self.register(cellType.self, forCellWithReuseIdentifier: cellType.reuseIdentifier)
    }
    
    final func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath, cellType: T.Type = T.self) -> T where T: Reusable {
        let cell = self.dequeueReusableCell(withReuseIdentifier: cellType.reuseIdentifier, for: indexPath)
        guard let typedCell = cell as? T else {
            preconditionFailure("Failed to dequeue a cell with identifier \(cellType.reuseIdentifier) matching type \(cellType). Check that the reuseIdentifier is set properly in your XIB/Storyboard and that you registered the cell for reuse.")
        }
        return typedCell
    }
    
    // MARK: - Supplementary views
    final func register<T: UICollectionReusableView>(supplementaryViewType: T.Type, ofKind elementKind: String) where T: Reusable & NibLoadable {
        self.register(supplementaryViewType.nib, forSupplementaryViewOfKind: elementKind, withReuseIdentifier: supplementaryViewType.reuseIdentifier)
    }
    
    final func register<T: UICollectionReusableView>(supplementaryViewType: T.Type, ofKind elementKind: String) where T: Reusable {
        self.register(supplementaryViewType.self, forSupplementaryViewOfKind: elementKind, withReuseIdentifier: supplementaryViewType.reuseIdentifier)
    }
    
    final func dequeueReusableSupplementaryView<T: UICollectionReusableView> (ofKind elementKind: String, for indexPath: IndexPath, viewType: T.Type = T.self) -> T where T: Reusable {
        let view = self.dequeueReusableSupplementaryView(ofKind: elementKind, withReuseIdentifier: viewType.reuseIdentifier, for: indexPath)
        guard let typedView = view as? T else {
            preconditionFailure("Failed to dequeue a supplementary view with identifier \(viewType.reuseIdentifier) matching type \(viewType). Check that the reuseIdentifier is set properly in your XIB/Storyboard and that you registered the supplementary view for reuse.")
        }
        return typedView
    }
}
