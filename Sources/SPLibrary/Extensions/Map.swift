//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 22/09/2020.
//

import Foundation
import MapKit

public extension MKMapView {
    
    func openMapFrom(mapOptions: MapEventOptions, pin: Bool = true, animated: Bool = true) {
        if pin {
            let annotation = MKPointAnnotation()
            annotation.title = mapOptions.title
            //You can also add a subtitle that displays under the annotation such as
            annotation.subtitle = mapOptions.subtitle
            annotation.coordinate = mapOptions.coordinate
            self.addAnnotation(annotation)
        }
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let region = MKCoordinateRegion(center: mapOptions.coordinate, span: span)
        DispatchQueue.main.async {
            self.setRegion(region, animated: animated)
        }
    }
    
    func load(latitude: CLLocationDegrees, longitude: CLLocationDegrees, animated: Bool = true) {
        let location: CLLocationCoordinate2D = .init(latitude: latitude, longitude: longitude)
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let region = MKCoordinateRegion(center: location, span: span)
        DispatchQueue.main.async {
            self.setRegion(region, animated: animated)
        }
    }
    
    func loadCoordinatesFrom(address: String, areaDistance: Double = 500, title: String? = nil, subtitle: String? = nil, pin: Bool = true, animated: Bool = true, completion: @escaping (_ mapOptions: MapEventOptions?) -> Void) {
        Map.getCoordinateFrom(address: address) { (coordinate) in
            if let coordinate = coordinate {
                let options: MapEventOptions = .init(title: title, subtitle: subtitle, location: nil, coordinate: coordinate, regionArea: areaDistance)
                self.openMapFrom(mapOptions: options, pin: pin, animated: animated)
                completion(options)
            } else { completion(nil) }
        }
    }
    
    func loadLocationFrom(address: String, areaDistance: Double = 500, title: String? = nil, subtitle: String? = nil, pin: Bool = true, animated: Bool = true, completion: @escaping (_ mapOptions: MapEventOptions?) -> Void) {
        Map.getLocationFrom(address: address) { (location) in
            if let _location = location {
                let options: MapEventOptions = .init(title: title, subtitle: subtitle, location: _location, coordinate: _location.coordinate, regionArea: areaDistance)
                self.openMapFrom(mapOptions: options, pin: pin, animated: animated)
                completion(options)
            } else { completion(nil) }
        }
    }
    
    func showRouteOnMap(pickupCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D) {

        let request = MKDirections.Request()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: pickupCoordinate, addressDictionary: nil))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: destinationCoordinate, addressDictionary: nil))
        request.requestsAlternateRoutes = true
        request.transportType = .automobile
        
        let directions = MKDirections(request: request)
        
        directions.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else { return }
            
            //for getting just one route
            if let route = unwrappedResponse.routes.first {
                //show on map
                self.addOverlay(route.polyline)
                //set the map area to show the route
                self.setVisibleMapRect(route.polyline.boundingMapRect, edgePadding: UIEdgeInsets.init(top: 80.0, left: 20.0, bottom: 100.0, right: 20.0), animated: true)
            }
            
            //if you want to show multiple routes then you can get all routes in a loop in the following statement
            //for route in unwrappedResponse.routes {}
        }
    }
}

open class Map {
    
    public class func getCoordinateFrom(address: String, completion: @escaping (_ location: CLLocationCoordinate2D?) -> Void) {
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(address) { (placemarks, error) in
            guard let placemarks = placemarks, let location = placemarks.first?.location else {
                completion(nil); return
            }
            completion(location.coordinate)
        }
    }
    
    public class func getLocationFrom(address: String, completion: @escaping (_ coordinate: CLLocation?) -> Void) {
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(address) { (placemarks, error) in
            guard let placemarks = placemarks, let location = placemarks.first?.location else {
                completion(nil); return
            }
            completion(location)
        }
    }
    
    public class func open(mapOptions: MapEventOptions) {
        let latitude: CLLocationDegrees = mapOptions.coordinate.latitude
        let longitude: CLLocationDegrees = mapOptions.coordinate.longitude
        let regionDistance: CLLocationDistance = mapOptions.regionArea
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
          MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
          MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = mapOptions.title
        mapItem.openInMaps(launchOptions: options)
    }
}

public struct MapEventOptions {
    
    public var title: String?
    public var subtitle: String?
    public var location: CLLocation?
    public var coordinate: CLLocationCoordinate2D
    public var regionArea: Double = 500
    
    public init(title: String?, subtitle: String?, location: CLLocation?, coordinate: CLLocationCoordinate2D, regionArea: Double = 500) {
        self.title = title
        self.subtitle = subtitle
        self.location = location
        self.coordinate = coordinate
        self.regionArea = regionArea
    }
}
