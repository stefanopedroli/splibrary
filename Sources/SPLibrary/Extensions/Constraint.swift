//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 08/04/2020.
//

import UIKit

public extension UIView {

    func setMargins(top: CGFloat, left: CGFloat, right: CGFloat, bottom: CGFloat, toItem: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.topAnchor.constraint(equalTo: toItem.topAnchor, constant: top).isActive = true
        self.bottomAnchor.constraint(equalTo: toItem.bottomAnchor, constant: bottom).isActive = true
        self.leadingAnchor.constraint(equalTo: toItem.leadingAnchor, constant: left).isActive = true
        self.trailingAnchor.constraint(equalTo: toItem.trailingAnchor, constant: right).isActive = true
    }
}

// other methods
//placeholderLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 15).isActive = true
