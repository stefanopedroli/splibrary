//  Created by Stefano Pedroli on 20/11/2019.

import UIKit

public struct UserNotificationPayload {
    
    let isAppearing: Bool
    let endFrame: CGRect
    let startFrame: CGRect
    let animationDuration: TimeInterval
    let animationCurve: UIView.AnimationCurve
    
    init?(notification: Notification) {
        guard let userInfo = notification.userInfo,
            let endFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect,
            let startFrame = userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? CGRect,
            let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval,
            let animationCurveRawValue = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? Int,
            let animationCurve = UIView.AnimationCurve(rawValue: animationCurveRawValue)
            else {
                return nil
        }
        self.isAppearing = (
            notification.name == UIResponder.keyboardWillShowNotification ||
            notification.name == UIResponder.keyboardDidShowNotification
        )
        
        self.endFrame = endFrame
        self.startFrame = startFrame
        self.animationDuration = animationDuration
        self.animationCurve = animationCurve
    }
}

public extension UIView {
    static func animateAlongsideKeyboard(_ payload: UserNotificationPayload, delay: TimeInterval = 0, animations: @escaping () -> Void, completion: ((Bool) -> Void)? = nil) {
        let options = UIView.AnimationOptions(curve: payload.animationCurve)
        UIView.animate(withDuration: payload.animationDuration, delay: delay, options: options, animations: animations, completion: completion)
    }
}

public extension UIViewController {
    
    func automaticallyKeyboard(spacer: Bool = false) {
        let keyboardScrollSelector: Selector = spacer ? #selector(performKeyboardScrollSpacer(notification:)) : #selector(performKeyboardScrol(notification:))
        
        NotificationCenter.default.addObserver(self, selector: keyboardScrollSelector, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(UIViewController.addDismissKeyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: keyboardScrollSelector, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func performKeyboardScrol(notification: Notification) {
        guard let payload: UserNotificationPayload = UserNotificationPayload(notification: notification) else { return }
        for view in self.view.subviews {
            if let scroll = view as? UIScrollView {
                let maxFrame = scroll.frame.height
                let maxContentFrame = scroll.contentSize.height
                
                let contentInset: CGFloat
                if maxFrame > maxContentFrame {
                    let keyboardDiff = payload.endFrame.height - (maxFrame - maxContentFrame)
                    if keyboardDiff > 0 {
                        contentInset = keyboardDiff
                    } else {
                        contentInset = 0.0
                    }
                } else {
                    contentInset = payload.endFrame.height
                }
                UIView.animateAlongsideKeyboard(payload, animations: { scroll.contentInset.bottom = payload.isAppearing ? contentInset : 0.0 })
                return
            }
        }
    }
    
    @objc func performKeyboardScrollSpacer(notification: Notification) {
        guard let payload: UserNotificationPayload = UserNotificationPayload(notification: notification) else { return }
        // no scroll view here
        let keyboardActualHeight = payload.endFrame.height - (view.safeAreaInsets.bottom - additionalSafeAreaInsets.bottom)
        additionalSafeAreaInsets.bottom = payload.isAppearing ? keyboardActualHeight : 0
        UIView.animateAlongsideKeyboard(payload, animations: { [weak self] in self?.view.layoutIfNeeded() })
    }
}

public extension UIView.AnimationOptions {
    
    init(curve: UIView.AnimationCurve) {
        switch curve {
        case .easeIn:
            self = .curveEaseIn
        case .easeOut:
            self = .curveEaseOut
        case .easeInOut:
            self = .curveEaseInOut
        default:
            self = .curveLinear
        }
    }
}

public extension UIViewController {
    @objc func addDismissKeyboard() {
        let action = #selector(UIViewController.dismissKeyboardTapped(_:))
        let recognizer = UITapGestureRecognizer(target: self, action: action)
        view.addGestureRecognizer(recognizer)
    }

    @objc private func dismissKeyboardTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
        view.removeGestureRecognizer(sender)
    }
}

public extension UIView {
    
    func automaticallyKeyboard() {
        let selector = #selector(performKeyboardScrollUIView)
        NotificationCenter.default.addObserver(self, selector: selector, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(addDismissUIViewKeyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: selector, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func performKeyboardScrollUIView(notification: Notification) {
        guard let payload: UserNotificationPayload = UserNotificationPayload(notification: notification) else { return }
        let mainView: UIView = (self as? AlertProtocol)?.mainView ?? self
//        let maxFrame = mainView.frame.height
//        let maxContentFrame = self.bounds.height
//
//        let contentInset: CGFloat
//        if maxFrame > maxContentFrame {
//            let keyboardDiff = payload.endFrame.height - (maxFrame - maxContentFrame)
//            if keyboardDiff > 0 {
//                contentInset = keyboardDiff
//            } else {
//                contentInset = 0.0
//            }
//        } else {
//            contentInset = payload.endFrame.height
//        }
        UIView.animateAlongsideKeyboard(payload, animations: {
            let positionControl = mainView.center == self.center
            if payload.isAppearing && positionControl {
                mainView.frame.origin.y -= payload.endFrame.height/2
            } else {
                mainView.center = self.center
            }
//            mainView.frame.origin.y = payload.isAppearing ? contentInset : 0.0
        })
    }
    
    @objc internal func addDismissUIViewKeyboard() {
        let action = #selector(dismissUIViewKeyboardTapped(_:))
        let recognizer = UITapGestureRecognizer(target: self, action: action)
        self.addGestureRecognizer(recognizer)
    }

    @objc private func dismissUIViewKeyboardTapped(_ sender: UITapGestureRecognizer) {
        self.endEditing(true)
        self.removeGestureRecognizer(sender)
    }
}
