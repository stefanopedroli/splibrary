//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 14/01/2020.
//

import Foundation

extension URL: ExpressibleByStringLiteral {
    
    public typealias StringLiteralType = UnicodeScalarLiteralType
    public typealias UnicodeScalarLiteralType = ExtendedGraphemeClusterLiteralType
    public typealias ExtendedGraphemeClusterLiteralType = String
    
    public init(stringLiteral value: StringLiteralType) {
        self.init(unicodeScalarLiteral: value)
    }
    
    public init(unicodeScalarLiteral value: UnicodeScalarLiteralType) {
        self.init(extendedGraphemeClusterLiteral: value)
    }
    
    public init(extendedGraphemeClusterLiteral value: ExtendedGraphemeClusterLiteralType) {
        precondition(URL(string: value as String) != nil, "The manually typed URL \(value) is an invalid URL.")
        self.init(string: value)!
    }
}
