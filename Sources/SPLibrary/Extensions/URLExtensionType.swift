//  Created by Stefano Pedroli on 20/11/2019.

import Foundation
import CoreServices

let path: String =  "https://www.computerhope.com/issues/ch001789.htm"

public enum URLExtensionType: StringLiteralType {
    
    // Document type
    case pdf // Portable Document Format (PDF)
    case txt // Plain text file (TXT)
    case doc, docx // Microsoft Word file
    case odt // OpenOffice Writer document file
    case rtf // Rich Text Format
    case tex // A LaTeX document file
    case wks, wps // Microsoft Works file
    case wpd // WordPerfect document
    
    // image type
    case png // Portable Network Graphic (PNG)
    case tiff, tif // Tagged Image File Format (TIFF)
    case jpeg, jpg // Joint Photographic Experts Group (JPEG)
    case gif // Graphic Interchange Format (GIF)
    case bmp, bmpf // Windows Bitmap Format (DIB)
    case ico // Windows Icon Format
    case cur // Windows Cursor
    case xbm // XWindow bitmap
    
    // Image type but not conform to apple IMageView
    case svg // Scalable Vector Graphics file
    case ai // Adobe Illustrator file
    case psd // Adobe Photoshop File
    
    // Video file formats
    case _3g2 = "3g2", _3GPP2 = "3gpp2" // multimedia file
    case _3gp = "3gp", _3GPP = "3gpp" // multimedia file
    case avi // AVI file
    case flv // Adobe Flash file
    case h264 // H.264 video file
    case m4v // Apple MP4 video file
    case mkv // Matroska Multimedia Container
    case mov // Apple QuickTime movie file
    case mp4 // MPEG4 video file
    case mpg, mpeg // MPEG video file
    case rm // RealMedia file
    case swf // Shockwave flash file
    case vob // DVD Video Object
    case wmv // Windows Media Video file
    
    // System related file formats
    case bak // Backup file
    case cab // Windows Cabinet file
    case cfg // Configuration file
    case cpl // Windows Control panel file
    case dll // DLL file
    case dmp // Dump file
    case drv // Device driver file
    case icns // macOS X icon resource file
    case ini // Initialization file
    case lnk // Windows shortcut file
    case msi // Windows installer package
    case sys // Windows system file
    case tmp // Temporary file
    
    // Spreadsheet file formats
    case ods // OpenOffice Calc spreadsheet file
    case xlr // Microsoft Works spreadsheet file
    case xls // Microsoft Excel file
    case xlsx // Microsoft Excel Open XML spreadsheet file
    
    // Programming files
    case c // C and C++ source code file
    case `class` // Java class file
    case cpp // C++ source code file
    case cs // Visual C# source code file
    case h // C, C++, and Objective-C header file
    case java // Java Source code file
    case sh // Bash shell script
    case swift // Swift source code file
    case vb // Visual Basic file
    
    case unknown
    
    public var category: URLExtensionCategory {
        switch self {
        case .png, .tif, .tiff, .jpeg, .jpg, .gif, .bmp, .bmpf, .ico, .cur, .xbm, .svg, .ai, .psd: return .image
        case .pdf, .txt, .doc, .docx, .odt, .rtf, .tex, .wks, .wps, .wpd: return .document
        case ._3g2, ._3gp, ._3GPP, ._3GPP2, .avi, .flv, .h264, .m4v, .mkv, .mov, .mp4, .mpg, .mpeg, .rm, .swf, .vob, .wmv : return .video
        case .bak, .cab, .cfg, .cpl, .dll, .dmp, .drv, .icns, .ini, .lnk, .msi, .sys, .tmp: return .systemReleatedFile
        case .ods, .xlr, .xls, .xlsx: return .spreadsheet
        case .c, .class, .cpp, .cs, .h, .java, .sh, .swift, .vb: return .programmingFile
        default: return .unknown
        }
    }
    
    public var mimetype: String? {
        let fileExtension: CFString = self.rawValue as CFString
        guard let extUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, nil)?.takeUnretainedValue() else { return nil }
        guard let mimeUTI = UTTypeCopyPreferredTagWithClass(extUTI, kUTTagClassMIMEType) else { return nil }
        return mimeUTI.takeRetainedValue() as String
    }
}

public enum URLExtensionCategory {
    case image
    case document // Word processor and text file formats
    case video // Video file formats
    case systemReleatedFile
    case spreadsheet
    case programmingFile
    
    case unknown
}

public extension String {

    var fileName: String {
        return NSURL(fileURLWithPath: self).deletingPathExtension?.lastPathComponent ?? ""
    }

    var fileExtension: String {
        return NSURL(fileURLWithPath: self).pathExtension?.lowercased() ?? ""
    }
    
    var fileExtensionType: URLExtensionType {
        return URLExtensionType(rawValue: fileExtension.lowercased()) ?? .unknown
    }
    
    func mimeType() -> String? {
        let fileExtension: CFString = self as CFString
        guard let extUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, nil)?.takeUnretainedValue() else { return nil }
        guard let mimeUTI = UTTypeCopyPreferredTagWithClass(extUTI, kUTTagClassMIMEType) else { return nil }
        return mimeUTI.takeRetainedValue() as String
    }
}

public extension URL {
    var type: URLExtensionType {
        return URLExtensionType(rawValue: self.pathExtension.lowercased()) ?? .unknown
    }
    
    var mimetype: String? {
        return self.absoluteString.fileExtensionType.mimetype
    }
}
