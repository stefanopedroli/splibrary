//  Created by Stefano Pedroli on 28/01/2020.

import UIKit

public extension UILabel {
    
    /// description: This function stilize only your substring by font weight and color
    func color(substring: String, weight: UIFont.Weight? = nil, color: UIColor, range: NSRange? = nil) {
        guard let string = text else { return }
        let range = range ?? (string as NSString).range(of: substring)
        let attributedString: NSMutableAttributedString
        if let attrText = attributedText {
            attributedString = NSMutableAttributedString(attributedString: attrText)
        } else {
            attributedString = NSMutableAttributedString(string: string)
        }
        
        if let customWeight = weight {
            let newFont = self.font.withWeight(customWeight)
            attributedString.addAttribute(NSAttributedString.Key.font, value: newFont, range: range)
        }
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        self.attributedText = attributedString
    }
}
