//  Created by Stefano Pedroli on 20/01/2020.

import UIKit

public extension CABasicAnimation {
    convenience init(keyPath: CABasicAnimationType) {
        self.init(keyPath: keyPath.rawValue)
    }
    
    enum CABasicAnimationType: String {
        case position = "position"
        case rotation = "transform.rotation"
        case opacity = "opacity"
        case transform = "transform"
    }
}

public extension CALayer {
    func addAnimation(_ anim: CABasicAnimation) {
        self.add(anim, forKey: anim.keyPath)
    }
    
    func removeAnimation(_ anim: CABasicAnimation) {
        guard let keyPath: String = anim.keyPath else { return }
        self.removeAnimation(forKey: keyPath)
    }
}

public extension CABasicAnimation {
    static func createAnimtion(key: CABasicAnimationType, fromValue: Any?, toValue: Any?, duration: CFTimeInterval = 0, autoreverses: Bool = false, delay: TimeInterval = 0, fillMode: CAMediaTimingFillMode = .removed, delegate: CAAnimationDelegate) -> CABasicAnimation {
        let animation = CABasicAnimation(keyPath: key)
        animation.fromValue = fromValue
        animation.toValue = toValue
        animation.duration = duration
        animation.autoreverses = autoreverses
        animation.beginTime = CACurrentMediaTime() + delay
        animation.fillMode = fillMode
        animation.isRemovedOnCompletion = false
        animation.delegate = delegate
        return animation
    }
    
    static func moveObject(fromObject: UIView, direction: CADirection, withValue: CGFloat, delegate: CAAnimationDelegate) -> CABasicAnimation {
        let fromValue: NSValue = NSValue(cgPoint: CGPoint(x: fromObject.center.x, y: fromObject.center.y))
        var toValue: NSValue?
        switch direction {
        case .top:
            toValue = NSValue(cgPoint: CGPoint(x: fromObject.center.x, y: fromObject.center.y - withValue))
        case .bottom:
            toValue = NSValue(cgPoint: CGPoint(x: fromObject.center.x, y: fromObject.center.y + withValue))
        case .left:
            toValue = NSValue(cgPoint: CGPoint(x: fromObject.center.x - withValue, y: fromObject.center.y))
        case .right:
            toValue = NSValue(cgPoint: CGPoint(x: fromObject.center.x + withValue, y: fromObject.center.y + withValue))
        }
        return CABasicAnimation.createAnimtion(key: .position, fromValue: fromValue, toValue: toValue, fillMode: .forwards, delegate: delegate)
    }
    
    enum CADirection {
        case top
        case bottom
        case left
        case right
    }
}

public extension UIView {
    
    func moveObject(from object: UIView?, direction: CABasicAnimation.CADirection, withValue: CGFloat, completion: ((Bool) -> Void)? = nil) {
        UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
            var transform = CGAffineTransform()
            let x: CGFloat = 0
            let y: CGFloat = 0
            switch direction {
            case .top:
                transform = CGAffineTransform(translationX: x, y: y - withValue)
            case .bottom:
                transform = CGAffineTransform(translationX: x, y: y + withValue)
            case .left:
                transform = CGAffineTransform(translationX: x - withValue, y: y)
            case .right:
                transform = CGAffineTransform(translationX: x + withValue, y: y)
            }
            self.transform = transform
        }, completion: nil)
    }
    
    func rotate(value: CGFloat) {
        UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
            self.transform = self.transform.rotated(by: value)
        }, completion: nil)
    }
    
    func rotate(grade: Grade, mode: RotationMode = .clockwise) {
        var value: CGFloat = 0
        switch grade {
        case .grade_90: value = mode == .clockwise ? (CGFloat.pi / 2) : -(CGFloat.pi / 2)
        case .grade_180: value = .pi
        case .grade_270: value = mode == .clockwise ? -(CGFloat.pi / 2) : (CGFloat.pi / 2)
        case .full: value = (.pi * 2)
        }
        self.rotate(value: value)
    }
    
    func expand(from viewController: UIViewController, completion: ((Bool) -> Void)?) {
        let viewHeight = viewController.view.frame.height
        UIView.animate(withDuration: 0.5, animations: {
            self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: viewHeight, height: viewHeight)
            self.alpha = 0
        }, completion: completion)
    }
    
    func flip() {
        UIView.transition(with: self, duration: 0.8, options: .transitionFlipFromRight, animations: {
            // none
        }, completion: nil)
    }
    
    func fade(type: FadeType, duration: CGFloat, completion: ((_: Bool) -> Swift.Void)? = nil) {
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: .showHideTransitionViews, animations: {
            switch type {
            case .in: self.alpha = 1
            case .out: self.alpha = 0
            case .custom(let alpha): self.alpha = alpha
            }
        }, completion: completion)
    }
    
    enum FadeType {
        case `in`
        case out
        case custom(alpha: CGFloat)
    }
    
    enum Grade {
        case grade_90
        case grade_180
        case grade_270
        case full
    }
    
    enum RotationMode {
        case clockwise
        case counterclockwise
    }
}
