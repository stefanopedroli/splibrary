//  Created by Stefano Pedroli on 16/04/2020.

import UIKit

public extension UIAlertController {
    
    convenience init(title: String?, message: String?, preferredStyle: UIAlertController.Style, tintColor: UIColor) {
        self.init(title: title, message: message, preferredStyle: preferredStyle)
        self.view.tintColor = tintColor
    }
    
    func addActions(elements: [UIAlertAction]) {
        for element in elements {
            self.addAction(element)
        }
    }
    
    /// - description: Call it after you add all actions
    func setTintColorExceptCancelAction(color: UIColor) {
        for action in self.actions {
            if action.style != .cancel {
                action.setValue(color, forKey: "titleTextColor")
            }
        }
    }
}

public extension UIAlertAction{
    
    @NSManaged var image : UIImage?

    convenience init(title: String?, style: UIAlertAction.Style, image : UIImage?, handler: ((UIAlertAction) -> Swift.Void)? = nil){
        self.init(title: title, style: style, handler: handler)
        self.image = image
    }
}

public extension UIAlertAction {
    static var cancel: UIAlertAction {
        let cancelAction = UIAlertAction(title: "CANCEL".localizedInternal, style: .cancel, handler: nil)
        cancelAction.setValue(UIColor.mainThemeRed, forKey: "titleTextColor")
        return cancelAction
    }
}
