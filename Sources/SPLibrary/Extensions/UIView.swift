//  Created by Stefano Pedroli on 20/11/2019.

import UIKit

public extension Optional where Wrapped: UIView {
    func safeRemoveFromSuperview() {
        guard let `self` = self else {
            return
        }
        self.removeFromSuperview()
    }
}

public extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.delegate = self
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func rotate(angle: CGFloat) {
        let radians = angle / 180.0 * CGFloat.pi
        let rotation = self.transform.rotated(by: radians)
        self.transform = rotation
    }
    
    var parentViewController: UIViewController? {
        return findViewController()
    }
    
    private func findViewController() -> UIViewController? {
        if let nextResponder = self.next as? UIViewController {
            return nextResponder
        } else if let nextResponder = self.next as? UIView {
            return nextResponder.findViewController()
        } else {
            return nil
        }
    }
    
    func removeAllSubviews() {
        for view in self.subviews {
            view.removeFromSuperview()
        }
    }
    
    func findSubview(by tag: Int) -> UIView? {
        for view in self.subviews {
            if view.tag == tag { return view }
        }
        return nil
    }
    
    func firstViewOfType<T: UIView>(for type: T.Type) -> T? {
        for view in self.subviews {
            if let findedView = view as? T {
                return findedView
            } else if !view.subviews.isEmpty, let finedSubview = view.firstViewOfType(for: T.self) {
                return finedSubview
            }
        }
        return nil
    }
    
    func replaceSubview(newView: UIView, at tag: Int) {
        if let oldView = self.findSubview(by: tag) {
            replaceSuview(new: newView, old: oldView)
        } else {
            self.addSubview(newView)
        }
    }
    
    func replaceSuview(new: UIView, old: UIView) {
        for (index, element) in self.subviews.enumerated() {
            if element == old {
                self.insertSubview(new, at: index)
                element.removeFromSuperview()
                break
            }
        }
    }
}

public extension UIStackView {
    @discardableResult func removeAllArrangedSubviews() -> [UIView] {
        let removedSubviews = arrangedSubviews.reduce([]) { (removedSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            NSLayoutConstraint.deactivate(subview.constraints)
            subview.removeFromSuperview()
            return removedSubviews + [subview]
        }
        return removedSubviews
    }
}
