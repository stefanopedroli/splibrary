//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 19/11/2019.
//

import Foundation

import Foundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

extension Int {
    static var maxPhoneLength: Int { return 14 }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l >= r
    default:
        return !(lhs < rhs)
    }
}

func matchesForRegexInText(_ regex: String, text: String) -> [String] {
    do {
        let regex = try NSRegularExpression(pattern: regex, options: [])
        let nsString = text as NSString
        let results = regex.matches(in: text,
                                    options: [], range: NSMakeRange(0, nsString.length))
        return results.map { nsString.substring(with: $0.range)}
    } catch let error as NSError {
        print("invalid regex: \(error.localizedDescription)")
        return []
    }
}

final public class MobilePhoneValidator {
    
    static let minNumbers = 1
    static var prefixes : [String] = []
    
    public enum MobilePhoneStatus {
        case validItalian
        case validInternational
        case invalidItalian
        case invalidInternational
        case invalidInternationalPrefix
        case invalid
        case noPrefixes
    }
    
    class func isPhoneNumber(_ number : String) -> Bool {
        let res = matchesForRegexInText("^[(\\+)]?[0-9]{2,\(String(Int.maxPhoneLength))}$", text: number)
        return !res.isEmpty
    }
    
    class func isValidItalianMobilePhone(_ number : String) -> MobilePhoneStatus {
        let phone = number as NSString
        guard phone.length == 9 || phone.length == 10 else {
            return .invalidItalian
        }
        return  phone.substring(with: NSMakeRange(0, 1)) == "3" ? .validItalian : .invalidItalian
    }
    
    typealias PhoneComponents = (MobilePhoneStatus, String?, String?)
    
    @discardableResult
    class func checkNumber(_ number : String , completion: (_ status : MobilePhoneStatus, _ prefix : String?, _ number : String?) -> () = { _, _, _ in }) -> PhoneComponents {
        guard isPhoneNumber(number) == true else {
            completion(MobilePhoneStatus.invalid, nil, nil)
            return (MobilePhoneStatus.invalid, nil, nil)
        }
        
        let phone = number as NSString
        var CC : String? = nil
        var NDC_SN : String? = nil
        //has prefix
        if phone.range(of: "00").location == 0 || phone.range(of: "+").location == 0 {
            guard phone.length > 4 else {
                completion(MobilePhoneStatus.invalid, nil, nil)
                return (MobilePhoneStatus.invalid, nil, nil)
            }
            
            if phone.range(of: "00").location == 0 {
                CC = phone.substring(with: NSMakeRange(0, 4))
            } else {
                CC = phone.substring(with: NSMakeRange(0, 3))
            }
            
            switch CC {
            case .some("0039"), .some("+39"):
                guard let cc = CC else { return (MobilePhoneStatus.invalid, nil, nil) }
                let ndc_sin = phone.replacingOccurrences(of: cc, with: "")
                NDC_SN = ndc_sin
                switch isValidItalianMobilePhone(ndc_sin) {
                case .validItalian:
                    completion(MobilePhoneStatus.validItalian, "0039", NDC_SN)
                    return (MobilePhoneStatus.validItalian, "0039", NDC_SN)
                default :
                    completion(MobilePhoneStatus.invalidItalian, nil, nil)
                    return (MobilePhoneStatus.invalidItalian, nil, nil)
                }
            case .none :
                completion(MobilePhoneStatus.invalid, nil, nil)
                return (MobilePhoneStatus.invalid, nil, nil)
            default:
                guard !prefixes.isEmpty else {
                    completion(MobilePhoneStatus.noPrefixes, nil, nil)
                    return (MobilePhoneStatus.noPrefixes, nil, nil)
                }
                
                let fullPhone = phone.replacingOccurrences(of: "+", with: "00") as NSString
                
                for p in prefixes {
                    let prefix = p.replacingOccurrences(of: "+", with: "00").replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
                    if fullPhone.range(of: prefix).location == 0 {
                        NDC_SN = fullPhone.replacingOccurrences(of: prefix, with: "")
                        CC = prefix
                        if NDC_SN?.count >= minNumbers {
                            completion(MobilePhoneStatus.validInternational, CC, NDC_SN)
                            return (MobilePhoneStatus.validInternational, CC, NDC_SN)
                        }
                        else {
                            completion(MobilePhoneStatus.invalidInternational, nil, nil)
                            return (MobilePhoneStatus.invalidInternational, nil, nil)
                        }
                    }
                }
                
                completion(MobilePhoneStatus.invalidInternationalPrefix, nil, nil)
                return (MobilePhoneStatus.invalidInternationalPrefix, nil, nil)
            }
        } else {
            switch isValidItalianMobilePhone(number) {
            case .validItalian:
                completion(MobilePhoneStatus.validItalian, "0039", number)
                return (MobilePhoneStatus.validItalian, "0039", number)
            default :
                completion(MobilePhoneStatus.invalidItalian, nil, nil)
                return (MobilePhoneStatus.invalidItalian, nil, nil)
            }
        }
    }
}
