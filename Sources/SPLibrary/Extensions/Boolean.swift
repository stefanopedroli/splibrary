//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 15/01/2020.
//

import Foundation

public extension Bool {
    var bitEncoded: String {
        if self { return "1" } else { return "0" }
    }
    
    mutating func inverse() {
        self = !self
    }
    
    /// - description: This function generate a random Boolean with percentage probability limitation
    /// - description: Percentage goes from 0 to 100 %
    /// - description: For example: if you set percentage = 20 you have 20% of probability to get 'true' from this function
    static func random(percentage: Float) -> Bool {
        let randomNumber = Float.random(in: 1...100)
        return randomNumber < percentage
    }
}

public extension String {
    var boolean: Bool {
        if self == "0" { return false } else { return true }
    }
}

// La classe per quando le cose si mettono male
public struct BooleanBit: Codable {
    let value: Bool
    
    init(bool: Bool) {
        self.value = bool
    }
    
    init(string: String) {
        self.value = string.boolean
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let string = try container.decode(String.self)
        self.init(string: string)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(value.bitEncoded)
    }
}
