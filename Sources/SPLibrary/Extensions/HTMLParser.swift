//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 17/01/2020.
//

import UIKit

public extension String {

    var utfData: Data {
        return Data(utf8)
    }

    var attributedHtmlString: NSAttributedString? {
        do {
            return try NSAttributedString(data: utfData,
            options: [.documentType: NSAttributedString.DocumentType.html,
                      .characterEncoding: String.Encoding.utf8.rawValue
                     ], documentAttributes: nil)
        } catch {
            print("HTML PARSING ERROR:", error)
            return nil
        }
    }
}

public extension Data {
    var html: NSAttributedString? {
        do {
            return try NSAttributedString(data: self,
                options: [.documentType: NSAttributedString.DocumentType.html,
                          .characterEncoding: String.Encoding.utf8.rawValue
                         ], documentAttributes: nil)
        } catch {
            print("HTML PARSING ERROR:", error)
            return nil
        }
    }
}

extension UILabel {
   func setAttributedHtmlText(_ html: String) {
      if let attributedText = html.attributedHtmlString {
         self.attributedText = attributedText
      }
   }
}
