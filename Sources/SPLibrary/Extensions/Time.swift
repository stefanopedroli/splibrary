//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 03/12/2019.
//

import Foundation

open class Time: Codable {
    
    public var hours: Int
    public var minutes: Int
    public var seconds: Int

    public init(hours: Int = 0, minutes: Int = 0, seconds: Int = 0) {
        self.hours = hours
        self.minutes = minutes
        self.seconds = seconds
    }
    
    public init(date: Date) {
        self.hours = date.hour
        self.minutes = date.minutes
        self.seconds = date.seconds
    }
    
    public var description: String {
        var date = Date()
        date.setTime(time: self)
        return date.getFormattedDate(format: .hoursMinute)
    }
}

public extension Date {
    mutating func setTime(time: Time) {
        self.hour = time.hours
        self.minutes = time.minutes
        self.seconds = time.seconds
    }
}

public class DateStringLiterally: Codable {
    
    static var formatter: Date.DateFormatExample = .inverted
    
    public let value: Date
    public let formattedText: String
    
    public required init(date: Date, text: String? = nil) {
        self.value = date
        self.formattedText = text ?? date.getFormattedDate()
    }
    
    required public convenience init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let literallyString = try container.decode(String.self)
        if let date = Date(string: literallyString, formatter: DateStringLiterally.formatter) {
            self.init(date: date, text: literallyString)
        } else { throw Network.Error.parsingDataError }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(formattedText)
    }
}

