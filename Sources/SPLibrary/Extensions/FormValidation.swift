//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 19/11/2019.
//

import UIKit

public extension UITextField {
    
    enum ValidationError: Swift.Error {
        case empty
        case invalid
        case none
    }
    
    var validable: ValidationError {
        
        // check if is not empty
        guard let text = text, !text.isEmpty else { return .empty }
        
        switch self.textContentType {
        case UITextContentType.name: break
        case UITextContentType.namePrefix: break
        case UITextContentType.givenName: break
        case UITextContentType.middleName: break
        case UITextContentType.familyName: break
        case UITextContentType.nameSuffix: break
        case UITextContentType.nickname: break
        case UITextContentType.jobTitle: break
        case UITextContentType.organizationName: break
        case UITextContentType.location: break
        case UITextContentType.fullStreetAddress: break
        case UITextContentType.streetAddressLine1: break
        case UITextContentType.streetAddressLine2: break
        case UITextContentType.addressCity: break
        case UITextContentType.addressState: break
        case UITextContentType.addressCityAndState: break
        case UITextContentType.sublocality: break
        case UITextContentType.countryName: break
        case UITextContentType.postalCode: break
        case UITextContentType.telephoneNumber:
            guard MobilePhoneValidator.isPhoneNumber(text) else { return .invalid }
        case UITextContentType.emailAddress:
            guard text.isValidEmail else { return .invalid }
        case UITextContentType.URL:
            guard URL(string: text) != nil else { return .invalid }
        case UITextContentType.creditCardNumber: break
        case UITextContentType.username: break
        case UITextContentType.password: break
        default: break
        }
        
        if #available(iOS 12.0, *) {
            switch self.textContentType {
            case UITextContentType.newPassword: break
            case UITextContentType.oneTimeCode: break
            default: break
            }
        }
        return .none
    }
    
    func apply(regex: ValidationRejexType) -> Bool {
        guard let text = self.text else { return false }
        return text.applyRegex(regex: regex.regex)
    }
}

public extension String {
    func validate(regex: ValidationRejexType) -> Bool {
        return self.applyRegex(regex: regex.regex)
    }
}

public enum ValidationRejexType {
    
    case email
    case phone
    case atLeastOneSpecialCharacter
    case atLeastOneDigit
    case atLeastOneLetter
    case atLeastOneUppercaseCharacter
    case atLeastOneLowercaseCharacter
    
    var regex: String {
        switch self {
        case .email: return "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        case .phone: return "^[(\\+)]?[0-9]{2,14}$"
        case .atLeastOneSpecialCharacter: return "(?=.*[!@#$%^&*])"
        case .atLeastOneDigit: return "(?=.*[0-9])"
        case .atLeastOneLetter: return "(/[a-z]/i)"
        case .atLeastOneUppercaseCharacter: return "(?=.*[A-Z])"
        case .atLeastOneLowercaseCharacter: return "(?=.*[a-z])"
        }
    }
}
