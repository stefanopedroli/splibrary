//  Created by Stefano Pedroli on 21/01/2020.

import UIKit
import WebKit

public extension Bundle {
    
    var executable: String {
        return info(for: kCFBundleExecutableKey as String)
    }
    
    var identifier: String {
        return info(for: kCFBundleIdentifierKey as String)
    }
    
    var versionNumber: String {
        return info(for: "CFBundleShortVersionString")
    }
    
    var buildNumber: String {
        return info(for: kCFBundleVersionKey as String)
    }
    
    private func info(for key: String) -> String {
        return infoDictionary?[key] as? String ?? "Unknown"
    }
    
    var displayName: String {
        return object(forInfoDictionaryKey: "CFBundleDisplayName") as? String ?? ""
    }

    var releaseVersionNumber: String? {
        return self.infoDictionary?["CFBundleShortVersionString"] as? String
    }

    var buildVersionNumber: String? {
        return self.infoDictionary?["CFBundleVersion"] as? String
    }
    
    var versionAndBundle: String {
        return "\(Bundle.main.releaseVersionNumber ?? "?").\(Bundle.main.buildVersionNumber ?? "??")"
    }
    
    var isAppExtension: Bool {
        return Bundle.main.bundleURL.pathExtension == "appex"
    }
    
    /// - description: Version Number is: x.xx.xxx (major.minor.build)
    var majorVersionNumber: String {
        let numbers = self.versionNumber.split(separator: ".")
        if let major = numbers[safe: 0] {
            return String(major)
        } else {
            return String()
        }
    }
    
    /// - description: Version Number is: x.xx.xxx (major.minor.build)
    var minorVersionNumber: String {
        let numbers = self.versionNumber.split(separator: ".")
        if let minor = numbers[safe: 1] {
            return String(minor)
        } else {
            return String()
        }
    }
    
    /// - description: Version Number is: x.xx.xxx (major.minor.build)
    var buildReleaseVersionNumber: String {
        let numbers = self.versionNumber.split(separator: ".")
        if let build = numbers[safe: 1] {
            return String(build)
        } else {
            return String()
        }
    }
}

public extension UIDevice {
    
    // MARK: - Device info
    static var isSimulator: Bool {
//        #if targetEnvironment(simulator)
//        return true
//        #else
//        return false
//        #endif
        return ProcessInfo.processInfo.environment["SIMULATOR_DEVICE_NAME"] != nil
    }

    static var isIPad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }

    static var isIPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }

    static var isIPhone4: Bool {
        return (self.screenHeight - 568 == 0)
    }

    static var iOSVersion: Float {
        return (UIDevice.current.systemVersion as NSString).floatValue
    }

    // MARK: - Dimensions
    static var screenWidth: CGFloat {
        return UIScreen.main.bounds.size.width
    }

    static var screenHeight: CGFloat {
        return UIScreen.main.bounds.size.height
    }
    
    /// Has safe area
    /// with notch: 44.0 on iPhone X, XS, XS Max, XR
    /// without notch: 20.0 on iPhone 8 on iOS 12+
    static var hasNotch: Bool {
        if #available(iOS 11.0,  *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }

    static var deviceManufacturer: String = "Apple"
    static var deviceModel: String = UIDevice.current.model
    static var deviceOsVersion: String = UIDevice.current.systemVersion
    static var deviceUserAgent: String = WKWebView(frame: CGRect.zero).customUserAgent ?? ""
    static var deviceUniqueId: String = UIDevice.current.vendorID ?? String()
    
    var vendorID: String? {
        return identifierForVendor?.uuidString
    }
    
    static var extra_info: String = ["device manufacturer: \(deviceManufacturer)", "device model: \(deviceModel)", "ios versione: \(deviceOsVersion)"].joined(separator: ", ")
}

public extension UIApplication {
    
    static var appName: String { return Bundle.displayName ?? String() }
    static var appVersionRelease: String { return String(Bundle.main.versionNumber) }
    static var appMajorMinorVersionRelease: String { return Bundle.main.majorVersionNumber + "." + Bundle.main.minorVersionNumber }
    static var deviceUniqueId: String { return UIDevice.current.vendorID ?? String() }
    
    static var application_info: String { return [appName, "versione app: \(appVersionRelease)", "decive id: \(deviceUniqueId)"].joined(separator: ", ") }
    static var extra_info: String { return application_info + ", " + UIDevice.extra_info }
}

public extension Bundle {
    static var displayName: String? {
        return main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String ?? main.object(forInfoDictionaryKey: "CFBundleName") as? String
    }
}
