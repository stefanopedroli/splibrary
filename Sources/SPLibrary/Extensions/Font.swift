//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 27/01/2020.
//

import UIKit

public extension UIFont {
    static func libraryFont(ofSize: CGFloat, weight: Weight = .regular) -> UIFont {
        if let customFamily = SPLibrary.familyFontName, let font = UIFont(name: customFamily, size: ofSize) {
            return font.withWeight(weight)
        } else {
            return UIFont.systemFont(ofSize: ofSize, weight: weight)
        }
    }
}

public extension UIFont {
    
    func withWeight(_ weight: UIFont.Weight) -> UIFont {
        var attributes = fontDescriptor.fontAttributes
        var traits = (attributes[.traits] as? [UIFontDescriptor.TraitKey: Any]) ?? [:]
        traits[.weight] = weight
        attributes[.name] = nil
        attributes[.traits] = traits
        attributes[.family] = familyName
        let descriptor = UIFontDescriptor(fontAttributes: attributes)
        return UIFont(descriptor: descriptor, size: pointSize)
    }
    
    static func getCustomFont(name: String, weight: UIFont.Weight, size: CGFloat) -> UIFont {
        return UIFont(name: name, size: size)?.withWeight(weight) ?? UIFont.libraryFont(ofSize: size, weight: weight)
    }
}

public extension UILabel {
    func setCharacterSpacing(_ spacing: CGFloat){
        let attributedStr = NSMutableAttributedString(string: self.text ?? "")
        attributedStr.addAttribute(NSAttributedString.Key.kern, value: spacing, range: NSMakeRange(0, attributedStr.length))
        self.attributedText = attributedStr
     }
}
