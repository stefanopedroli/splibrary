//  Created by Stefano Pedroli on 30/07/2020.

import Foundation

public extension URL {
    var normalize: URL? {
        if (!absoluteString.starts(with: "https://") && !absoluteString.starts(with: "http://")) {
          // The following line is based on the assumption that the URL will resolve using https.
          // Ideally, after all checks pass, the URL should be pinged to verify the correct protocol.
          // Better yet, it should need to be provided by the user - there are nice UX techniques to address this.
          return URL(string: "http://\(absoluteString)")
        } else { return self }
    }
}
