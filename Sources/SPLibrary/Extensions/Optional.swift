//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 18/12/2019.
//

import Foundation

public protocol OptionalType: ExpressibleByNilLiteral {
    associatedtype WrappedType
    var asOptional: WrappedType? { get }
}

extension Optional: OptionalType {
    public var asOptional: Wrapped? {
        return self
    }
}

public extension Optional {
    var reduce: Wrapped? {
        guard let self = self else { return nil }
        return self
    }
}
