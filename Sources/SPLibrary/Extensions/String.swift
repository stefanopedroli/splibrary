//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 16/11/2019.
//

import Foundation

public extension String {
    
    var url: URL? { return URL(string: self) }
    
    /// - description: Reconvert string to optional nil if is empty
    var optionalyze: String? {
        return self.isEmpty ? nil : self
    }
}

public extension Optional where Wrapped == String {
    var isEmpty: Bool {
        return self == .none || self == ""
    }
}

public extension BinaryInteger {
    var isEven: Bool { return self % 2 == 0 }
    var isOdd: Bool { return self % 2 != 0 }
}

public extension String {
    
    var initials: String {
        var letters: String = ""
        self.components(separatedBy: " ").forEach { (element) in
            letters += String(element.first ?? Character(""))
        }
        return letters
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func replaceString(in range: NSRange, with replacementString: String) -> String {
        let start = index(startIndex, offsetBy: range.location)
        let end = index(start, offsetBy: range.length)
        return replacingCharacters(in: start ..< end, with: replacementString)
    }
    
    var isValidEmail: Bool {
        let regex = ValidationRejexType.email.regex
        let emailPred = NSPredicate(format:"SELF MATCHES %@", regex)
        return emailPred.evaluate(with: self)
    }
    
    var isValidPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
    
    func applyRegex(regex: String) -> Bool {
        let regex = try! NSRegularExpression(pattern: regex)
        let range = NSRange(location: 0, length: self.utf16.count)
        return regex.firstMatch(in: self, options: [], range: range) != nil
    }
    
    func loadRegexMatches(regex: String) -> [String] {
        let regex = try! NSRegularExpression(pattern: regex)
        let range = NSRange(location: 0, length: self.utf16.count)
        let matches = regex.matches(in: self, options: [], range: range)
        var stringMatches: [String] = []
        for match in matches {
            let nsString = self as NSString
            let matchString = nsString.substring(with: match.range) as String
            stringMatches.append(matchString)
        }
        return stringMatches
    }
}

internal extension NSRegularExpression {
    convenience init(_ pattern: String) {
        do {
            try self.init(pattern: pattern)
        } catch {
            preconditionFailure("Illegal regular expression: \(pattern).")
        }
    }
}
