//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 27/01/2020.
//

import UIKit
import Lottie

public extension UITableView {
    
    func isLastVisibleCell(at indexPath: IndexPath) -> Bool {
        guard let lastIndexPath = indexPathsForVisibleRows?.last else { return false }
        return lastIndexPath == indexPath
    }
    
    func isLastIndexPath(indexPath: IndexPath) -> Bool {
        return indexPath.row == (self.numberOfRows(inSection: indexPath.section) - 1)
    }
    
    func clearOnAppearance() {
        for indexPath in self.indexPathsForSelectedRows ?? [] {
            self.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = EmptyTableViewLabel(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: 50))
        messageLabel.text = message
        messageLabel.textColor = UIColor.mainThemeBlack
        messageLabel.numberOfLines = 2
        messageLabel.textAlignment = .center
        self.tableFooterView?.addSubview(messageLabel)
    }
    
    func removeEmptyMessage() {
        for case let emptyLabel as EmptyTableViewLabel in self.tableFooterView?.subviews ?? [] {
            emptyLabel.removeFromSuperview()
        }
    }
    
    func addEmptyMessageIfNeeded(message: String, numberOfItems: Int) -> Int {
        if numberOfItems == 0 {
            self.setEmptyMessage(message)
        } else {
            self.removeEmptyMessage()
        }
        return numberOfItems
    }
    
    func addBottomSpace(height: CGFloat) {
        self.tableFooterView = UIView(frame: .init(x: 0, y: 0, width: self.bounds.width, height: height))
    }
}

public extension UITableViewCell {
    var tableView: UITableView? {
        var view = superview
        while let v = view, v.isKind(of: UITableView.self) == false {
            view = v.superview
        }
        return view as? UITableView
    }
}

final class EmptyTableViewLabel: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

public final class TableViewAnimation {
    
    public typealias Animation = (UITableViewCell, IndexPath, UITableView) -> Void
    
    public static func makeMoveUpWithBounce(rowHeight: CGFloat, duration: TimeInterval, delayFactor: Double) -> Animation {
        return { cell, indexPath, tableView in
            cell.transform = CGAffineTransform(translationX: 0, y: rowHeight)

            UIView.animate(
                withDuration: duration,
                delay: delayFactor * Double(indexPath.row),
                usingSpringWithDamping: 0.4,
                initialSpringVelocity: 0.1,
                options: [.curveEaseInOut],
                animations: {
                    cell.transform = CGAffineTransform(translationX: 0, y: 0)
            })
        }
    }
    
    public static func makeMoveUpWithFade(rowHeight: CGFloat, duration: TimeInterval, delayFactor: Double) -> Animation {
        return { cell, indexPath, _ in
            cell.transform = CGAffineTransform(translationX: 0, y: rowHeight / 2)
            cell.alpha = 0

            UIView.animate(
                withDuration: duration,
                delay: delayFactor * Double(indexPath.row),
                options: [.curveEaseInOut],
                animations: {
                    cell.transform = CGAffineTransform(translationX: 0, y: 0)
                    cell.alpha = 1
            })
        }
    }
    
    public final class Animator {
        private var hasAnimatedAllCells = false
        private let animation: Animation

        public init(animation: @escaping Animation) { self.animation = animation }

        public func animate(cell: UITableViewCell, at indexPath: IndexPath, in tableView: UITableView) {
            guard !hasAnimatedAllCells else { return }

            animation(cell, indexPath, tableView)

            hasAnimatedAllCells = tableView.isLastVisibleCell(at: indexPath)
        }
    }
}

public protocol ThemableSelection {
    var selectionColor: UIColor { get }
    var normalColor: UIColor { get }
}

public extension ThemableSelection {
    var selectionColor: UIColor { return UIColor.secondTheme.light }
    var normalColor: UIColor { return UIColor.mainThemeWhite }
}

public extension ThemableSelection where Self: UITableViewCell {
    func loadSelection(_ selected: Bool) {
        // Configure the view for the selected state
        if selected {
            self.contentView.backgroundColor = selectionColor
        } else {
            self.contentView.backgroundColor = normalColor
        }
    }
}

public extension UITableViewCell {
    /// - description: This method just remove top and bottom separator on a groupped tableView. You need to call this method on willAppear tableView delegate
    /// - warning: make this on groupped table view only
    func removeOuterGrouppedSeparatorIfNeeded() {
        self.layoutSubviews()
        let width = self.subviews[0].frame.width
        for view in self.subviews where view != self.contentView {
            //for top and bottom separator will be same width with the tableview width
            //so we check at here and remove accordingly
            if view.frame.width == width && NSStringFromClass(view.classForCoder) == "_UITableViewCellSeparatorView" {
                view.removeFromSuperview()
            }
        }
    }
}

public extension UIEdgeInsets {
    static var none: UIEdgeInsets { return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: UIScreen.main.bounds.width) }
    static var defaultSeparatorInset: UIEdgeInsets { return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20) }
}
