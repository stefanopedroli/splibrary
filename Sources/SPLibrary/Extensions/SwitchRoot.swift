//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 18/12/2019.
//

import UIKit

// Custom AppDelegate Class
private typealias ApplicationDelegate = (UIResponder & UIApplicationDelegate)

public func switchRootViewController(rootViewController: UIViewController, animated: Bool, completion: (() -> Void)? = nil) {
    guard let appDelegate = UIApplication.shared.delegate as? ApplicationDelegate else { return }
    guard let rootVC = appDelegate.window??.rootViewController else {
        return addNewRootViewController(rootViewController: rootViewController, animated: animated, completion: completion)
    }
    
    if rootVC.presentingViewController != nil {
        rootVC.dismiss(animated: false) {
            addNewRootViewController(rootViewController: rootViewController, animated: animated, completion: completion)
        }
    } else {
        addNewRootViewController(rootViewController: rootViewController, animated: animated, completion: completion)
    }
}

private func addNewRootViewController(rootViewController: UIViewController, animated: Bool, completion: (() -> Void)? = nil) {
    guard let appDelegate = UIApplication.shared.delegate as? ApplicationDelegate else { return }
    guard let window = appDelegate.window?.reduce else { return }
    window.rootViewController = rootViewController
    window.makeKeyAndVisible()
    if animated {
        let oldState = UIView.areAnimationsEnabled
        UIView.transition(with: window, duration: 0.2, options: .transitionCrossDissolve, animations: {
            UIView.setAnimationsEnabled(false)
            window.rootViewController = rootViewController
            window.makeKeyAndVisible()
        }, completion: {(finished: Bool) -> () in
            UIView.setAnimationsEnabled(oldState)
            completion?()
        })
    } else {
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
        completion?()
    }
}
