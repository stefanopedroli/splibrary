//  Created by Stefano Pedroli on 19/11/2019.

import Foundation

private extension DateFormatter {
    static var formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "it_IT")
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.timeZone = .current
//        formatter.timeZone = TimeZone(abbreviation: "UTC")
        return formatter
    }()
    
    static var italianFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "it_IT")
        formatter.calendar = Calendar(identifier: .gregorian)
//        let nyTimeZone = TimeZone(identifier: "Europe/Rome")
        formatter.timeZone = TimeZone(abbreviation: "GMT+1")
        return formatter
    }()
}

public extension DateFormatter {
    /// - description: Used for print informations time
    static let completeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, dd MMMM yyyy HH:mm:ss Z"
        formatter.locale = Locale(identifier: "it_IT")
        formatter.timeZone = TimeZone(abbreviation: "UTC+1")
        return formatter
    }()
}

public extension Locale {
    static var preferredLocale: Locale {
        guard let preferredIdentifier = Locale.preferredLanguages.first else {
            return Locale.current
        }
        return Locale(identifier: preferredIdentifier)
    }
}

public extension Date {
    
    init?(string: String, formatter: DateFormatExample = .normal) {
        DateFormatter.formatter.dateFormat = formatter.rawValue
        guard let date = DateFormatter.formatter.date(from: string) else { return nil }
        self = date
    }
    
    init?(string: String, formattedString: String) {
        DateFormatter.formatter.dateFormat = formattedString
        guard let date = DateFormatter.formatter.date(from: string) else { return nil }
        self = date
    }
    
    enum DateFormatExample: String {
        case all = "yyyy-MM-dd'T'HH:mm:ssZ"
        case complete = "EEEE, dd MMMM yyyy HH:mm:ss Z"
        case normal = "dd/MM/yyyy"
        case hoursMinute = "HH:mm"
        case hoursMinuteEn = "HH:mm a" // am/pm
        case inverted = "yyyy-MM-dd"
        case time = "HH:mm:ss"
        case monthDescriptiorYear = "MMMM yyyy"
        case timeStamp = "yyyy_MM_dd_HH_mm_ss"
        case normalWithMonthShort = "dd MMM yyyy"
        case text = "dd MMMM yyyy"
    }
    
    enum TimeZoneIdentifier {
        case rome
        case newYork
        case los_angeles
        case custom(identifier: String)
        case abbreviation(value: String)
        
        public var rawValue: String {
            switch self {
            case .rome: return "Europe/Rome"
            case .newYork: return "America/New_York"
            case .los_angeles: return "America/Los_Angeles"
            default: return ""
            }
        }
    }
    
    var isToday: Bool {
        return self.day == Date().day && self.month == Date().month && self.year == Date().year
    }
    
    func comprares(components: [Calendar.Component], to compare: Date) -> Bool {
        for component in components {
            guard Date.calendar.component(component, from: self) == Date.calendar.component(component, from: compare) else {
                return false
            }
        }
        return true
    }
    
    func getFormattedDate(format: DateFormatExample = .normal) -> String {
        getFormattedDate(format: format.rawValue)
    }
    
    func getFormattedDate(format: String) -> String {
//        DateFormatter.formatter.timeZone = Date.getTimeZone(timezone: timezone)
        DateFormatter.formatter.dateFormat = format
        return DateFormatter.formatter.string(from: self)
    }
    
    private static func getTimeZone(timezone: TimeZoneIdentifier = .rome) -> TimeZone? {
        switch timezone {
        case .abbreviation(let abbreviation):
            return TimeZone(abbreviation: abbreviation)
        case .custom(let identifier):
            return TimeZone(identifier: identifier)
        default:
            return TimeZone(identifier: timezone.rawValue)
        }
    }
    
    static var yesterday: Date? { return Date().dayBefore }
    static var tomorrow:  Date? { return Date().dayAfter }
    static var today: Date { return Date() }
    
    var zero: Date {
        return Date.calendar.startOfDay(for: self)
    }
    
    var dayBefore: Date? {
        guard let noon = noon else { return nil }
        return Date.calendar.date(byAdding: .day, value: -1, to: noon)
    }
    var dayAfter: Date? {
        guard let noon = noon else { return nil }
        return Date.calendar.date(byAdding: .day, value: 1, to: noon)
    }
    var noon: Date? {
        return Date.calendar.date(bySettingHour: 12, minute: 0, second: 0, of: self)
    }
    var beforeMidnight: Date? {
        return Date.calendar.date(bySettingHour: 23, minute: 0, second: 0, of: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter?.month != month
    }
    
    var nextHour: Date {
        let minutes = Date.calendar.component(.minute, from: self)
        let components = DateComponents(hour: 1, minute: -(minutes+1))
        return Date.calendar.date(byAdding: components, to: self) ?? self
    }
    
    static var secondUntileNextHour: Int {
        return Date.today.difference(of: .second, from: Date.today.nextHour)
    }
    
    static var secondUntileNextMinute: Int {
        let seconds = Date.calendar.component(.second, from: Date.today)
        return 60-seconds
    }
    
    static var calendar: Calendar {
        return Calendar.init(identifier: .gregorian)
    }
    
    /// - description: This calendar have weeks starting from **Monday**!
    /// So if you wnat to check if a Date is in same week but forcing Monday as firstWeekDay, you need to use this Calendar
    static var isoCalendar: Calendar {
        return Calendar.init(identifier: .iso8601)
    }
    
    var year: Int {
        get { Date.calendar.component(.year, from: self) }
        set { change(component: .year, newValue: newValue) }
    }
    var month: Int {
        get { return Date.calendar.component(.month, from: self) }
        set { change(component: .month, newValue: newValue) }
    }
    var day: Int {
        get {return Date.calendar.component(.day, from: self) }
        set { change(component: .day, newValue: newValue) }
    }
    var hour: Int {
        get { return Date.calendar.component(.hour, from: self) }
        set { change(component: .hour, newValue: newValue) }
    }
    var minutes: Int {
        get { return Date.calendar.component(.minute, from: self) }
        set { change(component: .minute, newValue: newValue) }
    }
    var seconds: Int {
        get { return Date.calendar.component(.second, from: self) }
        set { change(component: .second, newValue: newValue) }
    }
    
    mutating func change(component: Calendar.Component, newValue: Int) {
        guard let newDate = Date.calendar.date(bySetting: component, value: newValue, of: self) else { return }
        self = newDate
    }
    
    func differenceDay(from date: Date) -> Int {
        let components = Date.calendar.dateComponents([.day], from: self, to: date)
        return abs(components.day ?? 0)
    }
    
    func differenceHours(from date: Date) -> Int {
        let components = Date.calendar.dateComponents([.hour], from: self, to: date)
        return abs(components.hour ?? 0)
    }
    
    // return nil if not difference, instead of "+1", "-1", "+2", ...
    func timeZoneDayDifference(from date: Date) -> String? {
        func formattedStringInteger(_ value: Int) -> String {
            if value > 0 { return "+\(value)" }
            else { return String(value) }
        }
        let components = Date.calendar.dateComponents([.day], from: self, to: date)
        let difference = components.day ?? 0
        if difference != 0 {
            return formattedStringInteger(difference)
        } else {
            let realDifference = date.day - self.day
            if realDifference != 0 {
                return formattedStringInteger(realDifference)
            } else { return nil }
        }
    }
    
    /// - description: This function will return absolute value not negative
    /// - warning: If you want to know
    func difference(of component: Calendar.Component, from date: Date) -> Int {
        let _component = Date.calendar.dateComponents([component], from: self, to: date)
        var value: Int = 0
        switch component {
        case .era: value = _component.era ?? 0
        case .year: value = _component.year ?? 0
        case .month: value = _component.month ?? 0
        case .day: value = _component.day ?? 0
        case .hour: value = _component.hour ?? 0
        case .minute: value = _component.minute ?? 0
        case .second: value = _component.second ?? 0
        case .weekday: value = _component.weekday ?? 0
        case .weekdayOrdinal: value = _component.weekdayOrdinal ?? 0
        case .quarter: value = _component.quarter ?? 0
        case .weekOfMonth: value = _component.weekOfMonth ?? 0
        case .weekOfYear: value = _component.weekOfYear ?? 0
        case .yearForWeekOfYear: value = _component.yearForWeekOfYear ?? 0
        case .nanosecond: value = _component.nanosecond ?? 0
        case .calendar, .timeZone: break
        @unknown default: break
        }
        return abs(value)
    }
    
    func timeDurationFrom(date: Date, format: String) -> String {
        let differenceInSeconds = self.timeIntervalSince(date)
        let date = Date.init(timeInterval: differenceInSeconds, since: date)
        return date.getFormattedDate(format: format)
    }
    
    var monthDescription: String {
        get {
            let dateFormatter = DateFormatter.formatter
            dateFormatter.dateFormat = "MMMM"
            return dateFormatter.string(from: self)
        }
    }
    
    var shortMonthDescription: String {
        get {
            let dateFormatter = DateFormatter.formatter
            dateFormatter.dateFormat = "MMM"
            return dateFormatter.string(from: self)
        }
    }
    
    var dayWeekDescription: String {
        let dateFormatter = DateFormatter.formatter
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self)
    }
    
    var timestampDescription: String {
        return self.getFormattedDate(format: .timeStamp)
    }
}

public extension String {
    var timestampDate: Date? {
        return Date.init(string: self, formatter: .timeStamp)
    }
}

public extension DateInterval {
    
    var dates: [Date] {
        var dates: [Date] = []
        var date = self.start
        while date <= self.end {
            dates.append(date)
            guard let newDate = Date.calendar.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
}

public extension Calendar {
    static var defaultSPCalendar: Calendar { return Date.calendar }
}

public extension Calendar {
    var firstWorkWeekday: Int {
        guard #available(iOS 10.0, *) else{
            return self.firstWeekday
        }
        guard let endOfWeekend = self.nextWeekend(startingAfter: Date())?.end else {
            return self.firstWeekday
        }
        return self.ordinality(of: .weekday, in: .weekOfYear, for: endOfWeekend) ?? self.firstWeekday
    }
}

public extension Date {
    func startOfMonth() -> Date? {
        return Date.calendar.date(from: Date.calendar.dateComponents([.year, .month], from: Date.calendar.startOfDay(for: self)))
    }

    func endOfMonth() -> Date? {
        guard let startOfMonth = self.startOfMonth() else { return nil }
        return Date.calendar.date(byAdding: DateComponents(month: 1, day: -1), to: startOfMonth)
    }
    
    func getPreviousMonth() -> Date? {
        return Date.calendar.date(byAdding: .month, value: -1, to: self)
    }
    
    func getNextMonth() -> Date? {
        return Date.calendar.date(byAdding: .month, value: +1, to: self)
    }
    
    func remove(days: Int) -> Date? {
        return Date.calendar.date(byAdding: .day, value: -days, to: self)
    }
    
    func add(days: Int) -> Date? {
        return Date.calendar.date(byAdding: .day, value: +days, to: self)
    }
    
    func remove(months: Int) -> Date? {
        return Date.calendar.date(byAdding: .day, value: -months, to: self)
    }
    
    func add(months: Int) -> Date? {
        return Date.calendar.date(byAdding: .day, value: +months, to: self)
    }
    
    func remove(years: Int) -> Date? {
        return Date.calendar.date(byAdding: .day, value: -years, to: self)
    }
    
    func add(years: Int) -> Date? {
        return Date.calendar.date(byAdding: .day, value: +years, to: self)
    }
    
    func isSameDay(date: Date) -> Bool {
        return Date.calendar.isDate(self, inSameDayAs: date)
    }
    
    func isEqual(to date: Date, toGranularity component: Calendar.Component) -> Bool {
        return Date.calendar.isDate(self, equalTo: date, toGranularity: component)
    }

    func isInSameYear(as date: Date) -> Bool { isEqual(to: date, toGranularity: .year) }
    func isInSameMonth(as date: Date) -> Bool { isEqual(to: date, toGranularity: .month) }
    func isInSameWeek(as date: Date) -> Bool { isEqual(to: date, toGranularity: .weekOfYear) }
    func isInSameWeekWithMondayFirst(as date: Date) -> Bool {
        return Date.isoCalendar.isDate(self, equalTo: date, toGranularity: .weekOfYear)
    }

    func isInSameDay(as date: Date) -> Bool { Date.calendar.isDate(self, inSameDayAs: date) }

    var isInThisYear:  Bool { isInSameYear(as: Date()) }
    var isInThisMonth: Bool { isInSameMonth(as: Date()) }
    var isInThisWeek:  Bool { isInSameWeek(as: Date()) }

    var isInYesterday: Bool { Date.calendar.isDateInYesterday(self) }
    var isInToday:     Bool { Date.calendar.isDateInToday(self) }
    var isInTomorrow:  Bool { Date.calendar.isDateInTomorrow(self) }

    var isInTheFuture: Bool { self > Date() }
    var isInThePast:   Bool { self < Date() }
    
    func getWeekDays(format: DateFormatter.WeekDaysFormat, formatter: DateFormatter) -> [String] {
        return formatter.weekDays(format: format)
    }
    
    /// - description: This return weekdays from italian Locale
    func getItalianWeekDays(format: DateFormatter.WeekDaysFormat) -> [String] {
        return DateFormatter.formatter.weekDays(format: format)
    }
    
    static func datesBetween(from fromDate: Date, to toDate: Date) -> [Date] {
        var dates: [Date] = []
        var date = fromDate
        while date <= toDate {
            dates.append(date)
            guard let newDate = Date.calendar.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
}

public extension DateFormatter {
    
    /// - warning: Is automatically ordered by firstWeekday of the formatter
    func weekDays(format: WeekDaysFormat) -> [String] {
        let weekdaysArray: [String]
        switch format {
        case .weekdaySymbols: weekdaysArray = self.weekdaySymbols
        case .shortWeekdaySymbols: weekdaysArray = self.shortWeekdaySymbols
        case .veryShortWeekdaySymbols: weekdaysArray = self.veryShortWeekdaySymbols
        case .standaloneWeekdaySymbols: weekdaysArray = self.standaloneWeekdaySymbols
        case .shortStandaloneWeekdaySymbols: weekdaysArray = self.shortStandaloneWeekdaySymbols
        case .veryShortStandaloneWeekdaySymbols: weekdaysArray = self.veryShortStandaloneWeekdaySymbols
        }
        // WeekdaySymbols by default not follow firstWeekday from Locale you pass.. is always start from Sunday.
        // So here's the code for order based to firstWeekday
        return Array(weekdaysArray[self.calendar.firstWeekday - 1 ..< weekdaysArray.count] + weekdaysArray[0 ..< self.calendar.firstWeekday - 1])
    }
    
    enum WeekDaysFormat {
        case weekdaySymbols // -> ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
        case shortWeekdaySymbols // -> ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
        case veryShortWeekdaySymbols // -> ["S", "M", "T", "W", "T", "F", "S"]
        case standaloneWeekdaySymbols // -> ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
        case shortStandaloneWeekdaySymbols // -> ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
        case veryShortStandaloneWeekdaySymbols // -> ["S", "M", "T", "W", "T", "F", "S"]
    }
}

public extension Array where Element == Date {
    func cronologically() -> [Date] {
        return self.sorted(by: { (first, second) -> Bool in
            return first < second
        })
    }
}

/// You can set timeinterval reference from backend into getFormattedDate method,
/// but you can also use this class to specify that timeinterval from backend is from 1970 unstead of now
public class DateSince1970: Codable {
    
    public var value: Date
    
    public init(date: Date) {
        value = date
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        do {
            let timeinterval = try container.decode(TimeInterval.self)
            value = Date.init(timeIntervalSince1970: timeinterval)
        } catch {
            throw error
        }
    }
    
    // Encode from String value to INT value
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try? container.encode(value.timeIntervalSince1970)
    }
}
