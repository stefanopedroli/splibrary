//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 04/02/2020.
//

import Foundation

internal var percentageFormatter: NumberFormatter = {
    let formatter = NumberFormatter()
    formatter.numberStyle = .percent
    formatter.maximumFractionDigits = 0
    formatter.multiplier = 1
    return formatter
}()

public extension NumberFormatter {
    static let shared = NumberFormatter()
}

public extension NSNumber {
    var percentage: String? {
        return percentageFormatter.string(from: self)
    }
}

public extension String {
    var numberFromPercentage: NSNumber? {
        return percentageFormatter.number(from: self)
    }
}

public extension StringProtocol {
    var doubleValue: Double? {
        return NumberFormatter.shared.number(from: String(self))?.doubleValue
    }
}

public extension Int {
    func normalize(digit: Int) -> String {
        let formatter = NumberFormatter()
        formatter.minimumIntegerDigits = digit
        return formatter.string(from: NSNumber(value: self))!
    }
}

public extension Float {
    func normalize(integerDigits: Int? = nil, fractionDigits: Int = 2) -> String {
        let formatter = NumberFormatter()
        if let intDigits = integerDigits { formatter.minimumIntegerDigits = intDigits }
        formatter.minimumFractionDigits = fractionDigits
        return formatter.string(from: NSNumber(value: self))!
    }
}

public extension String {
    func normalize(digit: Int) -> String {
        guard let number = Int(self) else { return "" }
        let formatter = NumberFormatter()
        formatter.minimumIntegerDigits = digit
        return formatter.string(from: NSNumber(value: number))!
    }
}

public extension Float {
    func formatted(separator: String = ",", decimalDigits: Int) -> String {
        let converter = NumberFormatter()
        converter.decimalSeparator = separator
        converter.maximumFractionDigits = decimalDigits
        converter.minimumFractionDigits = decimalDigits
        return "\(converter.string(from: NSNumber(value: self)) ?? self.description)"
    }
}

public extension Double {
    func formatted(separator: String = ",", decimalDigits: Int) -> String {
        let converter = NumberFormatter()
        converter.decimalSeparator = separator
        converter.maximumFractionDigits = decimalDigits
        converter.minimumFractionDigits = decimalDigits
        return "\(converter.string(from: NSNumber(value: self)) ?? self.description)"
    }
}

public class Percentage: Codable {
    
    public var number: Float
    
    public var text: String? {
        return NSNumber(value: number).percentage
    }
    
    public init(number: Float) {
        self.number = number
    }
    
    public init?(text: String) {
        self.number = text.numberFromPercentage?.floatValue ?? .zero
    }
}
