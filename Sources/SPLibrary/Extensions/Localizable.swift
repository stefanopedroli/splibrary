//  Created by Stefano Pedroli on 28/11/2019.

import UIKit

public extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
        let localized = NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
        return localized.resolveLocalizedSubstring()
    }
    
    internal func resolveLocalizedSubstring() -> String {
        var text = self
        let substrings = text.loadRegexMatches(regex: "\\{(.*?)\\}")
        for substring in substrings {
            let translationKey = substring
                .replacingOccurrences(of: "}", with: "")
                .replacingOccurrences(of: "{", with: "")
            let translationValue = translationKey.localized
            text = text.replacingOccurrences(of: substring, with: translationValue)
        }
        return text
    }
}

public extension UILabel {
    @IBInspectable
    var localizeIdentifier: String? {
        get { self.localizeIdentifier }
        set { self.text = newValue?.localized ?? self.text  }
    }
}

public extension UIButton {
    @IBInspectable
    var localizeIdentifier: String? {
        get { self.localizeIdentifier }
        set { self.setTitle(newValue?.localized ?? self.titleLabel?.text, for: .normal)  }
    }
}

public extension UITextField {
    @IBInspectable
    var localizeIdentifier: String? {
        get { self.localizeIdentifier }
        set { self.placeholder = newValue?.localized ?? self.placeholder  }
    }
}

public extension NSLocale {
    static var language: String {
        switch SPLibrary.configurations.internationalization {
        case .italian: return "it"
        case .english: return "en"
        case .spanish: return "es"
        case .double: return self.current.languageCode == "it" ? "it" : "en"
        case .all: return self.current.languageCode ?? "en"
        }
    }
}

extension String {
    var localizedInternal: String {
        return Localized.getLocalization(for: self)
    }
}

public enum InternazionalizationType {
    case italian
    case english
    case spanish
    case double
    case all
}
