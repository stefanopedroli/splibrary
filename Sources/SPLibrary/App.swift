//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 16/01/2020.
//

import UIKit

public final class App {
    static var name: String = UIApplication.appName
    static var version: String = Bundle.main.versionAndBundle
}

public extension App {
    static func openApp(type: ExternalApp) {
        if let url = URL(string: type.path) {
            UIApplication.shared.open(url)
        } else { print("URL invalid") }
    }
    
    static func openLink(url: URL) {
        guard let normalized = url.normalize else { return print("URL invalid") }
        UIApplication.shared.open(normalized, options: [:], completionHandler: { result in
            print(result)
        })
    }
    
    static func openApp(path: String) {
        if let url = URL(string: path) {
            UIApplication.shared.open(url, options: [:], completionHandler: { result in
                print(result)
            })
        } else { print("URL invalid") }
    }
    
    static func openMap(options: MapEventOptions) {
        Map.open(mapOptions: options)
    }
    
    static func openStartSatispayTransaction(transaction_id: String, callbackURL: String) {
//        let path: String = "\(ExternalApp.satispay.rawValue)external/generic/preauthorized-payments?token=\(transaction_id)&callback_url=\(callbackURL)"
        let path: String = "\(ExternalApp.satispay.path)external/generic/charge?token=\(transaction_id)&callback_url=\(callbackURL)"
        App.openApp(path: path)
    }
    
    static func checkExistingApp(type: ExternalApp) -> Bool {
        guard let appQuerySchemes = Bundle.main.object(forInfoDictionaryKey: "LSApplicationQueriesSchemes") as? Array<String> else {
            assertionFailure("Empty array for LSApplicationQueriesSchemes, you need to add 'LSApplicationQueriesSchemes' property on info.plist (is an array) and att your app scheme you want to check")
            return false
        }
        if !appQuerySchemes.contains(type.rawValue) {
            assertionFailure("You did not add your app scheme (\(type.rawValue)) on info.plist under LSApplicationQueriesSchemes")
            return false
        }
        let appScheme = "\(type.path)app"
        let appUrl = URL(string: appScheme)
        if UIApplication.shared.canOpenURL(appUrl! as URL) {
            return true
        } else {
            print("App not installed")
            return false
        }
    }
    
    func isUpdateAvailable(completion: @escaping (Bool?, Error?) -> Void) throws -> URLSessionDataTask {
        let currentVersion = Bundle.main.versionNumber
        let identifier = Bundle.main.identifier
        guard let info = Bundle.main.infoDictionary,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
                throw Network.Error.generic
        }
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            do {
                if let error = error { throw error }
                guard let data = data else { throw Network.Error.parsingDataError }
                let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any]
                guard let result = (json?["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String else {
                    throw Network.Error.parsingDataError
                }
                completion(version != currentVersion, nil)
            } catch {
                completion(nil, error)
            }
        }
        task.resume()
        return task
    }
}
