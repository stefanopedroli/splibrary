//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 16/01/2020.
//

import UIKit

public protocol CoordinatorArchitecture {
    func getApplicationCoordinator() -> RootPresentingProtocol?
}

extension UIApplication {
    static var appCoordinator: RootPresentingProtocol? {
        guard let appDelegate = UIApplication.shared.delegate as? CoordinatorArchitecture else { return nil }
        return appDelegate.getApplicationCoordinator()
    }
    static var appDelegate: CoordinatorArchitecture? {
        return UIApplication.shared.delegate as? CoordinatorArchitecture
    }
}
