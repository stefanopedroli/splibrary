//  Created by Stefano Pedroli on 30/11/2019.

import UIKit

open class Coordinator: NSObject, ServicesUser {
    
    public var childCoordinators: [Coordinator] = []
    public let services: Services
        
    public init(services: Services) {
        self.services = services
        super.init()
    }
}

public  extension Coordinator {
    
    var presentedCoordinator: Coordinator? {
        return childCoordinators.last
    }
    
    @objc func add(childCoordinator: Coordinator) {
        // Rimuovo tutti i coordinator che non sono quello che si vuole presentare.
        // Ovviamente.. non va bene usare questo metodo per una TabBar!! :)
        self.childCoordinators.removeAll { (coordinator) -> Bool in
            return type(of: coordinator) !== type(of: childCoordinator)
        }
        self.childCoordinators.append(coordinator: childCoordinator)
    }
    
    func remove(childCoordinator: Coordinator) {
        self.childCoordinators = self.childCoordinators.filter { $0 !== childCoordinator }
    }
    
    func search<T: Coordinator>(type: T.Type) -> T? {
        if self is T {
            return (self as? T)
        }
        
        for child in childCoordinators {
            if let found = child.search(type: type) {
                return found
            }
        }
        return nil
    }
    
    func removePresentedCoordinator<T: Coordinator>(except: T.Type?) {
        return
    }
    
    private func searchPresentedCoordinator<T: Coordinator>(except: T.Type?) -> Coordinator? {
        guard let _ = except else {
            return presentedCoordinator
        }
        
        let coordinators = childCoordinators.filter { !($0 is T) }
        return coordinators.last
    }
}

public extension Array where Element == Coordinator {
    mutating func append(coordinator: Coordinator) {
        // Se l'istanza del coordinator che voglio presentare già esiste mantengo la stessa
        for (index, element) in self.enumerated() {
            if type(of: element) == type(of: coordinator) {
                self[index] = coordinator
                return
            }
        }
        self.append(coordinator)
    }
    
    func getCoordinator<T: Coordinator>(coordinator: T.Type) -> Coordinator? {
        for element in self {
            if type(of: element) == coordinator {
                return element
            }
        }
        return nil
    }
}

public protocol RootingProtocol {
    associatedtype T : UIViewController
    /// The first vc of the app, you can also don't use it.
    var rootViewController: T { get set }
}

public protocol RootProtocol {
    /// The first vc of the app, you can also don't use it.
    var rootViewController: UIViewController { get set }
}

public extension RootingProtocol {
    /// - description: first view controller (is the real rootViewController for only UIViewController types)
    var firstViewController: UIViewController? {
        if let navigation = self.rootViewController as? UINavigationController {
            return navigation.viewControllers.first
        } else {
            return self.rootViewController.navigationController?.viewControllers.last
        }
    }
    
    /// - description: last view controller (current visible if this coordinator is the last)
    var lastViewController: UIViewController? {
        if let navigation = self.rootViewController as? UINavigationController {
            return navigation.viewControllers.last
        } else {
            return self.rootViewController.navigationController?.viewControllers.last
        }
    }
}

public extension RootProtocol {
    /// - description: first view controller (is the real rootViewController for only UIViewController types)
    var firstViewController: UIViewController? {
        if let navigation = self.rootViewController as? UINavigationController {
            return navigation.viewControllers.first
        } else {
            return self.rootViewController.navigationController?.viewControllers.last
        }
    }
    
    /// - description: last view controller (current visible if this coordinator is the last)
    var lastViewController: UIViewController? {
        if let navigation = self.rootViewController as? UINavigationController {
            return navigation.viewControllers.last
        } else {
            return self.rootViewController.navigationController?.viewControllers.last
        }
    }
}

public protocol PresentingProtocol: class {
    /// - description: Present initally vc
    func present(options: PresentingOptions, animated: Bool, isFromDeepLinkRequest: Bool)
    /// - description: If coordinator is presented, define in wich navigation type you want
    /// - description: if you want a custom navigation, set none and put on rootViewController
    var embeddedInNavigationType: EmbeddedNavigationType? { get set }
}

public protocol PresentingTaskConditionProtocol: class {
    /// - description: If you have some task in background to do before present
    /// - description: PRESENTING VIEW CONTROLLER is only for display loader or errors, do no use it!
    func taskBeforePresent(presentingViewController: UIViewController, completion: @escaping (_ sender: Any?) -> Void, errors: @escaping (_ error: Network.Error) -> Void)
}

/// Your App have to implement only one MainCoordinator, the main cordinator with all app needed
public protocol RootPresentingProtocol: class {
    /// Present initially vc
    func present(from window: UIWindow, isFromDeepLinkRequest: Bool)
    /// Present from deep link
    func handleDeepLinkRequest(with url: URL, options: [UIApplication.OpenURLOptionsKey: Any])
    /// Function to attach to willEnterForegroundNotification to make some operation when app will return active
    /// - warning: Remember to deinit
    func performDefaultSync()
    /// Function for logout, if app did not implemen login ignore this function
    func logout()
    /// Here you can override default message error displayed by Library *handleError* function (extension of UIviewController)
    func handleError(message: String, completion: VoidHandler?)
}

/// - description: E' il protocollo per il coordinator principale. In ogni app dovrebbe esserci un solo MainCoordinator!
/// - description: Questo coordinator è colui che gestisce l'inizio dell'app, che flussi presentare appena si apre.
/// - description: Va dichiarata nell'AppDelegate, questo perché è il coordinator che regge l'istanza di tutti gli altri, quindi ha bisogno di essere messa in un posto che sicuramente sarà sempre istanziato. Dentro il suo metodo "present" penserà lui stesso a presentare il corretto flusso (login, walkthrough, main, ecc..)
public typealias MainCoordinator = Coordinator & RootingProtocol & RootPresentingProtocol
/// - description: E' il il protocollo per un coordinator normale
public typealias StandardCoordinator = Coordinator & RootingProtocol & PresentingProtocol
/// - description: Lo StandardTabCoordinator  non ha bisogno di presentare nulla, è il TabBarCoordinator che prende i rootViewController da questi e li inserisce a schermo.
public typealias StandardTabCoordinator = Coordinator & RootProtocol & PresentingTaskConditionProtocol
