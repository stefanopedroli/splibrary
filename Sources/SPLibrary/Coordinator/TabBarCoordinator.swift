//  Created by Stefano Pedroli on 10/09/2020.

import UIKit

open class TabBarCoordinator<Section: RawRepresentable, RootViewController: TabViewControllerProtocol>: Coordinator where Section: TabSection, Section.RawValue == String {
    
    open var rootViewController: RootViewController = .init()
        
    override public init(services: Services) {
        super.init(services: services)
    }
    
    public func add(childCoordinator: StandardTabCoordinator) {
        self.childCoordinators.append(coordinator: childCoordinator)
    }
    
    public func getCoordinator(sectionType: Section) -> StandardTabCoordinator? {
        return self.childCoordinators.getCoordinator(coordinator: sectionType.type) as? StandardTabCoordinator
    }
    
    public func addController(_ root: RootViewController, controller: UIViewController) {
        root.addChild(controller)
        controller.view.frame = root.contentView.bounds
        root.contentView.addSubview(controller.view)
        controller.didMove(toParent: root)
    }
    
    public func removeController(controller: UIViewController) {
        controller.willMove(toParent: nil)
        controller.view.safeRemoveFromSuperview()
        controller.removeFromParent()
    }
}

public protocol TabSection {
    var type: StandardTabCoordinator.Type { get }
    var title: String? { get }
    var description: String { get }
    var icon: UIImage { get }
}

/// - description: Protocol for your TabBarCoordinator
public protocol TabCoordinatorProtocol {
    associatedtype T: RawRepresentable where T: TabSection, T.RawValue == String
    var sections: [T] { get set }
    var currentCoordinatorType: T { get set }
    func present(from window: UIWindow?, isFromDeepLinkRequest: Bool) // automatically switch to root
}

//public protocol TabViewControllerDelegate: class {
//    associatedtype T: RawRepresentable where T: TabSection, T.RawValue == String
//    associatedtype GenericTabViewController: TabViewControllerProtocol
//    func didTappedItem(_ tabBarViewController: GenericTabViewController, type: T)
//}

public protocol TabViewControllerProtocol: UIViewController {
    associatedtype T: RawRepresentable where T: TabSection, T.RawValue == String
    var contentView: UIView! { get set }
    var sections: [T] { get set }
    var currentSection: T { get set }
//    var delegate: TabViewControllerDelegate? { get set }
    
}
