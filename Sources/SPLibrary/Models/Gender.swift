//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 15/01/2020.
//

import Foundation

public enum Gender: String, Codable {
    case male
    case female
    case other
    
    public var formattedDescription: String {
        if NSLocale.language == "it" {
            return self.it
        } else { return self.en }
    }
    
    private var it: String {
        switch self {
        case .male: return "Maschio"
        case .female: return "Femmina"
        case .other: return "Altro"
        }
    }
    
    private var en: String {
        switch self {
        case .male: return "Male"
        case .female: return "Female"
        case .other: return "Other"
        }
    }
    
    mutating func opposit() {
        switch self {
        case .male: self = .female
        case .female: self = .male
        default: break
        }
    }
    
    var oppositGender: Gender {
        switch self {
        case .male: return .female
        case .female: return .male
        default: return self
        }
    }
    
    var finalLetter: String {
        switch self {
        case .male: return "o"
        case .female: return "a"
        default: return "o/a"
        }
    }
}
