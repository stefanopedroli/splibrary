//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 06/03/2020.
//

import Foundation

public enum ExternalApp: String, CaseIterable {
    case mail = "message"
    case gmail = "googlegmail"
    case instagram = "instagram"
    #if DEBUG
    case satispay = "satispay-stag"
    #else
    case satispay = "satispay"
    #endif
    
    var path: String {
        return self.rawValue + "://"
        // googlegmail may required ":///"
    }
    
    public var name: String {
        switch self {
        case .mail: return "Mail"
        case .gmail: return "Gmail"
        case .instagram: return "Instagram"
        case .satispay: return "Satispay"
        }
    }
    
    public static func emailApps() -> [ExternalApp] {
        return [.mail, .gmail]
    }
}
