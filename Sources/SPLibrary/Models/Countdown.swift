//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 20/01/2020.
//

import UIKit

public protocol CountdownDelegate: class {
    func countdownDidEnd(_ countdown: Countdown)
}

public final class Countdown {
    
    public weak var delegate: CountdownDelegate?
    
    private var timer: Timer = Timer()
    private var currentSeconds: Int = 0
    private var formatType: CountdownFormat = .full
    var seconds: Int = 0
    var identifier: String?
    
    private var label: UILabel!
    
    public init(label: UILabel, seconds: Int, formatType: CountdownFormat, identifier: String) {
        self.label = label
        self.seconds = seconds
        self.formatType = formatType
        self.identifier = identifier
    }
    
    public init(label: UILabel, minutes: Int, formatType: CountdownFormat, identifier: String) {
        self.label = label
        self.seconds = minutes*60
        self.formatType = formatType
        self.identifier = identifier
    }
    
    public func start() {
        self.currentSeconds = seconds
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        self.label.text = timeString(time: TimeInterval(currentSeconds))
    }
    
    public func reset() {
        self.timer.invalidate()
        self.currentSeconds = seconds
        self.label.text = timeString(time: TimeInterval(currentSeconds))
    }
    
    public func pause() {
        self.timer.invalidate()
        self.label.text = timeString(time: TimeInterval(currentSeconds))
    }
    
    public func resume() {
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        self.label.text = timeString(time: TimeInterval(currentSeconds))
    }
    
    public func end() {
        self.timer.invalidate()
        self.label.text = nil
        self.delegate = nil
    }
    
    @objc private func updateTimer() {
        if currentSeconds == 1 {
            timer.invalidate()
            didEnd()
        } else {
            self.currentSeconds -= 1
            self.label.text = timeString(time: TimeInterval(currentSeconds))
        }
    }
    
    private func didEnd() {
        self.delegate?.countdownDidEnd(self)
    }
    
    private func timeString(time: TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        switch self.formatType {
        case .full:
            return String(format: "%02i:%02i:%02i", hours, minutes, seconds)
        case .seconds_and_minutes:
            return String(format: "%02i:%02i", minutes, seconds)
        case .only_seconds:
            return String(currentSeconds)
        }
    }
    
    public enum CountdownFormat {
        case only_seconds
        case seconds_and_minutes
        case full
    }
}
