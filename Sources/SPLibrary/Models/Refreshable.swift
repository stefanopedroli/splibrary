//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 03/03/21.
//

import UIKit
import Lottie

public protocol Refreshable: class {
    var isLoading: Bool { get set }

    func setupRefreshLoader() -> UIRefreshControl?
    func isStartRefreshing()
}

public extension Refreshable {
    
    func setupRefreshLoader() -> UIRefreshControl? {
        return .default
    }
    
    func internalStartRefreshing() {
        self.isLoading = true
        self.isStartRefreshing()
    }
    
    func internalStopRefreshing() {
        self.isLoading = false
    }
}

public extension UIRefreshControl {
    
    static var `default`: UIRefreshControl { return .createRefresh(title: nil, color: .mainTheme) }
    
    static func createRefresh(title: String?, color: UIColor) -> UIRefreshControl {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = color
        if let title = title { refreshControl.attributedTitle = NSAttributedString(string: title) }
        return refreshControl
    }
}

extension Refreshable where Self: TableViewSetup {
    internal func addRefresh(refreshControl: UIRefreshControl) {
        self.tableView.alwaysBounceVertical = true
        (self as? UIViewController)?.extendedLayoutIncludesOpaqueBars = true
        refreshControl.addTarget(self.tableView, action: #selector(self.tableView.startRefreshing), for: UIControl.Event.valueChanged)
        self.tableView.contentInsetAdjustmentBehavior = .automatic
        if (self as? UIViewController)?.navigationController?.navigationBar.prefersLargeTitles == true {
            self.tableView.refreshControl = refreshControl
        } else {
            self.tableView.refreshControl = refreshControl
        }
    }
}

public extension UITableView {
    
    func addRefresh(title: String? = nil, color: UIColor = .mainTheme, target: Any?, action: Selector) {
        self.alwaysBounceVertical = true
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = color
        if let title = title { refreshControl.attributedTitle = NSAttributedString(string: title) }
        refreshControl.addTarget(target, action: action, for: UIControl.Event.valueChanged)
//        self.backgroundView = refreshControl
        self.refreshControl = refreshControl
//        self.addSubview(refreshControl)
    }
    
    func addLottieRefresh(animationName: String, target: Any?, action: Selector) {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .clear
        refreshControl.addTarget(target, action: action, for: UIControl.Event.valueChanged)
        let animatedView: AnimationView = .init(name: animationName)
        animatedView.loopMode = .loop
        animatedView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        animatedView.center = refreshControl.center
        animatedView.play()
        refreshControl.addSubview(animatedView)
        self.addSubview(refreshControl)
    }
    
    @objc internal func startRefreshing() {
        if let refreshing = self.parentViewController as? Refreshable {
            refreshing.internalStartRefreshing()
        }
    }
    
    func stopRefresh() {
        DispatchQueue.main.async {
            for case let segmented as UIRefreshControl in self.subviews {
                for subview in segmented.subviews {
                    if let animation = subview as? AnimationView {
                        animation.stop()
                    }
                }
                if let refreshControl = self.refreshControl {
                    refreshControl.endRefreshing()
                } else {
                    segmented.endRefreshing()
                }
            }
//            self.refreshControl = nil
            self.refreshControl?.endRefreshing()
            self.parentViewController?.navigationController?.navigationBar.sizeToFit()
            if let refreshing = self.parentViewController as? Refreshable {
                refreshing.internalStopRefreshing()
            }
        }
    }
}
