//  Created by Stefano Pedroli on 15/09/2020.

import Foundation

public enum CodingError: Swift.Error {
    case parsingError
    
    public var localizedDescription: String {
        switch self {
        case .parsingError: return "Error parsing data"
        }
    }
}
