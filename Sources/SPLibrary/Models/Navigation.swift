//  Created by Stefano Pedroli on 28/11/2019.

import UIKit

public extension UIViewController {
    var navigation: Navigation { return Navigation(reference: self) }
}

public final class Navigation {
    
    private var reference: UIViewController?
    private var nav: UINavigationBar?
    private var navItem: UINavigationItem?
    
    public init(reference: UIViewController) {
        self.reference = reference
        self.nav = reference.navigationController?.navigationBar
        self.navItem = reference.navigationItem
    }
    
    public func addItem(position: NavigationPosition, item: UIBarButtonItem) {
        switch position {
        case .left:
            self.navItem?.leftBarButtonItem = item
        case .right:
            self.navItem?.rightBarButtonItem = item
        }
    }
    
    public func config(left: UIBarButtonItem?, title: String, right: UIBarButtonItem?) {
        self.navItem?.leftBarButtonItem = left
        self.navItem?.rightBarButtonItem = right
        self.reference?.title = title
    }
    
    public func addItems(position: NavigationPosition, items: [UIBarButtonItem]) {
        switch position {
        case .right:
            self.navItem?.rightBarButtonItems = items
        case .left:
            self.navItem?.leftBarButtonItems = items
        }
    }
    
    public func show(vc: UIViewController) {
        self.reference?.show(vc, sender: nil)
    }
    
    public func push(vc: UIViewController) {
        self.reference?.navigationController?.pushViewController(vc, animated: true)
    }
    
    public var isGradient: Bool = false {
        didSet { self.updateColor() }
    }
    
    public var color: UIColor = .white {
        didSet { self.updateColor() }
    }
    
    public var gradientColor: GradientStyle = .defaultStyle {
        didSet { self.updateColor() }
    }
    
    public func preferedLargeTitles(value: Bool = true) {
        self.nav?.prefersLargeTitles = value
    }
    
    private func updateColor() {
        if isGradient {
            self.nav?.setGradientBackground(gradientStyle: .defaultStyle, gradientOrientation: .leftRight)
        } else {
            self.nav?.removeGradient(replace: self.color)
        }
    }
    
    public enum NavigationPosition {
        case left, right
    }
}
