//  Created by Stefano Pedroli on 25/11/2019.

import UIKit

open class TappableLabel: UILabel {
    
    typealias TapableLink = (range: NSRange, action: () -> Swift.Void)
    
    // MARK: Vars
    private lazy var linkGestures: UITapGestureRecognizer = .init(target: self, action: #selector(tapLabel(gesture:)))
    private var links: [TapableLink] = []
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        self.addGestureRecognizer(linkGestures)
        self.isUserInteractionEnabled = true
    }
    
    public func addLink(link: String, for substring: String, color: UIColor? = nil, underlined: Bool = false, action: @escaping() -> Swift.Void) {
        guard let string = self.text ?? self.attributedText?.string else { return assertionFailure("hai tappato un label vuoto") }
        let range = (string as NSString).range(of: substring)
        let tupla: TapableLink = (range: range, action: action)
        if !links.contains(where: { $0.range == tupla.range } ) {
            links.append(tupla)
        } else { /* link already added */ }
        
        switch (underlined, color) {
        case (false, .none): break
        case (false, .some(let color)):
            self.color(substring: substring, color: color)
        case (true, .none):
            self.underline(substring: substring)
        case (true, .some(let color)):
            self.underline(substring: substring, color: color)
        }
    }
    
    public func underline(substring: String, color: UIColor? = nil, textColor: UIColor? = nil, action: @escaping() -> Swift.Void) {
        guard let string = self.text ?? self.attributedText?.string else { return assertionFailure("hai tappato un label vuoto") }
        let range = (string as NSString).range(of: substring)
        let tupla: TapableLink = (range: range, action: action)
        if !links.contains(where: { $0.range == tupla.range } ) {
            links.append(tupla)
        } else { /* link already added */ }
        self.underline(substring: substring, color: color, textColor: textColor)
    }
    
    @objc private func tapLabel(gesture: UITapGestureRecognizer) {
        for link in links {
            if gesture.didTapAttributedTextInLabel(label: self, inRange: link.range) {
                link.action()
            }
        }
    }
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)

        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)

        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize

        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(
            x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
            y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y
        )
        let locationOfTouchInTextContainer = CGPoint(
            x: locationOfTouchInLabel.x - textContainerOffset.x,
            y: locationOfTouchInLabel.y - textContainerOffset.y
        )
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)

        return NSLocationInRange(indexOfCharacter, targetRange)
    }
}
