//  Created by Stefano Pedroli on 28/11/2019.

import UIKit

public extension UIViewController {
    
    func getBarButton<T: UIImageRapresentable>(type: T, action: Selector?) -> UIBarButtonItem {
        return UIBarButtonItem(image: type.image, style: .plain, target: self, action: action)
    }
    
    func getBarButton(title: String, action: Selector?) -> UIBarButtonItem {
        return UIBarButtonItem(title: title, style: .plain, target: self, action: action)
    }
    
    /// - warning: Did never work if you don't put image on you asset, waiting apple introduce resources
    func getIntelligentBack(action: Selector?) -> UIBarButtonItem {
        let image: UIImage
        let defaultAction: Selector
        if self.navigationController?.viewControllers[0] == self {
            image = SPLibrary.configurations.intelligentBack_closeIcon
            defaultAction = action ?? #selector(close)
        } else {
            image = SPLibrary.configurations.intelligentBack_backIcon
            defaultAction = action ?? #selector(back)
        }
        return UIBarButtonItem(image: image, style: .plain, target: self, action: defaultAction)
    }
    
    func getNormalClose(action: Selector? = nil) -> UIBarButtonItem {
        let image: UIImage = SPLibrary.configurations.intelligentBack_closeSecondaryIcon
        return UIBarButtonItem(image: image, style: .plain, target: self, action: action ?? #selector(close))
    }
    
    func getNormalBack(action: Selector? = nil) -> UIBarButtonItem {
        let image: UIImage = SPLibrary.configurations.intelligentBack_backIcon
        return UIBarButtonItem(image: image, style: .plain, target: self, action: action ?? #selector(close))
    }
    
    func getIntelligentBack(overrideBackAction: Selector?, overrideCloseAction: Selector?) -> UIBarButtonItem {
        let image: UIImage
        let defaultAction: Selector
        if self.navigationController?.viewControllers[0] == self {
            image = SPLibrary.configurations.intelligentBack_closeIcon
            defaultAction = overrideCloseAction ?? #selector(close)
        } else {
            image = SPLibrary.configurations.intelligentBack_backIcon
            defaultAction = overrideBackAction ?? #selector(back)
        }
        return UIBarButtonItem(image: image, style: .plain, target: self, action: defaultAction)
    }
    
    var intelligentBack: UIBarButtonItem { return getIntelligentBack(action: nil) }
}

public protocol UIImageRapresentable {
    var image: UIImage { get }
}
