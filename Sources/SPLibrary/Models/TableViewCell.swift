//  Created by Stefano Pedroli on 07/10/2020.

import UIKit

open class TableViewCellSelectable: UITableViewCell, NibReusable {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    open var selectionTheme: SelectionTheme = .mainTheme
        
    public enum SelectionTheme {
        case mainTheme, secondTheme, gray, blu, custom(color: UIColor), none
        var color: UIColor {
            switch self {
            case .mainTheme: return UIColor.mainTheme
            case .secondTheme: return UIColor.secondTheme.normal
            case .gray: return UIColor.grayPalette(heavy: .light)
            case .blu: return UIColor.systemBlue
            case .custom(let color): return color
            case .none: return.clear
            }
        }
    }
    
    /// - description: Is internal beacause selection is automatically managed
    final public override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            self.contentView.backgroundColor = self.selectionTheme.color
        } else {
            self.contentView.backgroundColor = self.backgroundColor
        }
        self.setSelectedCell(selected, animated: animated)
    }
    
    /// - description: Is the 'setSelected' original method
    public func setSelectedCell(_ selected: Bool, animated: Bool) {}
}
