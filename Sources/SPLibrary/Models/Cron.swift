//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 22/01/21.
//

import Foundation
import Dispatch

open class Cron {
 
    internal let timer = DispatchSource.makeTimerSource()
    
    /// - description: default load handler each hours
    public init(deadline: DispatchTime = (.now() + .seconds(3600)), interval: DispatchTimeInterval = .seconds(3600), leeway: DispatchTimeInterval = .seconds(60), handler: DispatchSourceTimer.DispatchSourceHandler?) {
        self.timer.setEventHandler(handler: handler)
        self.timer.schedule(deadline: deadline, repeating: interval, leeway: leeway)
    }
    
    public convenience init(each: CycleType, handler: DispatchSourceTimer.DispatchSourceHandler?) {
        let deadline: DispatchTime = each.deadline
        let interval: DispatchTimeInterval = each.interval
        let leeway: DispatchTimeInterval = each.leeway
        self.init(deadline: deadline, interval: interval, leeway: leeway, handler: handler)
    }
    
    public enum CycleType {
        
        case hour
        case minute
        
        var deadline: DispatchTime {
            switch self {
            case .hour: return (.now() + .seconds(Date.secondUntileNextHour))
            case .minute: return (.now() + .seconds(Date.secondUntileNextMinute))
            }
        }
        
        var interval: DispatchTimeInterval {
            switch self {
            case .hour: return .seconds(3600)
            case .minute: return .seconds(60)
            }
        }
        
        var leeway: DispatchTimeInterval {
            switch self {
            case .hour: return .seconds(60)
            case .minute: return .seconds(60)
            }
        }
    }
    
    public func start() {
        self.timer.activate()
    }
}

import BackgroundTasks

@available(iOS 13.0, *)
open class BackgroundProcess {
    
    public enum BackgroundProcessIdentifier: String {
        case refreshApp = "com.example.apple.Travaj.refresh"
    }
    
    internal static var scheduler: BGTaskScheduler { return .shared }
    
    internal static func retrieveIdentifier() -> Array<String>? {
        guard let identifiers = Bundle.main.object(forInfoDictionaryKey: "BGTaskSchedulerPermittedIdentifiers") as? Array<String> else {
            assertionFailure("You forgot to add 'BGTaskSchedulerPermittedIdentifiers' key in info.plist")
            return nil
        }
        return identifiers
    }
    
    internal static func validateIdentifier(identifier: BackgroundProcessIdentifier) -> Bool {
        guard let identifiers = Bundle.main.object(forInfoDictionaryKey: "BGTaskSchedulerPermittedIdentifiers") as? Array<String> else {
            assertionFailure("You forgot to add 'BGTaskSchedulerPermittedIdentifiers' key in info.plist")
            return false
        }
        if identifiers.contains(identifier.rawValue) {
            return true
        } else {
            assertionFailure("You forgot to add add your identifier ('\(identifier.rawValue)') value on BGTaskSchedulerPermittedIdentifiers in info.plist")
            return false
        }
    }
    
    // after 5 min
    public static func scheduleJob(identifier: BackgroundProcessIdentifier, taks: @escaping VoidHandler, queue: DispatchQueue? = nil, time: TimeInterval = 5*60) {
        if self.validateIdentifier(identifier: identifier) {
            self.scheduler.register(forTaskWithIdentifier: identifier.rawValue, using: queue) { backgroundTask in
                self.handleAppRefresh(handler: taks, backgroundTask: backgroundTask as! BGAppRefreshTask, identifier: identifier, time: time)
            }
        }
    }
    
    internal static func scheduleAppRefresh(identifier: BackgroundProcessIdentifier, time: TimeInterval) {
        let request = BGAppRefreshTaskRequest(identifier: identifier.rawValue)
        request.earliestBeginDate = Date(timeIntervalSinceNow: time)
        do {
            try self.scheduler.submit(request)
        } catch {
            print("Could not schedule app refresh: \(error)")
        }
    }
    
    internal static func handleAppRefresh(handler: @escaping VoidHandler, backgroundTask: BGAppRefreshTask, identifier: BackgroundProcessIdentifier, time: TimeInterval) {
        // Schedule a new refresh task
        scheduleAppRefresh(identifier: identifier, time: time)
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        let operation: BackgroundOperation = .init(task: handler)
        backgroundTask.expirationHandler = {
            // After all operations are cancelled, the completion block below is called to set the task to complete.
            queue.cancelAllOperations()
        }
        operation.completionBlock = {
            backgroundTask.setTaskCompleted(success: true)
        }
        queue.addOperation(operation)
    }
}
 
open class BackgroundOperation: Operation {
    
    var task: VoidHandler
    
    public required init(task: @escaping VoidHandler) {
        self.task = task
    }
    
    open override func main() {
        self.task()
    }
}
