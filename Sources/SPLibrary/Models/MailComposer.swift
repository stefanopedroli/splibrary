//  Created by Stefano Pedroli on 31/01/2020.

import Foundation
import MessageUI

public final class MailComposer: NSObject {
    
    public struct EmailObject {
        
        public let recipients: [String]
        public let ccRecipients: [String]
        public let bccRecipients: [String]
        public let subject: String?
        public let body: String?
        public let htmlBody: Bool
        
        public init(recipients: [String], ccRecipients: [String] = [], bccRecipients: [String] = [], subject: String? = nil, body: String? = nil, isHtmlBody: Bool = false) {
            self.recipients = recipients
            self.ccRecipients = ccRecipients
            self.bccRecipients = bccRecipients
            self.subject = subject
            self.body = body
            self.htmlBody = isHtmlBody
        }
    }
    
    private override init () {}
    private static var shared: MailComposer = .init()
    
    public typealias MailComposerSuccessHandler = (_ result: MFMailComposeResult) -> Swift.Void
    private var successHandler: MailComposerSuccessHandler?
    
    public static func presentEmailComposeViewController(from presentingViewController: UIViewController, email: EmailObject, completion: @escaping MailComposerSuccessHandler) {
        shared.successHandler = completion
        if MFMailComposeViewController.canSendMail() {
            let mailViewController = MFMailComposeViewController()
            mailViewController.mailComposeDelegate = shared
            mailViewController.setToRecipients(email.recipients)
            mailViewController.setCcRecipients(email.ccRecipients)
            mailViewController.setBccRecipients(email.bccRecipients)
            if let subject = email.subject { mailViewController.setSubject(subject) }
            if let body = email.body { mailViewController.setMessageBody((body), isHTML: email.htmlBody) }
            presentingViewController.present(mailViewController, animated: true, completion: nil)
        }
    }
    
    public static func openEmail(email: String) {
        App.openApp(type: .mail)
    }
}

extension MailComposer: MFMailComposeViewControllerDelegate {
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Cancelled")
        case MFMailComposeResult.saved.rawValue:
            print("Saved")
        case MFMailComposeResult.sent.rawValue:
            print("Sent")
        case MFMailComposeResult.failed.rawValue:
            print("Error: \(String(describing: error?.localizedDescription))")
        default:
            break
        }
        self.successHandler?(result)
        controller.dismiss(animated: true, completion: nil)
    }
}

// MARK: - Open Email Application External Service
public extension MailComposer {
    
    static func openMailApp() {
        if let url = URL(string: ExternalApp.mail.rawValue) {
            UIApplication.shared.open(url)
        }
    }
    
    typealias OpenEmailHandler = (_ service: ExternalApp) -> Swift.Void
    
    static func openAllMailService(from presentingViewController: UIViewController, title: String = String(), completion: @escaping OpenEmailHandler) {
        let actionSheet = UIAlertController.init(title: title, message: "", preferredStyle: .actionSheet)
        for service in ExternalApp.emailApps() {
            if let url = URL(string: service.rawValue) {
                let action = UIAlertAction.init(title: service.name, style: .default) { (_action) in
                    completion(service)
                    UIApplication.shared.open(url)
                }
                actionSheet.addAction(action)
            }
        }
        actionSheet.addAction(.cancel)
        presentingViewController.present(actionSheet, animated: true, completion: nil)
    }
}
