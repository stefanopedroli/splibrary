// Created by Stefano Pedroli on 29/07/2020.

import UIKit

open class UIImageCodable: Codable {
    
    public var image: UIImage
    
    public init(image: UIImage) {
        self.image = image
    }

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let data = try? container.decode(Data.self), let decodedImage = UIImage(data: data) {
            self.image = decodedImage
        } else {
            throw CodingError.parsingError
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        do {
        try container.encode(image.pngData())
        } catch {
            print(error)
        }
    }
}

public extension UIImage {
    var imageCodable: UIImageCodable { return UIImageCodable(image: self) }
}

public extension Array where Element == UIImage? {
    var imagesCodable: [UIImageCodable] {
        var codableImages: [UIImageCodable] = []
        for image in self {
            if let img = image {
                codableImages.append(img.imageCodable)
            }
        }
        return codableImages
    }
}

public extension Array where Element == UIImageCodable {
    var uiimages: [UIImage] {
        var _images: [UIImage] = []
        for element in self {
            _images.append(element.image)
        }
        return _images
    }
}
