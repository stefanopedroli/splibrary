// Created by Stefano Pedroli on 14/09/2020.

import UIKit

public enum PresentingOptions {
    
    case windows(window: UIWindow)
    case switchToRoot
    case presented(presentingViewController: UIViewController, embedded: EmbeddedNavigationType?)
    case show(presentingViewController: UIViewController)
    
    public func present(presentedViewController: UIViewController, modalPresentationStyle: UIModalPresentationStyle = .overCurrentContext, animated: Bool, completion: VoidHandler? = nil) {
        switch self {
        case .windows(let window):
            window.rootViewController = presentedViewController
            window.makeKeyAndVisible()
        case .switchToRoot:
            switchRootViewController(rootViewController: presentedViewController, animated: animated)
        case .presented(let presentingViewController, let embedded):
            let navigation = presentedViewController.embeddInNavigationIfNeeded(type: embedded)
            navigation.modalPresentationStyle = modalPresentationStyle
            presentingViewController.present(navigation, animated: animated, completion: completion)
        case .show(let presentingViewController):
            presentingViewController.show(presentedViewController, sender: nil)
        }
    }
    
    public var window: UIWindow? {
        switch self {
        case .windows(let _window): return _window
        default: return nil
        }
    }
    
    public var presentingViewController: UIViewController? {
        switch self {
        case .presented(let _presentingViewController, _): return _presentingViewController
        case .show(let _presentingViewController): return _presentingViewController
        default: return nil
        }
    }
}
