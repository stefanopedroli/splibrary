//  Created by Stefano Pedroli on 25/11/2019.

import UIKit
#if !TARGET_OS_TV
import WebKit

open class WebViewController: UIViewController {
    
    public let webView = WKWebView(frame: .zero)
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        webView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.webView)
        self.view.backgroundColor = UIColor.white
        // You can set constant space for Left, Right, Top and Bottom Anchors
        NSLayoutConstraint.activate([
            self.webView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            self.webView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            self.webView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            self.webView.topAnchor.constraint(equalTo: self.view.topAnchor),
        ])
        // For constant height use the below constraint and set your height constant and remove either top or bottom constraint
        //self.webView.heightAnchor.constraint(equalToConstant: 200.0),

        self.view.setNeedsLayout()
        self.navigation.addItem(position: .left, item: intelligentBack)
        updateShare()
    }
    
    public static var shareIcon: UIImage?
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    public var isShareEnable: Bool = false {
        didSet { updateShare() }
    }
    
    private func updateShare() {
        if isShareEnable {
            if let icon = Self.shareIcon {
                self.navigation.addItem(position: .right, item: .init(image: icon, style: .plain, target: self, action: #selector(share)))
            } else {
                self.navigation.addItem(position: .right, item: .init(title: "share", style: .done, target: self, action: #selector(share)))
            }
        } else {
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    
    public var overrideShare: VoidHandler?
    
    @objc private func share() {
        guard let url = url else { return }
        if let overrideShare = overrideShare { return overrideShare() }
        var items: [Any] = []
        switch url.fileExtensionType.category {
        case .document: items = [NSURL.fileURL(withPath: url)]
        case .image:
            if url.fileExtensionType.category == .image, let imageurl = URL(string: url), let data = try? Data(contentsOf: imageurl) {
                let image: UIImage = UIImage(data: data)!
                items = [image]
            }
        default: items = [url]
        }
    
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        DispatchQueue.main.async {
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    private func loadUrl(url: String) {
        guard let _url = URL(string: url) else { return }
        let request = URLRequest(url: _url)
        self.webView.load(request)
    }
    
    private func loadHTML(html: String) {
        self.webView.loadHTMLString(html, baseURL: nil)
    }
    
    public var url: String? { // "https://www.google.com"
        didSet { if let _url = url { self.loadUrl(url: _url) } }
    }
    
    public var html: String? {
        didSet { if let _html = html { self.loadHTML(html: _html) } }
    }
    
    @discardableResult
    public static func presentWebView(from presentingViewController: UIViewController, url: String, title: String? = nil, share: Bool = false) -> WebViewController {
        let webViewController: WebViewController = WebViewController()
        webViewController.url = url
        webViewController.title = title
        webViewController.isShareEnable = share
        presentingViewController.show(webViewController, sender: self)
        return webViewController
    }
    
    @discardableResult
    public static func presentWebView(from presentingViewController: UIViewController, html: String, title: String? = nil) -> WebViewController {
        let webViewController: WebViewController = WebViewController()
        webViewController.html = html
        webViewController.title = title
        webViewController.isShareEnable = false
        presentingViewController.show(webViewController, sender: self)
        return webViewController
    }
}
#endif
