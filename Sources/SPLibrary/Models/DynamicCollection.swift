//  Created by Stefano Pedroli on 28/11/2019.

import UIKit

public final class DynamicCollectionView: UICollectionView {
    
    public  override func layoutSubviews() {
        super.layoutSubviews()
        if !__CGSizeEqualToSize(bounds.size, self.intrinsicContentSize) {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    public override var intrinsicContentSize: CGSize {
        return contentSize
    }
    
    // questo forse è eccessivo
    public override func reloadData() {
        super.reloadData()
        self.layoutIfNeeded()
    }
}
