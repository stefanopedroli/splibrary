//  Created by Stefano Pedroli on 17/11/2019.
import UIKit

// Init app

public final class SPLibrary {
    
    static var shared: SPLibrary = .init()
    
    // Colors Palette
    var colors: Colors = SPColors()
    public static func theme(colors: Colors) {
        shared.colors = colors
    }
    
    public static var familyFontName: String?
    
    static public var configurations: Configurations { return shared.settings }
    
    private var settings: Configurations = .init()
    
    // Navigation Themes
    var navigationTheme: NavigationTheme = .init()
    public func setNavigationTheme(theme: NavigationTheme) {
        self.navigationTheme = theme
    }
}
