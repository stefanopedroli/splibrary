//  Created by Stefano Pedroli on 07/01/2020.

import UIKit

final public class SearchController: UISearchController {

    override public func viewDidLoad() {
        super.viewDidLoad()
    }
    
    public convenience init(controller: (UISearchResultsUpdating & UISearchControllerDelegate), placeholder: String? = nil) {
        self.init(searchResultsController: nil)
        self.searchResultsUpdater = controller
        self.obscuresBackgroundDuringPresentation = false
        self.searchBar.placeholder = placeholder
        self.delegate = controller
    }
    
    public func applyTheme(theme: Theme) {
        searchBar.tintColor = theme.backgrountAndTintColor
        searchBar.barTintColor = UIColor.mainTheme
        searchBar.backgroundColor = UIColor.mainTheme
        searchBar.searchBarStyle = .prominent
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            textfield.backgroundColor = theme.backgrountAndTintColor
            textfield.tintColor = theme.textColor
            textfield.textColor = theme.textColor
            if let backgroundview = textfield.subviews.first {
                backgroundview.tintColor = theme.textColor
                backgroundview.backgroundColor = theme.backgrountAndTintColor
                backgroundview.layer.cornerRadius = 10
                backgroundview.clipsToBounds = true
            }
        }
    }
    
    public func applyGradient(gradient: GradientStyle? = nil) {
        // lo si implementerà poi
    }
}

public extension SearchController {
    
    enum Theme {
        case lightContent, darkContent, mainThemeBased
        
        var backgrountAndTintColor: UIColor {
            switch self {
            case .lightContent: return UIColor.mainThemeWhite
            case .darkContent: return UIColor.grayPalette(heavy: .medium)
            case .mainThemeBased: return UIColor.mainThemeWhite
            }
        }
        
        var textColor: UIColor {
            switch self {
            case .lightContent: return UIColor.mainThemeBlack
            case .darkContent: return UIColor.mainThemeWhite
            case .mainThemeBased: return UIColor.mainTheme
            }
        }
    }
}

public extension UIViewController {
    
    /// - warning: may not working from iOS 13
    func setCustomSearchController(delegate: (UISearchResultsUpdating & UISearchControllerDelegate),
                                   placeholeder: String = "Search...",
                                   contentTheme: SearchController.Theme) {
        let search = SearchController(controller: delegate)
        search.applyTheme(theme: contentTheme)
        self.navigationItem.searchController = search
        if #available(iOS 13, *) {
            navigationItem.hidesSearchBarWhenScrolling = false
        }
        definesPresentationContext = true
        extendedLayoutIncludesOpaqueBars = true
    }
}
