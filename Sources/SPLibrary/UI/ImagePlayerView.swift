//  Created by Stefano Pedroli on 23/09/
import UIKit

open class ImagePlayerViewController: UIViewController {
    
    public let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.imageView)
        self.view.backgroundColor = UIColor.mainThemeBlack
        NSLayoutConstraint.activate([
            self.imageView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            self.imageView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            self.imageView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            self.imageView.topAnchor.constraint(equalTo: self.view.topAnchor),
        ])
        self.imageView.contentMode = .scaleAspectFit
    }
    
    public func load(image: UIImage) {
        self.imageView.image = image
        self.imageView.enableZoom()
    }
    
    public func load(path: String) {
        self.imageView.download(path: path)
        self.imageView.enableZoom()
    }
    
    public static func openImagePlayer(from presentingViewController: UIViewController, image: UIImage) {
        let imagePlayerViewController: ImagePlayerViewController = .init()
        imagePlayerViewController.load(image: image)
        let navigationController: UINavigationController = .init(rootViewController: imagePlayerViewController)
        navigationController.modalPresentationStyle = .overFullScreen
        navigationController.modalTransitionStyle = .crossDissolve
        presentingViewController.present(navigationController, animated: true, completion: nil)
        imagePlayerViewController.addNormalClose()
        imagePlayerViewController.navigationController?.navigationBar.transparentNavigationBar()
        imagePlayerViewController.navigationController?.navigationBar.tintColor = .mainThemeWhite
    }
    
    public static func openImagePlayer(from presentingViewController: UIViewController, path: String) {
        let imagePlayerViewController: ImagePlayerViewController = .init()
        imagePlayerViewController.load(path: path)
        let navigationController: UINavigationController = .init(rootViewController: imagePlayerViewController)
        navigationController.modalPresentationStyle = .overFullScreen
        navigationController.modalTransitionStyle = .crossDissolve
        presentingViewController.present(navigationController, animated: true, completion: nil)
        imagePlayerViewController.addNormalClose()
        imagePlayerViewController.navigationController?.navigationBar.transparentNavigationBar()
        imagePlayerViewController.navigationController?.navigationBar.tintColor = .mainThemeWhite
    }
}
