//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 17/12/2019.
//

import UIKit

public protocol DatePickerDelegate: class {
    func datePicker(_ datePicker: DatePicker, textField: UITextField?, selected date: Date)
}

public protocol TimePickerDelegate: class {
    func datePicker(_ datePicker: DatePicker, selected time: Time)
}

open class DatePicker: UIDatePicker {
    
    public var formattedText: Date.DateFormatExample = .normal
    public var formattedTime: Date.DateFormatExample = .hoursMinute
    
    public convenience init(textField: UITextField, mode: UIDatePicker.Mode, maxDate: Date? = nil) {
        self.init(frame: CGRect.zero)
        if #available(iOS 13.4, *) {
            self.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: 100.0)
            self.preferredDatePickerStyle = .automatic
            self.tintColor = .mainTheme
            self.backgroundColor = .mainThemeWhite
        } else {
            self.backgroundColor = .grayPalette(heavy: .extraLight)
        }
        associatedTextField = textField
        textField.inputView = self
        self.maximumDate = maxDate
        self.datePickerMode = mode
        self.addTarget(self, action: #selector(datePickerSelected(sender:)), for: .valueChanged)
    }
    
    public convenience init(mode: UIDatePicker.Mode, maxDate: Date? = nil) {
        self.init(frame: CGRect.zero)
        self.maximumDate = maxDate
        self.datePickerMode = mode
        self.addTarget(self, action: #selector(datePickerSelected(sender:)), for: .valueChanged)
    }
    
    public var initialTime: Time? {
        didSet {
            if self.datePickerMode == .time, let time = initialTime {
                self.date = Date()
                self.date.setTime(time: time)
                self.datePickerSelected(sender: self)
            }
        }
    }
    
    public var initialDate: Date? {
        didSet {
            if (self.datePickerMode == .date || self.datePickerMode == .dateAndTime), let _date = initialDate {
                self.date = _date
                self.datePickerSelected(sender: self)
            }
        }
    }
    
    public var dateDelegate: DatePickerDelegate?
    public var timeDelegate: TimePickerDelegate?
    
    private var associatedTextField: UITextField?
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    @objc private func datePickerSelected(sender: UIDatePicker) {
        switch self.datePickerMode {
        case .time:
            associatedTextField?.text = sender.date.getFormattedDate(format: self.formattedTime)
        default:
            associatedTextField?.text = sender.date.getFormattedDate(format: self.formattedText)
        }
        dateDelegate?.datePicker(self, textField: self.associatedTextField, selected: sender.date)
        timeDelegate?.datePicker(self, selected: Time(date: sender.date))
    }
}
