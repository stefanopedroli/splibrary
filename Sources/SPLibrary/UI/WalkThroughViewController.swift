//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 18/12/2019.
//

import UIKit

public protocol WalkThroughDelegate: class {
    /// - Description: Function that is called when you tap the *startButton* on the *lastPage*
    func walkThroughDidEnd(_ presentingViewController: UIViewController)
}

public protocol WalkPageButtonDelegate {
    /// - Warning: Remember that this function did not update *isWalkthroughtPresented* on preferenceService
    func buttonTapped(sender: Button, isLastPage: Bool)
}

public protocol WalkPageProtocol: class {
    /// - Description: The button that you can use for go next or for and, you can hide it when you don't want it
    var startButton: Button! { get set }
    /// - Description: Index of page (*automatically setted*)
    var index: Int { get set }
    /// - Description: If you want to use an enum for your pages
    associatedtype TypeOfPage
    /// - Description: Something that differentiate pages
    static var identifiers: [TypeOfPage] { get }
    /// - Description: Boolean that indicates if is the last page (*automatically setted*)
    var isLastPage: Bool { get set }

    var delegate: WalkPageButtonDelegate? { get set }
    /// - Description: Config method for your ViewController
    func config(for page: TypeOfPage)
    /// - Description: Your current section
    var currentSection: TypeOfPage { get set }
    /// - Description: Default action when button is tapped
    func buttonDidTapped(_ sender: Button)
}

public final class WalkThroughViewController<PageViewController: UIViewController>: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource where PageViewController: WalkPageProtocol, PageViewController: Setup {
    
    /// - description: Identifiers of your page
    lazy var pagesIdentifiers = PageViewController.identifiers
    public weak var walkThroughDelegate: WalkThroughDelegate?
    
    // MARK: Variables
    private var backgrounColor: UIColor = UIColor.mainTheme
    
    // MARK: - VIEW LIFECYCLE
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        self.view.backgroundColor = self.backgrounColor
        self.setViewControllers([getViewController(from: 0)], direction: .forward, animated: true, completion: nil)
        self.didMove(toParent: self)
//        addIntelligentBack()
    }
    
    public required init(transitionStyle style: UIPageViewController.TransitionStyle = .scroll,
                         navigationOrientation: UIPageViewController.NavigationOrientation = .horizontal,
                         options: [UIPageViewController.OptionsKey : Any]? = nil,
                         backgroundColor: UIColor = UIColor.mainTheme) {
        super.init(transitionStyle: style, navigationOrientation: navigationOrientation, options: options)
        self.backgrounColor = backgroundColor
        setupPageControll()
    }
    
    public func setupPageControll(indicatorColor: UIColor = UIColor.mainThemeExtraLight, selectedColor: UIColor = UIColor.secondTheme.normal, background: UIColor = UIColor.clear) {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = indicatorColor
        appearance.currentPageIndicatorTintColor = selectedColor
        appearance.backgroundColor = background
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func getViewController(from index: Int) -> PageViewController {
        let vc: PageViewController = .instantiate()
        vc.config(for: pagesIdentifiers[index])
        vc.index = index
        vc.isLastPage = index == (self.pagesIdentifiers.count - 1)
        vc.delegate = self
        return vc
    }

    // MARK: - UIPageViewControllerDelegate, UIPageViewControllerDataSource
    public func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return pagesIdentifiers.count
    }
    
    public func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        guard let current = pageViewController.viewControllers?.first as? PageViewController else { return 0 }
        return current.index
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let current = viewController as? PageViewController else { return nil }
        if current.index == 0 { return nil }
        let previouslyIndex = current.index - 1
        return getViewController(from: previouslyIndex)
    }
        
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let current = viewController as? PageViewController else { return nil }
        if current.index == (self.pagesIdentifiers.count - 1) { return nil }
        let nextIndex = current.index + 1
        return getViewController(from: nextIndex)
    }
}

extension WalkThroughViewController: WalkPageButtonDelegate where PageViewController: WalkPageProtocol, PageViewController: Setup {
    public func buttonTapped(sender: Button, isLastPage: Bool) {
        if isLastPage {
            self.walkThroughDelegate?.walkThroughDidEnd(self)
        } else {
            self.goToNextPage()
        }
    }
}

public extension UIPageViewController {
    /// - Warning: The pageControll *dots* do not update, you need to calculate the index in **presentationIndex** method (is opened in *WalkThroughViewController* class, you can see what to do)
    func goToNextPage(animated: Bool = true) {
        guard let currentViewController = self.viewControllers?.first else { return }
        guard let nextViewController = dataSource?.pageViewController(self, viewControllerAfter: currentViewController) else { return }
        setViewControllers([nextViewController], direction: .forward, animated: animated, completion: nil)
    }

    /// - Warning: The pageControll *dots* do not update, you need to calculate the index in **presentationIndex** method (is opened in *WalkThroughViewController* class, you can see what to do)
    func goToPreviousPage(animated: Bool = true) {
        guard let currentViewController = self.viewControllers?.first else { return }
        guard let previousViewController = dataSource?.pageViewController(self, viewControllerBefore: currentViewController) else { return }
        setViewControllers([previousViewController], direction: .reverse, animated: animated, completion: nil)
    }
}

