//  Created by Stefano Pedroli on 27/11/2019.

import UIKit

@IBDesignable
open class Button: UIButton {
    
    public enum Style: Int {
        case action, cancel, alternative, onlyBlackTitile, cancelTileBlack
    }
    
    public var style: Style = .action {
        didSet { styleChanged(style) }
    }
    
    override public var isHighlighted: Bool {
        didSet { isHighlightedChanged(isHighlighted) }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    public var cornerStyle: CornerRadiusStyle = .half {
        didSet { self.rounded(style: cornerStyle, withShadow: false) }
    }
    
    public var titleColor: UIColor? = nil {
        didSet { styleChanged(style) }
    }
    
    public var identifier: String?
    
    private func setUp() {
        layer.borderWidth = 2
        rounded(style: .softCorner, withShadow: false)
        layer.masksToBounds = true
        
        titleLabel?.textAlignment = .center
        titleLabel?.font = UIFont.libraryFont(ofSize: .defaulfFontSize, weight: .medium)
        
        styleChanged(style)
        isHighlightedChanged(isHighlighted)
    }
    
    private func styleChanged(_ style: Style) {
        let backgroundColor = style.backgroundColor(for: .normal)
        let backgroundImage = UIImage.color(backgroundColor, size: .one)
        setBackgroundImage(backgroundImage, for: .normal)
        
        let highlightedBackgroundColor = style.backgroundColor(for: .highlighted)
        let highlightedBackgroundImage = UIImage.color(highlightedBackgroundColor, size: .one)
        setBackgroundImage(highlightedBackgroundImage, for: .highlighted)
        
        setTitleColor(titleColor ?? style.titleColor(for: .normal), for: .normal)
        setTitleColor(titleColor ?? style.titleColor(for: .highlighted), for: .highlighted)
        
        self.layer.borderColor = style.borderColor(for: .normal).cgColor
    }
    
    private func isHighlightedChanged(_ isHighlighted: Bool) {
        layer.borderColor = style.borderColor(for: state).cgColor
    }
    
    override public func setTitle(_ title: String?, for state: UIControl.State) {
        super.setTitle(title?.uppercased(), for: state)
    }
    
    override public var intrinsicContentSize: CGSize {
        return CGSize(width: 245, height: 55)
    }
    
    override public func sizeToFit() {
        frame.size = intrinsicContentSize
    }
    
    override public func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: intrinsicContentSize.height)
    }
    
    // MARK: - Interface builder
    @IBInspectable
    public var styleValue: Int {
        get { return style.rawValue }
        set { if let style = Style(rawValue: newValue) { self.style = style } }
    }
}

private extension Button.Style {
    
    func backgroundColor(for state: UIControl.State) -> UIColor {
        switch (self, state) {
        case (.action, .highlighted):       return UIColor.mainTheme
        case (.action, _):                  return UIColor.mainTheme
        case (.cancel, .highlighted):       return UIColor.clear
        case (.cancel, _):                  return UIColor.clear
        case (.alternative, .highlighted):  return UIColor.secondTheme.normal
        case (.alternative, _):             return UIColor.secondTheme.normal
        case (.onlyBlackTitile, .highlighted):  return UIColor.clear
        case (.onlyBlackTitile, _):             return UIColor.clear
        case (.cancelTileBlack, .highlighted):  return UIColor.clear // 4
        case (.cancelTileBlack, _):             return UIColor.clear
        }
    }
    
    func borderColor(for state: UIControl.State) -> UIColor {
        switch (self, state) {
        case (.action, .highlighted):           return UIColor.clear
        case (.action, _):                      return UIColor.clear
        case (.cancel, .highlighted):           return UIColor.mainTheme
        case (.cancel, _):                      return UIColor.mainTheme
        case (.alternative, .highlighted):      return UIColor.clear
        case (.alternative, _):                 return UIColor.clear
        case (.onlyBlackTitile, .highlighted):  return UIColor.clear
        case (.onlyBlackTitile, _):             return UIColor.clear
        case (.cancelTileBlack, .highlighted):  return UIColor.mainTheme // 4
        case (.cancelTileBlack, _):             return UIColor.mainTheme
        }
    }
    
    func titleColor(for state: UIControl.State) -> UIColor {
        switch (self, state) {
        case (.action, .highlighted):       return UIColor.mainThemeBlack
        case (.action, _):                  return UIColor.mainThemeBlack
        case (.cancel, .highlighted):       return UIColor.mainThemeWhite
        case (.cancel, _):                  return UIColor.mainThemeWhite
        case (.alternative, .highlighted):  return UIColor.mainThemeWhite
        case (.alternative, _):             return UIColor.mainThemeWhite
        case (.onlyBlackTitile, .highlighted):  return UIColor.mainThemeBlack
        case (.onlyBlackTitile, _):             return UIColor.mainThemeBlack
        case (.cancelTileBlack, .highlighted):  return UIColor.mainThemeBlack // 4
        case (.cancelTileBlack, _):             return UIColor.mainThemeBlack
        }
    }
}
