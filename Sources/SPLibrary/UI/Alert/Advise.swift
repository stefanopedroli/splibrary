//  Created by Stefano Pedroli on 27/11/2019.

import UIKit

public protocol AdviseProtocol {
    var mainView: UIView! { get set }
    var presentingViewController: UIViewController? { get set }
}

/// - description: Class that determinate Alert presented over current viewController, and is possible to continue using the view behind
open class AdviseClass: UIView {
    
    var completionOnHideHandler: VoidHandler?

    func addSwipeToHide() {
        let slideDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissSwipeView(gesture:)))
        slideDown.direction = .up
        self.addGestureRecognizer(slideDown)
    }
    
    @objc func dismissSwipeView(gesture: UISwipeGestureRecognizer) {
        let offset = self.frame.origin.y
        UIView.animate(withDuration: 0.4) {
            self.frame.origin.y = offset - self.frame.height
        } completion: { [weak self] (animated) in guard let self = self else { return }
            self.removeFromSuperview()
            self.completionOnHideHandler?()
        }
    }
}

public extension AdviseProtocol where Self: AdviseClass {
    
    func show(completion: VoidHandler?, options: UIView.AnimationOptions = [.curveEaseIn]) {
        guard let alert = mainView, let vc = presentingViewController else { return }
        self.frame = vc.view.bounds
        vc.view.addSubview(self)
        // Apply a fade-in animation
        let transformView : UIView = UIView()
        transformView.transform = CGAffineTransform(scaleX: 0, y: 0)
        UIView.animate(withDuration: 0.1, delay: 0.0, options: options, animations: {
            transformView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            alert.alpha = 1
        }, completion: { [weak self] _ in guard self != nil else { return }
            UIAccessibility.post(notification: UIAccessibility.Notification.screenChanged, argument: alert)
            completion?()
        })
    }
    
    /// - description: Automatically presented and hide after some second
    func showAutomatically(delay: TimeInterval = 4, complitionOnLoad: VoidHandler? = nil, completionOnHide: @escaping VoidHandler) {
        self.show {
            complitionOnLoad?()
            self.addSwipeToHide()
            self.completionOnHideHandler = completionOnHide
            UIView.animate(withDuration: 1, delay: delay, options: [.transitionCrossDissolve, .allowUserInteraction]) {
                self.alpha = 0.1
            } completion: { [weak self] (succeed) in guard let self = self else { return }
                self.removeFromSuperview()
                self.completionOnHideHandler?()
            }
        }
    }
}

public typealias Advise = AdviseClass & AdviseProtocol & NibLoadable
