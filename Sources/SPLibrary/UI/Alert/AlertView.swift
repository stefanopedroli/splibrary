// Created by: Stefano Pedroli on 26/08/2019.
// Copyright © 2019 MC Engineering. All rights reserved.

import UIKit

public class AlertView: Alert {
    
    // OUtlets
    @IBOutlet public weak var mainView: UIView!
    
    @IBOutlet private weak var firstButton: Button!
    @IBOutlet private weak var secondButton: Button!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var stackViewHeight: NSLayoutConstraint!
    
    // override default actions
    private var firstButtonActionHandler: AlertHandler = nil
    private var secondButtonActionHandler: AlertHandler = nil
    
    public var customBackgroundAlertColor: UIColor?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        mainView.rounded(style: .softCorner, withShadow: false)
        firstButton?.rounded(style: .half, withShadow: false)
        secondButton?.rounded(style: .half, withShadow: false)
    }
    
    public func config(title: String?, message: String?, cancelAction: AlertAction?, confirmAction: AlertAction?) -> Self {
        self.titleLabel.text = title ?? ""
        self.messageLabel.text = message ?? ""
        self.firstButtonActionHandler = cancelAction?.action
        self.secondButtonActionHandler = confirmAction?.action
        
        switch (cancelAction, confirmAction) {
            
        case (.some, .some):
            self.firstButton.setTitle(cancelAction?.title, for: .normal)
            self.firstButton.titleLabel?.adjustsFontSizeToFitWidth = true
            self.secondButton.setTitle(confirmAction?.title, for: .normal)
            self.secondButton.titleLabel?.adjustsFontSizeToFitWidth = true
            
        case (.some, .none):
            self.firstButton.setTitle(cancelAction?.title, for: .normal)
            self.secondButton.safeRemoveFromSuperview()
            
        case (.none, .some):
            self.firstButton.safeRemoveFromSuperview()
            self.secondButton.setTitle(confirmAction?.title, for: .normal)
            
        case (.none, .none):
            self.firstButton.safeRemoveFromSuperview()
            self.secondButton.setTitle("Ok".localized, for: .normal)
        }
        
        return self
    }
    
    public func config(error: String) -> Self {
        return self.config(title: "Errore".localized, message: error, cancelAction: nil, confirmAction: AlertAction.ok())
    }
    
    // SCORCIATOIE ALERT PREFATTI
    public func cancel(title: String?, message: String?) -> Self {
        return self.config(title: title, message: message, cancelAction: AlertAction.cancel(), confirmAction: nil)
    }
    
    public func ok(title: String?, message: String?, action: ((_ alert: UIView) -> Swift.Void)? = nil) -> Self {
        return self.config(title: title, message: message, cancelAction: nil, confirmAction: AlertAction(title: .ok, action: action))
    }
    
    public func confirm(title: String?, message: String?, action: AlertAction) -> Self {
        return self.config(title: title, message: message, cancelAction: AlertAction.cancel(), confirmAction: action)
    }
    
    @IBAction private func firstAction(_ sender: UIButton) {
        self.removeFromSuperview()
        firstButtonActionHandler?(self)
        firstButtonActionHandler?(self)
    }
    
    @IBAction private func secondAction(_ sender: UIButton) {
        self.removeFromSuperview() // da decidere
        secondButtonActionHandler?(self)
    }
}

public final class AlertAction {
    
    public enum AlertTitleType {
        case cancel
        case ok
        case custom(title: String)
    }
    
    public var title: String
    public var action: AlertHandler
    
    public init(title: String, action: AlertHandler) {
        self.title = title
        self.action = action
    }
    
    public init(title: AlertTitleType, action: AlertHandler) {
        var titolo = String()
        switch title {
        case .cancel: titolo = "Annulla".localized
        case .ok: titolo = "Ok".localized
        case .custom(let stringa): titolo = stringa
        }
        self.title = titolo
        self.action = action
    }
    
    // scorciatoie
    public class func cancel(title: String? = nil) -> AlertAction {
        var titolo: AlertTitleType = .cancel
        if let title = title { titolo = .custom(title: title) }
        return AlertAction(title: titolo, action: nil)
    }
    
    public class func ok(title: String? = nil)  -> AlertAction {
        var titolo: AlertTitleType = .ok
        if let title = title { titolo = .custom(title: title) }
        return AlertAction(title: titolo, action: nil)
    }
}
