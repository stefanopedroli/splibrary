//  Created by Stefano Pedroli on 27/11/2019.

import UIKit

public protocol AlertProtocol {
    var mainView: UIView! { get set }
    var customBackgroundAlertColor: UIColor? { get set }
}

open class AlertClass: UIView {}

public extension AlertProtocol where Self: AlertClass {
    func show(completion: (()->())? = nil) {
        let alert = self
        let backgroundColor = customBackgroundAlertColor ?? UIColor.alertBackgroundColor
        alert.backgroundColor = backgroundColor.withAlphaComponent(0.9)
        if let rv = UIApplication.shared.keyWindow {
            self.frame = rv.bounds
            rv.addSubview(self)
            // Apply a fade-in animation
            let transformView : UIView = UIView()
            transformView.transform = CGAffineTransform(scaleX: 0, y: 0)
            UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseIn, animations: {
                transformView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                alert.mainView.alpha = 1
            }, completion: { [weak self] (_) in
                if self != nil {
                    UIAccessibility.post(notification: UIAccessibility.Notification.screenChanged, argument: alert.mainView)
                }
                completion?()
            })
        }
    }
    
    /// - description: Automatically presented and hide after some second
    func showAutomatically(delay: TimeInterval = 3, complitionOnLoad: VoidHandler? = nil, completionOnHide: VoidHandler? = nil) {
        self.show {
            complitionOnLoad?()
            UIView.animate(withDuration: 1, delay: delay, options: [.transitionCrossDissolve, .allowUserInteraction]) {
                self.alpha = 0.1
            } completion: { (succeed) in
                self.removeFromSuperview()
                completionOnHide?()
            }
        }
    }
}

public typealias Alert = AlertClass & AlertProtocol & NibLoadable
