//
//  DatePickerView.swift
//  Miority
//
//  Created by Stefano Pedroli on 26/11/2019.
//  Copyright © 2019 Tandù. All rights reserved.
//

import UIKit

//public final class DatePickerView: Alert {
//    
//    public var customBackgroundAlertColor: UIColor?
//    
//    @IBOutlet public weak var mainView: UIView!
//    @IBOutlet private weak var datePicker: UIDatePicker!
//    @IBOutlet private weak var confirmButton: Button!
//    @IBOutlet private weak var descriptionLbl: UILabel!
//    
//    public typealias DatePickerHandler = (_ date: Date) -> Swift.Void
//    private var handler: DatePickerHandler?
//    
//    public func config(message: String, selectedDate: Date = Date(), completion: @escaping DatePickerHandler, mode: UIDatePicker.Mode = .date) -> Self {
//        self.descriptionLbl.text = message
//        self.handler = completion
//        self.datePicker.datePickerMode = mode
//        self.datePicker.setDate(selectedDate, animated: true)
//        let tap: UITapGestureRecognizer = .init(target: self, action: #selector(closeView))
//        addGestureRecognizer(tap)
//        return self
//    }
//    
//    @objc private func closeView() {
//        self.removeFromSuperview()
//    }
//    
//    @IBAction private func confirmDidTapped(_ sender: Button) {
//        handler?(self.datePicker.date)
//        self.closeView()
//    }
//}
