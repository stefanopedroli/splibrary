//  Created by Stefano Pedroli on 27/11/2019.

import UIKit

open class AlertManager {
    
    /// - warning: you have to implement xib on your project, waiting Apple introduce resources
    public static var normal: AlertView {
        return AlertView.loadFromNib()
    }
    
    /// - warning: you have to implement xib on your project, waiting Apple introduce resources
//    public static var datePicker: DatePickerView {
//        return DatePickerView.loadFromNib()
//    }
    
    private init() {}
}

public typealias AlertHandler = ((_ view: UIView) -> Swift.Void)?

public extension UIViewController {
    func showAlert(title: String?, message: String?) {
        AlertManager.normal.config(title: title, message: message, cancelAction: nil, confirmAction: .ok()).show()
    }
    
    @available(*, deprecated, renamed: "alertErrorMessage", message: "use 'hanldeError' to hanlde error after network call but to show only error alert use 'alertErrorMessage' method")
    /// description, you can override this function for change UI style
    func showError(title: String? = nil, message: String) {
        if let applicationCoordinator = UIApplication.appCoordinator {
            applicationCoordinator.handleError(message: message, completion: {
                // nothing here
            })
        } else {
            AlertManager.normal.config(title: "Attenzione", message: message, cancelAction: nil, confirmAction: .ok()).show()
        }
//        AlertManager.pulseAlert.ok(type: .failure, title: title, message: message).show()
    }
    
    func alertErrorMessage(message: String) {
        if let applicationCoordinator = UIApplication.appCoordinator {
            applicationCoordinator.handleError(message: message, completion: {
                // nothing here
            })
        } else {
            AlertManager.normal.config(title: "Attenzione", message: message, cancelAction: nil, confirmAction: .ok()).show()
        }
    }
    
    func alertErrorMessage(message: String, completion: @escaping VoidHandler) {
        if let applicationCoordinator = UIApplication.appCoordinator {
            applicationCoordinator.handleError(message: message, completion: completion)
        } else {
            AlertManager.normal.config(title: "Attenzione", message: message, cancelAction: nil, confirmAction: .ok()).show()
        }
    }
}
