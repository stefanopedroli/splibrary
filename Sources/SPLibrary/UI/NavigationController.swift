//  Created by Stefano Pedroli on 16/11/2019.

import UIKit

open class NavigationController: UINavigationController {
    
    public var previouslyTheme: UINavigationController.Theme?
    var navigationTheme: NavigationTheme {
        SPLibrary.shared.navigationTheme.theme = theme
        return SPLibrary.shared.navigationTheme
    }
    
    public var theme: UINavigationController.Theme = .default {
        didSet { apply(theme) }
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        apply(theme)
    }
    
    internal var isLargeTitle: Bool = false
    internal var istrasparent: Bool = false
    
    func apply(_ theme: UINavigationController.Theme) {
        setNeedsStatusBarAppearanceUpdate()
        navigationBar.prefersLargeTitles = isLargeTitle
        navigationBar.isTranslucent = istrasparent
//        // tint color
        navigationBar.tintColor = navigationTheme.tintColor
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundImage = navigationTheme.backgroundImage
            appearance.shadowImage = navigationTheme.backgroundImage
            appearance.shadowColor = .mainThemeWhite
            appearance.backgroundColor = navigationTheme.barTintColor
            appearance.titleTextAttributes = navigationTheme.titleTextAttributes
            appearance.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.mainThemeBlack,
                                                   NSAttributedString.Key.font: UIFont.libraryFont(ofSize: 32, weight: .light)]
            navigationBar.standardAppearance = appearance
            navigationBar.compactAppearance = appearance
            navigationBar.scrollEdgeAppearance = appearance
        } else {
            navigationBar.titleTextAttributes = navigationTheme.titleTextAttributes
            navigationBar.shadowImage = navigationTheme.backgroundImage
            navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.mainThemeBlack,
                                                      NSAttributedString.Key.font: UIFont.libraryFont(ofSize: 32, weight: .light)]
        }
            
        // background color
        switch navigationTheme.theme {
        case .gradient(let gradient):
            navigationBar.setGradientBackground(gradientStyle: gradient, gradientOrientation: .leftRight)
        default:
            navigationBar.setBackgroundImage(navigationTheme.backgroundImage, for: .default)
            navigationBar.barTintColor =  navigationTheme.barTintColor
        }
        
        if self.istrasparent {
            navigationBar.transparentNavigationBar()
        }
    }
    
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return navigationTheme.preferredStatusBarStyle
    }
}

public extension UINavigationController {
    enum Theme {
        case `default`, lightContent, gradient(gradient: GradientStyle)
    }
}

public extension UINavigationController {
    
    func applyGradientTheme(gradient: GradientStyle = .defaultStyle) {
        guard let navigation = self as? NavigationController else { return }
        navigation.theme = .gradient(gradient: gradient)
    }
    
    func applyTheme(theme: Theme = .default, trasparent: Bool, largeTitle: Bool = false) {
        guard let navigation = self as? NavigationController else { return }
        navigation.isLargeTitle = largeTitle
        navigation.istrasparent = trasparent
        navigation.theme = theme
    }
    
    func setMultilineNavigationBar(topText: String, bottomText: String) {
        let topTxt = NSLocalizedString(topText, comment: String())
        let bottomTxt = NSLocalizedString(bottomText, comment: String())
        let titleParameters = [NSAttributedString.Key.foregroundColor : UIColor.mainThemeBlack,
                               NSAttributedString.Key.font : UIFont.libraryFont(ofSize: 30, weight: .regular)]
        let subtitleParameters = [NSAttributedString.Key.foregroundColor : UIColor.mainThemeBlack,
                                  NSAttributedString.Key.font : UIFont.libraryFont(ofSize: 30, weight: .bold)]
        let title:NSMutableAttributedString = NSMutableAttributedString(string: topTxt, attributes: titleParameters)
        let subtitle:NSAttributedString = NSAttributedString(string: bottomTxt, attributes: subtitleParameters)
        
        title.append(NSAttributedString(string: "\n"))
        title.append(subtitle)
        let size = title.size()
        let width = size.width
        let height = navigationBar.frame.size.height
        
        let titleLabel = UILabel(frame: CGRect.init(x: 20, y: 10, width: width, height: height))
        titleLabel.attributedText = title
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .left
        self.navigationBar.addSubview(titleLabel)
    }
    
    func setmultilineTitle(topText: String, bottomText: String) {
        for navItem in (self.navigationBar.subviews) {
            for itemSubView in navItem.subviews {
                if let largeLabel = itemSubView as? UILabel {
                    let fontSize: CGFloat = 23
                    let topTxt = NSLocalizedString(topText, comment: String())
                    let bottomTxt = NSLocalizedString(bottomText, comment: String())
                    let titleParameters = [NSAttributedString.Key.foregroundColor : UIColor.mainThemeBlack,
                                           NSAttributedString.Key.font : UIFont.libraryFont(ofSize: fontSize, weight: .regular)]
                    let subtitleParameters = [NSAttributedString.Key.foregroundColor : UIColor.mainThemeBlack,
                                              NSAttributedString.Key.font : UIFont.libraryFont(ofSize: fontSize, weight: .bold)]
                    let title:NSMutableAttributedString = NSMutableAttributedString(string: topTxt, attributes: titleParameters)
                    let subtitle:NSAttributedString = NSAttributedString(string: bottomTxt, attributes: subtitleParameters)
                    title.append(NSAttributedString(string: "\n"))
                    title.append(subtitle)
                    let size = title.size()
                    let width = size.width
                    let height = navigationBar.frame.size.height
                    
                    let titleLabel = UILabel(frame: CGRect.init(x: 0, y: -fontSize , width: width, height: height))
                    titleLabel.attributedText = title
                    titleLabel.numberOfLines = 0
                    titleLabel.textAlignment = .left
                    titleLabel.tag = 293829223
                    // remove all others element before adding
                    largeLabel.text = String()
//                    largeLabel.attributedText = title
                    largeLabel.replaceSubview(newView: titleLabel, at: 293829223)
                }
            }
        }
    }
    
    func setCustomInlineitle(normalText: String, boldText: String) {
        for navItem in (self.navigationBar.subviews) {
            for itemSubView in navItem.subviews {
                if let largeLabel = itemSubView as? UILabel {
                    let fontSize: CGFloat = 25
                    let topTxt = NSLocalizedString(normalText, comment: String())
                    let bottomTxt = NSLocalizedString(boldText, comment: String())
                    let titleParameters = [NSAttributedString.Key.foregroundColor : UIColor.mainThemeBlack,
                                           NSAttributedString.Key.font : UIFont.libraryFont(ofSize: fontSize, weight: .regular)]
                    let subtitleParameters = [NSAttributedString.Key.foregroundColor : UIColor.mainThemeBlack,
                                              NSAttributedString.Key.font : UIFont.libraryFont(ofSize: fontSize, weight: .bold)]
                    let title:NSMutableAttributedString = NSMutableAttributedString(string: topTxt, attributes: titleParameters)
                    let subtitle:NSAttributedString = NSAttributedString(string: bottomTxt, attributes: subtitleParameters)
                    title.append(NSAttributedString(string: " "))
                    title.append(subtitle)
                    largeLabel.text = String()
                    largeLabel.attributedText = title
                }
            }
        }
    }
    
    func setmultilineTitleIfNeeded(topText: String, bottomText: String) {
        for navItem in (self.navigationBar.subviews) {
            for itemSubView in navItem.subviews {
                if let largeLabel = itemSubView as? UILabel {
                    if largeLabel.findSubview(by: 293829223) != nil { return } else {
                        setmultilineTitle(topText: topText, bottomText: bottomText)
                    }
                }
            }
        }
    }
}

public enum EmbeddedNavigationType {
    case embeddedInNavigationController
    case embeddedInLightNavigationController
    case embeddedInNavigationControllerTrasparent
    case embeddedInLightNavigationControllerTrasparent
    case embeddedInLargeNavigationController
}

public extension UIViewController {
    
    var embeddedInNavigationController: UINavigationController {
        return NavigationController(rootViewController: self)
    }
    
    var embeddedInLightNavigationController: UINavigationController {
        let navigationController = NavigationController(rootViewController: self)
        navigationController.theme = .lightContent
        return navigationController
    }
    
    var embeddedInLargeNavigationController: UINavigationController {
        let navigationController = NavigationController(rootViewController: self)
        navigationController.navigationBar.prefersLargeTitles = true
        navigationController.theme = .default
        return navigationController
    }
    
    func embeddInNavigationIfNeeded(type: EmbeddedNavigationType?) -> UIViewController {
        switch type {
        case .some(.embeddedInNavigationController): return self.embeddedInNavigationController
        case .some(.embeddedInLightNavigationController): return self.embeddedInLightNavigationController
        case .some(.embeddedInLargeNavigationController): return self.embeddedInLargeNavigationController
        case .some(.embeddedInNavigationControllerTrasparent):
            return self.embeddedInNavigationController.trasparent
        case .some(.embeddedInLightNavigationControllerTrasparent):
            return self.embeddedInLightNavigationController.trasparent
        case .none: return self
        }
    }
    
    /// - description: Need calling after set NavigationController
    func displayLargeTitleOnLoad() {
        DispatchQueue.main.async {
            self.navigationController?.navigationBar.prefersLargeTitles = true
        }
    }
}

public extension UINavigationController {
    var trasparent: UINavigationController {
        self.navigationBar.transparentNavigationBar()
        return self
     }
}
