//  Created by Stefano Pedroli on 25/06/2020.

import UIKit

open class GradientButton: UIButton {
    
    public enum Style: Int {
        case action, cancel, alternative
    }
    
    public var style: Style = .action {
        didSet { styleChanged(style) }
    }
    
    @IBInspectable
    public var firstColor: UIColor = UIColor.gradientColors[0]
    @IBInspectable
    public var secondColor: UIColor = UIColor.gradientColors[1]
    public var gradientOrientation: GradientOrientation = .leftRight
    @IBInspectable
    public var background: UIColor = UIColor.white
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    override open func layoutSubviews() {
        styleChanged(style)
    }
    
    private func setUp() {
        layer.borderWidth = 1
        rounded(style: .half, withShadow: false)
        layer.masksToBounds = true
//        titleLabel?.textAlignment = .center
        titleLabel?.font = UIFont.libraryFont(ofSize: .defaulfFontSize, weight: .bold)
    }
    
    private func styleChanged(_ style: Style) {
        switch style.backgroundColor() {
        case .gradient:
            self.applyGradient(withColours: [firstColor, secondColor], gradientOrientation: self.gradientOrientation)
        case .color(let value):
            let backgroundImage = UIImage.color(value, size: .one)
            setBackgroundImage(backgroundImage, for: .normal)
        }
        
        switch style.borderColor() {
        case .gradient:
            self.applyBorderGradient(colors: [firstColor, secondColor])
        case .color(let value):
            self.layer.borderColor = value.cgColor
        }
        setTitleColor(style.titleColor(), for: .normal)
        setTitleColor(style.titleColor(), for: .highlighted)
    }
    
    override public func setTitle(_ title: String?, for state: UIControl.State) {
        super.setTitle(title?.uppercased(), for: state)
    }
    
    override public var intrinsicContentSize: CGSize {
        return CGSize(width: 245, height: 55)
    }
    
    override public func sizeToFit() {
        frame.size = intrinsicContentSize
    }
    
    override public func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: size.width, height: intrinsicContentSize.height)
    }
    
    // MARK: - Interface builder
    @IBInspectable
    public var styleValue: Int {
        get { return style.rawValue }
        set { if let style = Style(rawValue: newValue) { self.style = style } }
    }
}

private extension GradientButton.Style {
    
    func backgroundColor() -> ColorType {
        switch self {
        case .action:                  return .gradient
        case .cancel:                  return .color(value: UIColor.mainThemeWhite)
        case .alternative:             return .color(value: UIColor.mainThemeWhite)
        }
    }
    
    func borderColor() -> ColorType {
        switch self {
        case .action:                  return .color(value: .clear)
        case .cancel:                  return .gradient
        case .alternative:             return .color(value: .clear)
        }
    }
    
    func titleColor() -> UIColor {
        switch self {
        case .action:                  return UIColor.mainThemeWhite
        case .cancel:                  return UIColor.mainThemeWhite
        case .alternative:             return UIColor.mainThemeWhite
        }
    }
}
