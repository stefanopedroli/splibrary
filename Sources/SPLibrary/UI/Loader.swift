// Created by Stefano Pedroli on 15/01/2020.

import UIKit
import Lottie

public protocol LoadingViewController {
    func showLoader()
    func hideLoader()
}

public extension LoadingViewController where Self: UIViewController {
    
    func showLoader() {
        if let refreshable = self as? Refreshable, refreshable.isLoading { return }
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.mainThemeBlack.withAlphaComponent(0.4)
            //UIColor(white: 0, alpha: 0.4)
        backgroundView.frame.size = referenceView.bounds.size
        backgroundView.tag = backgroundViewTag
        referenceView.addSubview(backgroundView)
        referenceView.bringSubviewToFront(backgroundView)
        if let fileName = SPLibrary.configurations.loaderJSONFileName {
            let animatedView: AnimationView = .init(name: fileName)
            animatedView.loopMode = .loop
            animatedView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
            animatedView.center.x = backgroundView.bounds.midX
            animatedView.center.y = backgroundView.bounds.midY
            animatedView.play()
            backgroundView.addSubview(animatedView)
            backgroundView.alpha = 0
            UIView.animate(withDuration: 0.1) {
                backgroundView.alpha = 1
                UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: animatedView)
            }
        } else {
            let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
            activityIndicator.color = UIColor.white
            activityIndicator.center.x = backgroundView.bounds.midX
            activityIndicator.center.y = backgroundView.bounds.midY
            activityIndicator.hidesWhenStopped = true
            activityIndicator.startAnimating()
            backgroundView.addSubview(activityIndicator)
            let label = UILabel()
            label.text = "LOADING".localizedInternal + "..."
            label.textColor = .white
            label.font = .libraryFont(ofSize: .defaulfFontSize, weight: .bold)
            label.sizeToFit()
            label.center.x = activityIndicator.center.x
            label.frame.origin.y = activityIndicator.frame.maxY + 16
            backgroundView.addSubview(label)
            
            backgroundView.alpha = 0
            UIView.animate(withDuration: 0.1) {
                backgroundView.alpha = 1
                UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: label)
            }
        }
    }
    
    func hideLoader() {
        guard let backgroundView = backgroundView else {
            return //assertionFailure("You tried to hide a loading indicator on a view that is not currently displaying one.")
        }
        backgroundView.alpha = 1
        UIView.animate(withDuration: 0.1) {
            backgroundView.alpha = 0
            backgroundView.removeFromSuperview()
            UIAccessibility.post(notification: UIAccessibility.Notification.layoutChanged, argument: self.referenceView)
        }
    }
    
    private var referenceView: UIView {
        return tabBarController?.view ?? navigationController?.view ?? view
    }
    
    private var backgroundView: UIView? {
        return referenceView.subviews.first(where: { view in view.tag == backgroundViewTag })
    }
    
    private var backgroundViewTag: Int { return 1234 }
}

extension UIViewController: LoadingViewController {}
