//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 13/12/2019.
//

import UIKit

public protocol CheckBoxDelegate: class {
    func checkbox(_ checkBox: CheckBox, value isChecked: Bool)
}

@IBDesignable
public class CheckBox: UIButton {
    
    // MARK: Inspectable
    @IBInspectable
    public var color: UIColor = UIColor.mainThemeBlack {
        didSet { self.loadGraphycs() }
    }
    
    @IBInspectable
    public var isChecked: Bool = false {
        didSet { self.loadGraphycs() }
    }
    
    @IBInspectable public var selectedImage: UIImage = SPLibrary.configurations.check_box_image {
        didSet { self.loadGraphycs() }
    }
    
    @IBInspectable
    public var isCheckBox: Bool = false {
        didSet {
            self.type = isChecked ? .checkbox : .radio
            self.loadGraphycs()
        }
    }
    
    private var pallino: UIImage {
        // travaj style
        let _pallino: UIView = .init(frame: CGRect(x: 0, y: 0, width: self.bounds.width/2, height: self.bounds.height/2))
//        let _pallino: UIView = .init(frame: CGRect(x: 0, y: 0, width: self.bounds.width - 5, height: self.bounds.height - 5))
        _pallino.backgroundColor = .mainThemeWhite
        _pallino.rounded(style: .half)
        let containerView: UIView = UIView(frame: self.bounds)
        _pallino.center = containerView.center
        containerView.addSubview(_pallino)
        return UIImage.init(view: containerView)!
    }
    
    // MARK: init / functions
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    private func setup() {
        self.addTarget(self, action: #selector(touchUpInside), for: .touchUpInside)
    }
    
    private func loadGraphycs() {
        self.layer.borderWidth = 0
        self.layer.borderColor = color.cgColor
        self.layer.cornerRadius = roundedBorder.value()
        if self.type == .checkbox {
            self.layer.cornerRadius = roundedBorder.value()
            self.imageView?.contentMode = .scaleAspectFill
            self.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
        } else {
            self.rounded(style: .half)
            self.imageView?.contentMode = .scaleAspectFit
            self.setImage(image.withRenderingMode(.alwaysOriginal), for: .normal)
        }
        
        // travaj style
        if self.type == .radio && self.isChecked {
            self.backgroundColor = color
        } else {
            self.backgroundColor = UIColor.grayPalette(heavy: .light)
        }
        
        self.clipsToBounds = true
        self.tintColor = color
    }
    
    @objc private func touchUpInside(_ sender: UIButton) {
        self.isChecked = !self.isChecked
        self.sendActions(for: .valueChanged)
        self.delegate?.checkbox(self, value: isChecked)
    }
    
    // MARK: Variables Private
    private var image: UIImage {
        switch self.type {
        case .checkbox:
            return self.isChecked ? selectedImage.template() : UIImage()
        case .radio:
            return self.isChecked ? pallino : UIImage()
        }
    }
    
    // MARK: Variables Public
    public weak var delegate: CheckBoxDelegate?
    
    public var type: CheckBoxType = .checkbox {
        didSet { self.loadGraphycs() }
    }
    
//    public var highlightedImage: UIImage = UIImage() {
//        didSet { self.loadGraphycs() }
//    }
    
    public var roundedBorder: CornerRadiusStyle = .custom(value: 0) {
        didSet { self.loadGraphycs() }
    }
}

public extension CheckBox {
    enum CheckBoxType {
        case checkbox, radio
    }
}
