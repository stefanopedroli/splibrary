//  Created by Stefano Pedroli on 27/04/2020.

//import UIKit
//
//public extension UITableView {
//    
//    var advanceOptions: [TableAdvanceOptions] {
//        get { return self.advanceOptions }
//        set { }
//    }
//    
//    func inizializeAdvanceOptions() {
//        let originalSelector = #selector(delegate?.tableView(_:didEndDisplaying:forRowAt:))
//        let swizzleSelector = #selector(custom_didEndDisplaying(_:didEndDisplaying:forRowAt:))
//        guard
//            let originalMethod = class_getInstanceMethod(object_getClass(self), originalSelector),
//            let swizzledMethod = class_getInstanceMethod(object_getClass(self), swizzleSelector)
//        else { return assertionFailure("non trova i metodi da swizzlingare") }
//        method_exchangeImplementations(originalMethod, swizzledMethod)
//    }
//    
//    /// - description: Swizzling method of didEndDisplaying cell forRowAt
//   @objc func custom_didEndDisplaying(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        // active options here
//        for option in self.advanceOptions {
//            switch option {
//            case .alternativeColors(let first, let second):
//                self.addAlternativeColors(first: first, second: second, for: cell, at: indexPath)
//            }
//        }
//        self.tableViewAdvanceOptions(tableView, didEndDisplaying: cell, forRowAt: indexPath)
//    }
//    
//    /// - description: TableViewAdvanceOptions use method didEndDisplaying for load settings, so if you whant to use you have implement this not default system method
//    func tableViewAdvanceOptions(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {}
//}
//
//public enum TableAdvanceOptions {
//    case alternativeColors(first: UIColor, second: UIColor)
//}
//
//public extension UITableView {
//    
//    /// - description: This method set altrenative colors for even and odd cell
//    func addAlternativeColors(first: UIColor, second: UIColor, for cell: UITableViewCell, at indexPath: IndexPath) {
//        if indexPath.row.isOdd {
//            cell.backgroundColor = second
//        } else {
//            cell.backgroundColor = first
//        }
//    }
//}
