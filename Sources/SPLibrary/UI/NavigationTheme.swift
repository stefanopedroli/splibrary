//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 16/11/2019.
//

import UIKit

open class NavigationTheme {
    
    var theme: UINavigationController.Theme = .gradient(gradient: .defaultStyle)
    
    var backgroundImage: UIImage? {
        switch theme {
        case .default: return UIImage()
        case .lightContent: return UIImage()
        case .gradient: return UIImage()
        }
    }
    
    // color of item inside navigation
    var tintColor: UIColor {
        switch theme {
        case .default: return .mainThemeBlack
        case .lightContent: return .mainThemeWhite
        case .gradient(let gradient): return gradient.tintColor
        }
    }
    
    // background of navigation bar
    var barTintColor: UIColor {
        switch theme {
        case .default: return .mainThemeWhite
        case .lightContent: return .mainThemeWhite
        case .gradient(let gradient): return gradient.tintColor
        }
    }
    
    var preferredStatusBarStyle: UIStatusBarStyle {
        switch theme {
        case .default: return .default
        case .lightContent: return .lightContent
        case .gradient: return .default
        }
    }
    
    var titleTextAttributes: [NSAttributedString.Key: Any] {
        let font = UIFont.libraryFont(ofSize: .defaulfFontSize + 2, weight: .medium)
        switch theme {
        case .default: return [.foregroundColor: self.tintColor, .font: font]
        case .lightContent: return [.foregroundColor: self.tintColor, .font: font]
        case .gradient(let gradient): return [.foregroundColor: gradient.tintColor, .font: font]
        }
    }
}
