//  Created by Stefano Pedroli on 22/11/2019.

import UIKit

public protocol TextViewEndEditingDelegate: class {
    func didFinishEditing(_ textView: TextView, _ text: String)
}

open class TextView: UITextView {

    public override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        setUp()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }
    
    // MARK: Delegate
    public weak var textDelegate: TextViewEndEditingDelegate?
    
    deinit { NotificationCenter.default.removeObserver(self) }
    
    public var placeholder: String? {
        didSet { updatePlaceHolder() }
    }
    
    public var placeholderColor: UIColor = .grayPalette(heavy: .medium) {
        didSet { self.placeholderLabel.textColor = placeholderColor }
    }
    
    lazy private var placeholderLabel: UILabel = UILabel()
    
    override public var text: String! {
        didSet {
            super.text = text
            if !text.isEmpty { removePlaceHolderIfNeeded() }
        }
    }
    
    open var fontSize: CGFloat = .defaulfFontSize {
        didSet { self.font = .libraryFont(ofSize: fontSize, weight: .light) }
    }
    
    open var borderColor: UIColor = .mainTheme {
        didSet { setUp() }
    }
    open var contentBackgroundColor: UIColor = .clear {
        didSet { setUp() }
    }
    
    // Material bottom line style
    private var borderLine: UIView?
    @IBInspectable public var materialStyle = true {
        didSet { setBottomBorderIfNeeded() }
    }
    
    private func setUp() {
        delegate = self
        textColor = UIColor.mainThemeBlack
        backgroundColor = self.contentBackgroundColor
        font = .libraryFont(ofSize: fontSize, weight: .light)
        layer.borderWidth = 1
        layer.borderColor = self.borderColor.cgColor
        self.rounded()
        layer.masksToBounds = true
        isScrollEnabled = false
        contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        textContainerInset =  UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        super.text = text
        
        // configure placeholder
        textInputView.addSubview(placeholderLabel)
//        placeholderLabel.bounds = self.textInputView.bounds
//        placeholderLabel.frame = self.frame
//        placeholderLabel.center = center
//        placeholderLabel.center = self.textInputView.center
//        placeholderLabel.isEditable = false
//        placeholderLabel.isSelectable = false
        placeholderLabel.setMargins(top: 8, left: 8, right: 8, bottom: 8, toItem: self)
        placeholderLabel.isUserInteractionEnabled = false
        placeholderLabel.textColor = placeholderColor
        placeholderLabel.font = .libraryFont(ofSize: fontSize, weight: .light)
        placeholderLabel.numberOfLines = 0
        layoutIfNeeded()
        
        setBottomBorderIfNeeded()
    }
    
    private func updatePlaceHolder() {
        if super.text.isEmpty {
            placeholderLabel.text = placeholder
            placeholderLabel.isHidden = false
            updateConstraints()
        }
    }
    
    private var isPlaceholderActive: Bool {
        return !placeholderLabel.text.isEmpty
    }
    
    private func removePlaceHolderIfNeeded() {
        if isPlaceholderActive {
            placeholderLabel.isHidden = true
        }
    }
}

extension TextView: UITextViewDelegate {
    public func textViewDidBeginEditing(_ textView: UITextView) {
        removePlaceHolderIfNeeded()
        UIAccessibility.post(notification: UIAccessibility.Notification.screenChanged, argument: self)
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        updatePlaceHolder()
        self.textDelegate?.didFinishEditing(self, textView.text)
        UIAccessibility.post(notification: UIAccessibility.Notification.screenChanged, argument: textView)
    }
}

private extension TextView {
    func setBottomBorderIfNeeded() {
        if self.materialStyle {
            let borderLineView = UIView(frame: CGRect(x: 0, y: self.frame.height - 1, width: self.frame.width, height: 1))
            borderLineView.backgroundColor = self.borderColor
            self.borderLine = borderLineView
            self.addSubview(borderLineView)
        } else {
            self.borderLine?.removeFromSuperview()
        }
    }
}
