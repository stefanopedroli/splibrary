//  Created by Stefano Pedroli on 27/04/2020.

import UIKit

public final class UIDeselectableSegmentedControl: UISegmentedControl {
    public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let previousSelectedSegmentIndex = self.selectedSegmentIndex
        super.touchesEnded(touches, with: event)
        if previousSelectedSegmentIndex == self.selectedSegmentIndex {
            self.selectedSegmentIndex = UISegmentedControl.noSegment
            let touch = touches.first!
            let touchLocation = touch.location(in: self)
            if bounds.contains(touchLocation) {
                self.sendActions(for: .valueChanged)
            }
        }
    }
}
