//  Created by Stefano Pedroli on 17/04/2020.

import UIKit
import Photos
import BSImagePicker

public protocol MultipleImagePickerControllerDelegate: class {
    func didFinishPickImages(_ imagePicker: MultipleImagePickerController, images: [SPAsset])
//    func didCancelPicking(_ imagePicker: MultipleImagePickerController, images: [SPAsset])
//    func didSelectImageAt(_ imagePicker: MultipleImagePickerController, image: SPAsset)
//    func didDeselectImageAt(_ imagePicker: MultipleImagePickerController, image: SPAsset)
}

//public extension MultipleImagePickerControllerDelegate {
//    func didCancelPicking(_ imagePicker: MultipleImagePickerController, images: [SPAsset]) {}
//    func didSelectImageAt(_ imagePicker: MultipleImagePickerController, image: SPAsset) {}
//    func didDeselectImageAt(_ imagePicker: MultipleImagePickerController, image: SPAsset) {}
//}

public final class MultipleImagePickerController {
    
    // MARK: Pickers
    public var multipleImagePicker = ImagePickerController()
    public weak var delegate: MultipleImagePickerControllerDelegate?
    
    // MARK: Public properties
    public var maxNumberOfPictures: Int
    
    public init(maxNumberOfPictures: Int) {
        self.maxNumberOfPictures = maxNumberOfPictures
        
        // setup picker
        multipleImagePicker.settings.selection.max = maxNumberOfPictures
        multipleImagePicker.settings.theme.selectionStyle = .numbered
        multipleImagePicker.settings.fetch.assets.supportedMediaTypes = [.image]
        multipleImagePicker.settings.selection.unselectOnReachingMax = false
    }
    
    public func present(from presentingViewController: UIViewController) {

        let start = Date()
        presentingViewController.presentImagePicker(multipleImagePicker, select: { (asset) in
            print("Selected: \(asset)")
//            asset.convertToSPAsset { (spAsset) in
//                self.delegate?.didSelectImageAt(self, image: spAsset)
//            }
        }, deselect: { (asset) in
            print("Deselected: \(asset)")
//            asset.convertToSPAsset { (spAsset) in
//                self.delegate?.didDeselectImageAt(self, image: spAsset)
//            }
        }, cancel: { (assets) in
            print("Canceled with selections: \(assets)")
//            assets.convertToSPAssets { (images) in
//                self.delegate?.didCancelPicking(self, images: images)
//            }
        }, finish: { (assets) in
            print("Finished with selections: \(assets)")
            assets.convertToSPAssets { (images) in
                DispatchQueue.main.async {
                    self.delegate?.didFinishPickImages(self, images: images)
                }
            }
        }, completion: {
            let finish = Date()
            print(finish.timeIntervalSince(start))
        })
    }
}

public extension PHAsset {
    
    static func convertAssetsToImages(assets: [PHAsset]) -> [UIImage] {
        var images: [UIImage] = []
        for asset in assets {
            if let newImage = asset.image {
                images.append(newImage)
            }
        }
        return images
    }
    
    var image: UIImage? {
        return self.toUIImage()
    }
    
    func toUIImage(compression: CGFloat = 0.7, size: CGSize = CGSize(width: 1000, height: 1000)) -> UIImage? {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: self, targetSize: size, contentMode: .aspectFill, options: option, resultHandler: {(result, info)-> Void in
            guard let image = result else { return }
            thumbnail = image
        })
        guard let data = thumbnail.jpegData(compressionQuality: compression) else { return nil }
        guard let newImage = UIImage(data: data) else { return nil }
        return newImage
    }
    
    func asynchToUIImage(compression: CGFloat = 0.7, size: CGSize = CGSize(width: 1000, height: 1000),
                         completion: @escaping(_ image: UIImage) -> Void,
                         error: VoidHandler?) {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        option.isSynchronous = true
        manager.requestImage(for: self, targetSize: size, contentMode: .aspectFill, options: option, resultHandler: {(result, info)-> Void in
            guard let image = result else { error?(); return }
            guard let data = image.jpegData(compressionQuality: compression) else { error?(); return }
            guard let newImage = UIImage(data: data) else { error?(); return }
            completion(newImage)
        })
    }
}

extension Array where Element == PHAsset {
    func convertToSPAssets(completion: @escaping(_ assets: [SPAsset]) -> Void) {
        var images: [SPAsset] = []
        let semaphore: DispatchGroup = .init()
        for asset in self {
            semaphore.enter()
            asset.asynchToUIImage(completion: { (image) in
                asset.requestContentEditingInput(with: PHContentEditingInputRequestOptions()) { (eidtingInput, info) in
                    let imgURL = eidtingInput?.fullSizeImageURL
                    let assetResource = PHAssetResource.assetResources(for: asset).first
                    guard let fileExtension = imgURL?.absoluteString.fileExtension else {
                        return semaphore.leave()
                    }
                    var name = assetResource?.originalFilename ?? generateNameForImage(fileExtension: fileExtension)
                    name = name.convertToValidFileName()
                    images.append(SPAsset.init(name: name, url: imgURL, image: image, phAsset: asset))
                    semaphore.leave()
                }
            }) {
                // error
                semaphore.leave()
            }
        }
        semaphore.notify(queue: .main) {
            completion(images)
        }
    }
}

extension PHAsset {
    func convertToSPAsset(completion: @escaping(_ asset: SPAsset) -> Void) {
        self.asynchToUIImage(completion: { (newImage) in
            self.requestContentEditingInput(with: PHContentEditingInputRequestOptions()) { (eidtingInput, info) in
                let imgURL = eidtingInput?.fullSizeImageURL
                guard let fileExtension = imgURL?.absoluteString.fileExtension else { return }
                let name = PHAssetResource.assetResources(for: self).first?.originalFilename ?? generateNameForImage(fileExtension: fileExtension)
                completion(SPAsset.init(name: name, url: imgURL, image: newImage, phAsset: self))
            }
        }, error: nil)
    }
}

public extension Array where Element == SPAsset {
    var images: [UIImage] {
        return self.map { (asset) -> UIImage in
            return asset.image
        }
    }
}

public extension String {
    func convertToValidFileName(replaced character: String = "_") -> String {
        let invalidCharacters = NSMutableCharacterSet(charactersIn: ":/")
        invalidCharacters.formUnion(with: NSCharacterSet.newlines)
        invalidCharacters.formUnion(with: NSCharacterSet.illegalCharacters)
        invalidCharacters.formUnion(with: NSCharacterSet.controlCharacters)
        return self.components(separatedBy: invalidCharacters as CharacterSet).joined(separator: character)
    }
}

public func generateNameForImage(fileExtension: String) -> String {
    return "image_\(Date.today.getFormattedDate(format: .timeStamp)).\(fileExtension)".convertToValidFileName()
}

public struct SPAsset {
    public let name: String
    public let url: URL?
    public let image: UIImage
    public let phAsset: PHAsset
    public var mimetype: String? { return self.url?.absoluteString.fileExtensionType.mimetype }
}
