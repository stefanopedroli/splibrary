//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 19/11/2019.
//

import UIKit

open class PickerView: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    public struct PickerComponent {
        var values: [String]
        var header: String?
        
        public init(values: [String], header: String?) {
            self.values = values
            self.header = header
        }
    }
    
    public override var backgroundColor: UIColor? {
        didSet { super.backgroundColor = backgroundColor }
    }
    
    public var textColor: UIColor? {
        didSet {
            self.reloadAllComponents()
            self.reloadAllHeaders()
        }
    }
    
    private var headersLabel: [UILabel] = []
    
    /// - Description: The toolbar is added if you implement handler
    public var toolbar: Bool = false {
        didSet {
            if toolbar {
                self.associatedTextField?.inputAccessoryView = customToolbar
            } else {  self.associatedTextField?.inputAccessoryView = nil }
        }
    }
    
    public var emptyRow: Bool = false {
        didSet {
            if emptyRow {
                self.dataSourceArray.insertEmptyRows()
            } else {
                self.dataSourceArray = originallyDataSourceArray
            }
            self.reloadAllComponents()
        }
    }
    
    private var customToolbar: UIToolbar {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.mainTheme
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem.init(title: "DONE".localizedInternal, style: UIBarButtonItem.Style.done, target: self, action: #selector(done))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "CANCEL".localizedInternal, style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancel))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        return toolBar
    }
    
    private var components: Int { return dataSourceArray.count }
    private var dataSourceArray: [PickerComponent] = []
    private var originallyDataSourceArray: [PickerComponent] = []
    private var associatedTextField: UITextField?
    private var separator: String = "+"
    private var initiallyValue: String = ""
    
    /// - description: init for 1 component
    public convenience init(input: PickerComponent, textField: UITextField) {
        self.init(frame: CGRect.zero)
        self.setup(inputs: [input], textField: textField, separator: separator)
    }
    
    /// - description: init for multiple components
    public convenience init(inputs: [PickerComponent], textField: UITextField, separator: String = " ") {
        self.init(frame: CGRect.zero)
        self.setup(inputs: inputs, textField: textField, separator: separator)
    }
    
    private func setup(inputs: [PickerComponent], textField: UITextField, separator: String = " ") {
        textField.inputView = self
        self.associatedTextField = textField
        self.originallyDataSourceArray = inputs
        self.dataSourceArray = inputs
        self.separator = separator
        self.delegate = self
        textField.delegate = self
        automaticallySelectRows(animated: true)
        reloadAllHeaders()
        
        // implement indicator down
        textField.rightViewMode = .always
        textField.rightView = indicatorDown
        textField.addTarget(self, action: #selector(pickerDidEndEditing), for: .editingDidEnd)
    }
    
    let indicatorDown: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 10, width: 15, height: 15))
        label.text = "⌵"
        label.textColor = UIColor.grayPalette(heavy: .medium)
        return label
    }()
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    private func reloadAllHeaders() {
        self.headersLabel.removeAll()
        let labelWidth = self.bounds.width / CGFloat(components)
        for index in 0..<components {
            if let header = dataSourceArray[index].header {
                let label: UILabel = UILabel.init(frame: CGRect(x: (self.bounds.origin.x + labelWidth * CGFloat(index)) + 20,
                                                                y: 30,
                                                                width: labelWidth,
                                                                height: 20))
                label.text = header
                label.textColor = self.textColor
                label.textAlignment = .center
                self.addSubview(label)
                self.headersLabel.append(label)
            }
        }
    }
    
    private func automaticallySelectRows(texts: [String]? = nil, animated: Bool) {
        // Prepare var and conditions
        let textfield: String = associatedTextField?.text ?? ""
        if textfield.isEmpty {
            // Automatically select first rows of all components
            for index in dataSourceArray.indices {
                self.selectRow(0, inComponent: index, animated: true)
            }
            return
        }
        
        // There's some value in texfild, try to find and select
        var textfieldInputs: [String] = []
        if self.dataSourceArray.count > 1 {
            textfieldInputs = textfield.split(separator: Character(separator)).map({ (substring) -> String in
                return String(substring)
            })
        } else {
            textfieldInputs = [textfield]
        }

        let values = texts ?? textfieldInputs
        if values.count != dataSourceArray.count { return }
        
        // Search value in all components
        for (indexComponent, component) in dataSourceArray.enumerated() {
            for (index, input) in component.values.enumerated() {
                if values[indexComponent] == input {
                    self.selectRow(index, inComponent: indexComponent, animated: animated)
                }
            }
        }
    }
     
    /// - Description: You can select all values you want
    /// - Warning: This function will search your values in all components and select each finded value!
    public func selectValues(texts: [String], animated: Bool = false) {
        automaticallySelectRows(texts: texts, animated: animated)
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return components
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSourceArray[component].values.count
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataSourceArray[component].values[row]
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.displayTextfieldInformations(pickerView)
    }
    
    private func displayTextfieldInformations(_ pickerView: UIPickerView) {
        var texts: [String] = []
        for (index, element) in dataSourceArray.enumerated() {
            let text = element.values[pickerView.selectedRow(inComponent: index)]
            if !text.isEmpty { texts.append(text) }
        }
        associatedTextField?.text = texts.joined(separator: separator)
    }
    
    public func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        return NSAttributedString(string: dataSourceArray[component].values[row], attributes: [NSAttributedString.Key.foregroundColor : self.textColor ?? UIColor.mainThemeBlack])
    }
    
    @objc private func pickerDidEndEditing(_ textField: TextField) {
        self.indicatorDown.rotate(grade: .grade_180)
    }
    
    deinit { }
}

extension PickerView: UITextFieldDelegate {
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        initiallyValue = associatedTextField?.text ?? ""
        self.displayTextfieldInformations(self)
        self.indicatorDown.rotate(grade: .grade_180)
    }
}

// Toolbar methods
extension PickerView {
    
    @objc func done() {
        self.associatedTextField?.endEditing(true)
    }
    
    @objc func cancel() {
        self.associatedTextField?.text = initiallyValue
        selectValues(texts: [initiallyValue])
        self.associatedTextField?.endEditing(true)
    }
}

extension Array where Element == PickerView.PickerComponent {
    mutating func insertEmptyRows() {
        for (index, _) in self.enumerated() {
            self[index].values.insert("", at: 0)
        }
    }
}
