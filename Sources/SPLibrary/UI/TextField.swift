//  Created by Stefano Pedroli on 22/11/2019.

import UIKit

public protocol TextFieldSelectionDelegate: class {
    func textFieldDidTapNext(_ textField: TextField)
    func textFieldDidTapPrevious(_ textField: TextField)
}

@objc public protocol TextEndEditingDelegate: class {
    func didFinishEditing(_ textField: TextField, _ text: String)
    @objc optional func didTextChange(_ textField: TextField)
}

@IBDesignable
open class TextField: UITextField, Validatable {
    
    override weak open var delegate: UITextFieldDelegate? {
        didSet {
            // Keep track of the delegate that users of this class are setting
            // in order to be able to forward events to it as they come.
            if let delegate = delegate, delegate !== self {
                self.otherDelegate = delegate
                self.delegate = self
            }
        }
    }
    
    @IBInspectable public var isValid = true {
        didSet { updateBorder() }
    }
    
    private var borderLine: UIView?
    @IBInspectable public var materialStyle = true {
        didSet { setBottomBorderIfNeeded() }
    }
    
    open var color: UIColor? = UIColor.mainThemeBlack {
        didSet { self.textColor = color }
    }
    
    open var fontSize: CGFloat = 17 {
        didSet { self.font = .libraryFont(ofSize: fontSize, weight: .light) }
    }
    
    /// - description: for text and placeholder
    open var fontFamily: String?
    
    open var borderOnEditing: Bool = true
    open var borderColor: UIColor { return .mainTheme }
    open var borderWidth: CGFloat = 0 {
        didSet { updateBorder() }
    }
    
    public weak var textDelegate: TextEndEditingDelegate?
    
    public var identifier: String?
    public var maximumCharactersCount: Int?
    public var allowedCharacterSet: CharacterSet?
    public var automaticallyLosesFocusOnMaximumCharactersCount = true
    
    public var textEdgeInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10) {
        didSet { setNeedsLayout() }
    }
    
    public var rightViewEdgeInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10) {
        didSet { setNeedsLayout() }
    }
    
    // accessoryViewDelegate
    public weak var selectionDelegate: TextFieldSelectionDelegate?
    public weak var toolbarDelegate: TextFieldToolBarDelegate?
    public var automaticallyChangeFocusForTableViewTextFieldOnConfirmButton: Bool = true
    
    public var charactersCount: Int {
        return text?.count ?? 0
    }
    
    private weak var otherDelegate: UITextFieldDelegate?
    
    /// - description: Value for restore "Cancel" button on toolbar
    private var prevouslyValue: String?
    
    @IBInspectable public var placeholderColorStyle: UIColor?
    @IBInspectable public var textColorStyle: UIColor? {
        didSet { self.color = self.textColorStyle }
    }
    
    open var placeholderColor: UIColor { return placeholderColorStyle ?? .grayPalette(heavy: .medium) }
    open var placeholderFont: UIFont { return .libraryFont(ofSize: fontSize, weight: .light) }
    open var placeholderAttributes: [NSAttributedString.Key: Any] {
        return [.foregroundColor: placeholderColor, .font: placeholderFont]
    }
    
    // Toolbar Value
    public var toolbarIsEnable: Bool = true {
        didSet { self.reloadToolbar() }
    }
    public var toolbarLeftTitle: String = "CANCEL".localizedInternal {
        didSet { self.reloadToolbar() }
    }
    public var toolbarRightTitle: String = "DONE".localizedInternal {
        didSet { self.reloadToolbar() }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.setUp()
        self.reloadToolbar()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setUp()
        self.reloadToolbar()
    }
    
    deinit { NotificationCenter.default.removeObserver(self) }
    
    private func setUp() {
        delegate = self
        borderStyle = .none
        layer.borderColor = borderColor.cgColor
        textColor = color
        backgroundColor = UIColor.grayPalette(heavy: .light)
        font = .libraryFont(ofSize: fontSize, weight: .light)
        
        layer.borderWidth = borderWidth
        rounded()
        layer.masksToBounds = true
        
        addTarget(self, action: #selector(TextField.editingDidBegin(_:)), for: .editingDidBegin)
        addTarget(self, action: #selector(TextField.editingDidEnd(_:)), for: .editingDidEnd)
        addTarget(self, action: #selector(TextField.editingChanged(_:)), for: .editingChanged)
        addTarget(self, action: #selector(TextField._editingOrValidationChanged), for: .editingDidBegin)
        addTarget(self, action: #selector(TextField._editingOrValidationChanged), for: .editingDidEnd)
        
        rightViewMode = .whileEditing
        
        materialStyle = false
    }
    
    @objc private func editingChanged(_ sender: TextField) {
        self.isValid = true
        if automaticallyLosesFocusOnMaximumCharactersCount, let maximumCharactersCount = maximumCharactersCount, charactersCount >= maximumCharactersCount {
            sender.resignFirstResponder()
            UIAccessibility.post(notification: UIAccessibility.Notification.screenChanged, argument: sender)
        }
        self.textDelegate?.didTextChange?(sender)
    }
    
    @objc private func editingDidBegin(_ sender: TextField) {
        if self.borderOnEditing { self.borderWidth = 2 }
        self.layoutIfNeeded()
        UIAccessibility.post(notification: UIAccessibility.Notification.screenChanged, argument: sender)
    }
    
    @objc private func editingDidEnd(_ sender: TextField) {
        // Prevents a glitch where text bounces horizontally for no apparent
        // reason when editing of a textfield ends and another textfield becomes
        // first responder.
        if self.borderOnEditing { self.borderWidth = 0 }
        self.textDelegate?.didFinishEditing(sender, sender.text ?? "")
        self.layoutIfNeeded()
        UIAccessibility.post(notification: UIAccessibility.Notification.screenChanged, argument: sender)
    }
    
    @objc private func _editingOrValidationChanged() {}
    
//    override var intrinsicContentSize: CGSize {
//        return CGSize(width: 120, height: 50)
//    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return super.textRect(forBounds: bounds).inset(by: textEdgeInsets)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
    override open func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.rightViewRect(forBounds: bounds)
        rect.origin.x -= rightViewEdgeInsets.right
        rect.origin.y -= rightViewEdgeInsets.top
        return rect
    }
    
    override open func drawPlaceholder(in rect: CGRect) {
        let inset = (rect.height - placeholderFont.lineHeight) / 2
        let insets = UIEdgeInsets(top: inset, left: 0, bottom: inset, right: 0)
        let insetRect = rect.inset(by: insets)
        placeholder?.draw(in: insetRect, withAttributes: placeholderAttributes)
    }
    
    private func updateBorder() {
        if isValid {
            if let borderView = self.borderLine {
                borderView.backgroundColor = borderColor
            } else {
                self.layer.borderWidth = borderWidth
                self.layer.borderColor = borderColor.cgColor
            }
        } else {
            if let borderView = self.borderLine {
                borderView.backgroundColor = UIColor.mainThemeRed
            } else {
                self.layer.borderWidth = borderWidth == 0 ? 1 : borderWidth
                self.layer.borderColor = UIColor.mainThemeRed.cgColor
            }
        }
    }
}

public extension TextField {
    
    fileprivate func setPasswordToggleImage(_ button: UIButton) {
        if isSecureTextEntry {
            button.setImage(Self.showPasswordImage, for: .normal)
        } else {
            button.setImage(Self.hidePasswordImage, for: .normal)
        }
    }
    
    private static var showPasswordImage: UIImage = .init()
    private static var hidePasswordImage: UIImage = .init()

    func enablePasswordToggle(with showImage: UIImage, hideImage: UIImage) {
        Self.showPasswordImage = showImage
        Self.hidePasswordImage = hideImage
        let button = UIButton(type: .custom)
        setPasswordToggleImage(button)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(self.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        button.addTarget(self, action: #selector(self.togglePasswordView), for: .touchUpInside)
        self.rightView = button
        self.rightViewMode = .always
    }
    
    @objc internal func togglePasswordView(_ sender: Any) {
        self.isSecureTextEntry = !self.isSecureTextEntry
        self.setPasswordToggleImage(sender as! UIButton)
    }
}

// In order to be notified of some delegate methods and implement useful
// behavior like `var allowedCharacterSet` and ` maximumCharactersCount`, all
// delegate callbacks are first intercepted and then forwarded to the delegate
// set by the user of this class (aka `otherDelegate`).
extension TextField: UITextFieldDelegate {
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return self.otherDelegate?.textFieldShouldEndEditing?(textField) ?? true
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        self.otherDelegate?.textFieldDidBeginEditing?(textField)
        self.prevouslyValue = text
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return otherDelegate?.textFieldShouldEndEditing?(textField) ?? true
    }

    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.otherDelegate?.textFieldDidEndEditing?(textField)
        self.prevouslyValue = nil
    }

    @available(iOS 10.0, *)
    public func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        self.otherDelegate?.textFieldDidEndEditing?(textField, reason: reason)
    }

    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // Prevent disallowed characters
        if let allowedCharacterSet = allowedCharacterSet, string.rangeOfCharacter(from: allowedCharacterSet.inverted) != nil {
            return false
        }
        
        // Prevent insertion of a numer of characters that exceeds the specified
        // `maximumCharactersCount`.
        if let maximumCharactersCount = maximumCharactersCount, let updatedString = textField.text?.replaceString(in: range, with: string), updatedString.count > maximumCharactersCount {
            return false
        }
        
        return true
    }

    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return otherDelegate?.textFieldShouldClear?(textField) ?? true
    }

    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return otherDelegate?.textFieldShouldReturn?(textField) ?? true
    }
    
    /// this function try to change focus for next textfield in tableView or inside one view only
    func automaticallyChangeFocusForNextTextField() {
        var textfieldView: UIView = self
        repeat {
            if let superview = textfieldView.superview {
                textfieldView = superview
            } else {
                break
            }
        } while !(textfieldView is UITableViewCell) || textfieldView.superview == nil
        guard let cell = textfieldView as? UITableViewCell,
              let indexPath = cell.tableView?.indexPath(for: cell) else {
            return automaticallyChangeFocusForNextTextFieldInView()
        }
        if cell.tableView?.isLastIndexPath(indexPath: indexPath) == false {
            if let nextCell = cell.tableView?.cellForRow(at: IndexPath(row: indexPath.row+1, section: indexPath.section)) {
                DispatchQueue.main.async {
//                    self.endEditing(true)
                    guard let nextTextField = nextCell.firstViewOfType(for: UITextField.self) else {
                        return self.automaticallyChangeFocusForNextTextFieldInView()
                    }
                    nextTextField.becomeFirstResponder()
                }
            }
        }
    }
}

public extension UITextField {
    /// this function try fo change focus for next textField not in a Tableview
    func automaticallyChangeFocusForNextTextFieldInView() {
        guard let superView = self.superview else { self.endEditing(true); return }
        var textFields: [UITextField] = .init()
        for view in superView.subviews {
            if let textField = view as? UITextField {
                textFields.append(textField)
            }
        }
        if let index = textFields.firstIndex(of: self), let next = textFields[safe: index+1] {
            DispatchQueue.main.async {
                next.becomeFirstResponder()
            }
        } else {
            self.endEditing(true)
        }
    }
}

extension UIEdgeInsets {
    var vertical: CGFloat   { return top + bottom }
    var horizontal: CGFloat { return left + right }
}

public extension TextField {
    enum Error: Swift.Error {
        case invalidLength
        case missingField
        
        public var localizedDescription: String {
            switch self {
            case .missingField:
                return "Campo obbligatorio: compila per proseguire"
            case .invalidLength:
                return "Formato non corretto"
            }
        }
    }
}

public protocol Validatable {
    var isValid: Bool { get set }
}

public extension Validatable {
    mutating func clear() { isValid = true }
}

// MARK: Toolbar
public protocol TextFieldToolBarDelegate: class {
    func doneDidTapped(_ textField: TextField, text: String?)
    func cancelDidTapped(_ textField: TextField, currentText: String?, restoredValue: String?)
}

public extension TextField {
    
    private func reloadToolbar() {
        if self.toolbarIsEnable {
            self.addToolBar()
        } else {
            self.removeToolbar()
        }
    }
    
    private func addToolBar() {
        self.autocorrectionType = .no // this fix a constraint problem of toolbar
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
//        toolBar.tintColor = UIColor.mainTheme
        let doneButton = UIBarButtonItem(title: self.toolbarRightTitle, style: .done, target: self, action: #selector(donePressed))
        let cancelButton = UIBarButtonItem(title: self.toolbarLeftTitle, style: .plain, target: self, action: #selector(cancelPressed))
        doneButton.tintColor = UIColor.mainTheme
        cancelButton.tintColor = UIColor.mainTheme
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.sizeToFit()
        self.inputAccessoryView = toolBar
        self.updateConstraintsWhenCustomKeyboardIsShown(customInputView: toolBar)
    }
    
    private func removeToolbar() {
        self.inputAccessoryView = nil
    }
    
    private func updateConstraintsWhenCustomKeyboardIsShown(customInputView: UIView) {
        if let superview = customInputView.superview {
            for constraint in superview.constraints {
                if (constraint.secondItem === customInputView && constraint.secondAttribute == .top) {
                    constraint.priority = UILayoutPriority(999)
                }
            }
        }
    }
    
    @objc internal func donePressed() {
        self.toolbarDelegate?.doneDidTapped(self, text: self.text)
        if self.automaticallyChangeFocusForTableViewTextFieldOnConfirmButton {
            self.automaticallyChangeFocusForNextTextField()
        } else {
            self.endEditing(true)
        }
    }
    
    @objc internal func cancelPressed() {
        if let _toolbarDelegate = self.toolbarDelegate {
            _toolbarDelegate.cancelDidTapped(self, currentText: self.text, restoredValue: self.prevouslyValue)
        } else {
            self.text = self.prevouslyValue
        }
        self.endEditing(true)
    }
}

private extension TextField {
    func setBottomBorderIfNeeded() {
        if self.materialStyle {
            let borderLineView = UIView(frame: CGRect(x: 0, y: self.frame.height - 1, width: self.frame.width, height: 1))
            borderLineView.backgroundColor = self.borderColor
            self.borderLine = borderLineView
            self.addSubview(borderLineView)
        } else {
            self.borderLine?.removeFromSuperview()
        }
    }
}
