//  Created by Stefano Pedroli on 16/04/2020.

import UIKit

public protocol LinearSegmentedControlDelegate:class {
    func changeToIndex(index:Int)
}

public final class LinearSegmentedControl: UIView {
    
    private var buttonTitles: [String]!
    private var buttons: [UIButton]!
    private var selectorView: UIView!
    private var stackView: UIStackView!
    
    public var textColor: UIColor = .grayPalette(heavy: .normal)
    public var selectorViewColor: UIColor = .mainThemeBlack
    public var selectorTextColor: UIColor = .mainTheme
    public var normalFont: UIFont = .libraryFont(ofSize: 19)
    public var selectedFont: UIFont = .libraryFont(ofSize: 19, weight: .medium)
    
    public weak var delegate: LinearSegmentedControlDelegate?
    
    public private(set) var selectedIndex : Int = 0
    
    public convenience init(frame: CGRect, buttonTitle: [String]) {
        self.init(frame: frame)
        self.buttonTitles = buttonTitle
    }
    
    public override func draw(_ rect: CGRect) {
        super.draw(rect)
        updateView()
    }

    public func config(buttonTitles: [String], textColor: UIColor = .grayPalette(heavy: .normal), selectedTextColor: UIColor = .mainThemeBlack, separatorColor: UIColor = .mainTheme, initiallyIndex: Int = 0) {
        self.selectedIndex = initiallyIndex
        self.buttonTitles = buttonTitles
        self.textColor = textColor
        self.selectorTextColor = selectedTextColor
        self.selectorViewColor = separatorColor
        self.updateView()
    }
    
    public func setIndex(index: Int) {
        buttons.forEach({ $0.setTitleColor(textColor, for: .normal) })
        let button = buttons[index]
        selectedIndex = index
        button.setTitleColor(selectorTextColor, for: .normal)
        button.titleLabel?.font = selectedFont
        let selectorPosition = frame.width/CGFloat(buttonTitles.count) * CGFloat(index)
        UIView.animate(withDuration: 0.2) {
            self.selectorView.frame.origin.x = selectorPosition
        }
    }
    
    @objc func buttonAction(sender: UIButton) {
        for (buttonIndex, btn) in buttons.enumerated() {
            btn.setTitleColor(textColor, for: .normal)
            btn.titleLabel?.font = normalFont
            if btn == sender {
                let selectorPosition = frame.width/CGFloat(buttonTitles.count) * CGFloat(buttonIndex)
                selectedIndex = buttonIndex
                delegate?.changeToIndex(index: selectedIndex)
                UIView.animate(withDuration: 0.3) {
                    self.selectorView.frame.origin.x = selectorPosition
                }
                btn.setTitleColor(selectorTextColor, for: .normal)
                btn.titleLabel?.font = selectedFont
            }
        }
    }
}

//Configuration View
extension LinearSegmentedControl {
    
    private func updateView() {
        createButton()
        configStackView()
        configSelectorView()
    }
    
    private func configStackView() {
        let stack = UIStackView(arrangedSubviews: buttons)
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fillEqually
        stack.spacing = 20
        addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stack.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        stack.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        self.stackView = stack
//        stack.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
    }
    
    private func configSelectorView() {
        DispatchQueue.main.async {
            let selectorWidth = self.stackView.frame.width / CGFloat(self.buttonTitles.count)
            let selectorPosition = self.stackView.frame.width/CGFloat(self.buttonTitles.count) * CGFloat(self.selectedIndex)
            self.selectorView = UIView(frame: CGRect(x: selectorPosition, y: self.frame.height, width: selectorWidth, height: 2))
            let separatorView = UIView(frame: CGRect(x: 0, y: self.frame.height, width: self.frame.width, height: 2))
            separatorView.backgroundColor = .grayPalette(heavy: .light)
            self.addSubview(separatorView)
            self.selectorView.backgroundColor = self.selectorViewColor
            self.addSubview(self.selectorView)
        }
    }
    
    private func createButton() {
        buttons = [UIButton]()
        buttons.removeAll()
        subviews.forEach({$0.removeFromSuperview()})
        for buttonTitle in buttonTitles {
            let button = UIButton(type: .system)
            button.setTitle(buttonTitle, for: .normal)
            button.addTarget(self, action:#selector(LinearSegmentedControl.buttonAction(sender:)), for: .touchUpInside)
            button.setTitleColor(textColor, for: .normal)
            button.titleLabel?.font = self.normalFont
            buttons.append(button)
        }
        buttons[selectedIndex].setTitleColor(selectorTextColor, for: .normal)
        buttons[selectedIndex].titleLabel?.font = self.selectedFont
    }
}
