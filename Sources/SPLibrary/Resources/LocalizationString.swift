//
//  File.swift
//  
//
//  Created by Stefano Pedroli on 14/01/2020.
//

import Foundation

public enum Localized: String {
    
    case NO_CONNECTION
    case NO_CONNECTION_DESCRIPTION
    case NOT_AUTHORIZED
    case NOT_AUTHORIZED_DESCRIPTION
    case RESOURCES_NOT_FOUND
    case RESOURCES_NOT_FOUND_DESCRIPTION
    case GENERIC_ERROR_DESCRIPTION
    case PARSING_DATA_ERROR
    case PATH_ERROR
    case WRAPPING_PAYLOAD_ERROR
    case LOADING
    case ERROR
    
    case DONE, CANCEL
    
    var it: String {
        switch self {
        case .ERROR: return "Errore"
        case .NO_CONNECTION: return "Nessuna connessione"
        case .NO_CONNECTION_DESCRIPTION: return "Controllare la connessione ad internet."
        case .NOT_AUTHORIZED: return "Non sei autorizzato"
        case .NOT_AUTHORIZED_DESCRIPTION: return "Sembra che tu non sia autorizzato ad effettuare questa richiesta."
        case .RESOURCES_NOT_FOUND: return "Risorsa non raggiungibile"
        case .RESOURCES_NOT_FOUND_DESCRIPTION: return "La tua richiesta non può essere processata."
        case .GENERIC_ERROR_DESCRIPTION: return "Si è verificato un errore inatteso, si prega di riprovare più tardi."
        case .PARSING_DATA_ERROR: return "C'è stato un errore nel recuperare correttamente i dati, si prega di riprovare."
        case .WRAPPING_PAYLOAD_ERROR: return "C'è stato un'errore nel trovare il payload"
        case .LOADING: return "Caricamento"
        case .DONE: return "Conferma"
        case .CANCEL: return "Annulla"
        case .PATH_ERROR: return "Il path inserito non è un url valido"
        }
    }
    
    var en: String {
        switch self {
        case .ERROR: return "Error"
        case .NO_CONNECTION: return "Connection not found"
        case .NO_CONNECTION_DESCRIPTION: return "Check the internet connection."
        case .NOT_AUTHORIZED: return "Unauthorized"
        case .NOT_AUTHORIZED_DESCRIPTION: return "You are not authorized"
        case .RESOURCES_NOT_FOUND: return "Resource not found"
        case .RESOURCES_NOT_FOUND_DESCRIPTION: return "Your request cannot be processed."
        case .GENERIC_ERROR_DESCRIPTION: return "Unexpected error. Try again later."
        case .PARSING_DATA_ERROR: return "An error has occurred while retrieving data. Try again later."
        case .WRAPPING_PAYLOAD_ERROR: return "There was an error wrapping payload"
        case .LOADING: return "Loading"
        case .DONE: return "Done"
        case .CANCEL: return "Cancel"
        case .PATH_ERROR: return "The insert path is not a valid URL"
        }
    }
    
    // tradotto con google translate
    var es: String {
        switch self {
         case .ERROR: return "Error"
         case .NO_CONNECTION: return "Conexión no encontrada"
         case .NO_CONNECTION_DESCRIPTION: return "Compruebe la conexión a Internet"
         case .NOT_AUTHORIZED: return "No autorizado"
         case .NOT_AUTHORIZED_DESCRIPTION: return "No estás autorizado"
         case .RESOURCES_NOT_FOUND: return "Recurso no encontrado"
         case .RESOURCES_NOT_FOUND_DESCRIPTION: return "Su solicitud no puede ser procesada"
         case .GENERIC_ERROR_DESCRIPTION: return "Error inesperado. Vuelve a intentarlo más tarde"
         case .PARSING_DATA_ERROR: return "Se ha producido un error al recuperar los datos. Vuelve a intentarlo más tarde"
         case .WRAPPING_PAYLOAD_ERROR: return "Hubo un error al empaquetar la carga útil"
         case .LOADING: return "Cargando"
         case .DONE: return "Done"
         case .CANCEL: return "Cancelar"
         case .PATH_ERROR: return "La ruta de inserción no es una URL válida"
         }
     }

    static func getLocalization(for identifier: String) -> String {
        guard let type = Localized.init(rawValue: identifier) else { return identifier }
        switch NSLocale.language {
        case "it": return type.it
        case "es": return type.es
        default: return type.en
        }
    }
}
