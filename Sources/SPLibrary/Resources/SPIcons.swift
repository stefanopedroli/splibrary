//  Created by Stefano Pedroli on 03/11/2020.

import UIKit

public enum SPIcon: StringLiteralType {
    case sp_back
    case sp_close
    case sp_settings
    case sp_camera
    case sp_check
    case sp_edit
    case sp_eye
    case sp_gallery
    case sp_notification
    case sp_share
    case sp_warning
    case sp_trash
    case sp_checkbox_on
}

public extension UIImage {
    static func getIcon(sp_type: SPIcon) -> UIImage {
        return self.getIcon(withType: sp_type)
    }
}
