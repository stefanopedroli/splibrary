//  Created by Stefano Pedroli on 10/06/2020.

import UIKit
    
public struct File {
    
    static var fileDateFormat: String = "yyyyMMddHHmmss"
    static var kMaximumByte: UInt64 = 3000000 // bytes
    
    /// automatically initialize and create the file with default name (id + timestamp) and with the device information already written in
    static func initialize(name: String? = nil, fileExtension: String = "txt") -> File? {
        guard var newFile = File(name: name, fileExtension: fileExtension) else { return nil }
        newFile.cleanFile()
        return newFile
    }
    
    public let path: String
    public let url: URL
    
    /// initialize new file where his name is composed by an identifier (ex: "logger") and a timestamp
    /// - attention: his extension is .txt
    private init?(name: String? = nil, fileExtension: String = "txt") {
        guard let fileUrl = FileManager.getNewPathFile(name: name, fileExtension: fileExtension) else {
            return nil
        }
        path = fileUrl.path
        url = fileUrl
    }
    
    /// init file from url
    public init(url: URL) {
        self.path = url.path
        self.url = url
    }
    
    /// you need to sign file after that or you never log all device informations
    private mutating func cleanFile() {
        try? self.write(content: NSString())
        isEmpty = true
    }
    
    /// return *true* if the current file is created
    /// - attention: You can have the path or the url but the file may not exist: maybe beacause there isn't directory, or beacause was never created
    public var exist: Bool {
        let value = FileManager.default.fileExists(atPath: path)
        return value
    }
    
    /// name of the file
    public var name: String {
        return url.lastPathComponent
    }
    
    public func data() -> Data {
        return (try? Data(contentsOf: self.url)) ?? Data()
    }
    
    /// return *true* if file have no logs
    /// - attention: is considered "empty" also if has only headers
    public var isEmpty: Bool = true
    
    /// all content of the file
    public var content: String {
        guard let content = try? String(contentsOfFile: path) else { return "" }
        return content
    }
    
    /// function called only if you want to log something
    /// - warning: if writing goes on success the current var **isEmpty** become *false*
    public mutating func log(content: NSString) {
        isEmpty = !((try? write(content: content)) != nil)
    }
    
    /// function that write into the file
    /// - attention:
    public func write(text: String) throws {
        return try self.write(content: NSString(string: text))
    }
    
    /// function that write into the file
    /// - attention: if there is already some written text this function will try to write the new text **after** that
    public func write(content: NSString) throws {
        FileManager.default.createDirectoryIfNeeded()
        do {
            if let fileHandle = FileHandle(forWritingAtPath: path),
                let data = content.data(using: String.Encoding.utf8.rawValue) {
                fileHandle.seekToEndOfFile()
                fileHandle.write(data)
                fileHandle.closeFile()
            } else {
                try content.write(to: URL(fileURLWithPath: path), atomically: true, encoding: String.Encoding.utf8.rawValue)
            }
        } catch {
            printFileError(error: Error.writeFileError(name: self.path, reason: error))
            throw error
        }
    }
    
    /// this function will try to delete the file
    /// - attention: If the file not exist the function will do nothing
    public func delete() {
        if !self.exist { return }
        do {
            try FileManager.default.removeItem(atPath: self.path)
        } catch let error {
            printFileError(error: Error.writeFileError(name: self.path, reason: error))
        }
    }
    
    /// return the size of the file in bytes
    /// - attention: You can normalize it calling .normalize to see the value in correct size format
    public  var size: UInt64 {
        do {
            let fileAttributes = try FileManager.default.attributesOfItem(atPath: path)
            if let fileSize = fileAttributes[FileAttributeKey.size],
                let value = (fileSize as? NSNumber)?.uint64Value {
                return value
            } else {
                print("Failed to get a size attribute from path: \(path)")
            }
        } catch {
            print("Failed to get file attributes for local path: \(path) with error: \(error)")
        }
        return 0
    }
    
    /// function that check if the size of the file exceeded the maximum size
    /// - returns: *true* if is oversize
    public func overSize() -> Bool {
        let _size = size
        if _size > File.kMaximumByte {
            printFileError(error: Error.maximumSizeReachError(dimension: _size.normalized))
            return true
        }
        return false
    }
}

extension File: Equatable {
    public static func ==(lhs: File, rhs: File) -> Bool {
        return lhs.name == rhs.name && lhs.path == rhs.path
    }
}

public extension Array where Element == File {
    /// function that will try to delete all the file of an array of File
    func deleteAll() {
        for file in self {
            file.delete()
        }
    }
}

public extension FileManager {
    
    static func getNewPathFile(name: String? = nil, fileExtension: String = "txt") -> URL? {
        let timestamp = DateFormatter.fileFormatter.string(from: Date())
        let directory = FileManager.documentsDirectoryPath
        if let name = name {
            return directory.appendingPathComponent("\(name)_\(timestamp).\(fileExtension)", isDirectory: false)
        } else {
            return directory.appendingPathComponent("\(timestamp).\(fileExtension)", isDirectory: false)
        }
    }
    
    static var documentsDirectoryPath: URL {
        return FileManager.default.temporaryDirectory.appendingPathComponent(Bundle.main.bundleIdentifier!, isDirectory: true)
    }
    
    func createDirectoryIfNeeded() {
        let manager = FileManager.default
        let directory = FileManager.documentsDirectoryPath
        if !manager.fileExists(atPath: directory.path) {
            do {
                try manager.createDirectory(at: directory, withIntermediateDirectories: true, attributes: nil)
            } catch {
                printFileError(error: File.Error.createDirectoryError(error: error))
            }
        }
    }
    
    /// it take all files in the sdk custom folder only
    /// - attention: return also files that are empty
    static func allFiles() -> [File] {
        guard let subpaths = FileManager.default.subpaths(atPath: FileManager.documentsDirectoryPath.path) else {
            return []
        }
        let urls = subpaths.map { relativePath in
            FileManager.documentsDirectoryPath.appendingPathComponent(relativePath)
        }
        
        var files: [File] = []
        for url in urls {
            files.append(File(url: url))
        }
        return files
    }
    
   @discardableResult
    func saveImage(image: UIImage, url: URL) -> Bool  {
        var imageData: Data?
        if url.type == .png {
            imageData = image.pngData()
        } else {
            imageData = image.jpegData(compressionQuality: 1)
        }
        guard let data = imageData else {
            return false
        }
        let name = url.lastPathComponent
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return false
        }
        do {
            try data.write(to: directory.appendingPathComponent(name)!)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    
    func retrieveImage(url: URL) -> UIImage? {
        let name = url.lastPathComponent
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(name).path)
        }
        return nil
    }
    
    func clearCacheFiles() {
        do {
            let myDocuments = self.urls(for: .documentDirectory, in: .userDomainMask).first!
            try self.removeItem(at: myDocuments)
        } catch {
            return print(error.localizedDescription)
        }
    }
}

extension DateFormatter {
    static let fileFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = File.fileDateFormat
        return formatter
    }()
}

// MARK: Errors
public extension File {
    enum Error: Swift.Error {
        case copyFileError(error: Swift.Error)
        case fileNotFound(url: URL)
        case writeFileError(name: String, reason: Swift.Error)
        case deleteFileError(name: String, reason: Swift.Error)
        case createDirectoryError(error: Swift.Error)
        case maximumSizeReachError(dimension: String)
        
        var localizedDescription: String {
            switch self {
            case .copyFileError(let reason): return "Copia del file fallita causa: \(reason.localizedDescription)"
            case .fileNotFound(let path): return "Il file \"\(path.lastPathComponent)\" (\(path.path)) non è stato trovato"
            case .writeFileError(let name, let reason): return "La scrittura sul file (\"\(name)\") non è andata a buon fine a causa: \(reason.localizedDescription)"
            case .deleteFileError(let name, let reason): return "Cancellazione del file (\"\(name)\") non riuscita a causa di: \(reason.localizedDescription)"
            case .createDirectoryError(let reason): return "C'è stato un errore nella creazione della cartella principale a causa di: \(reason.localizedDescription)"
            case .maximumSizeReachError(let dimension): return "🔋 Il file ha raggiunto il limite massimo di dimensioni \(dimension)/\(File.kMaximumByte.normalized)"
            }
        }
    }
}

public extension UInt64 {
    /// convert UIInt64 of only bytes in the correct format
    var normalized: String {
        var convertedValue: Double = Double(self)
        var multiplyFactor = 0
        let tokens = ["bytes", "KB", "MB", "GB", "TB", "PB",  "EB",  "ZB", "YB"]
        while convertedValue > 1024 {
            convertedValue /= 1024
            multiplyFactor += 1
        }
        return String(format: "%4.2f %@", convertedValue, tokens[multiplyFactor])
    }
}

internal func printFileError(error: Swift.Error) {
    #if DEBUG
    switch error {
    case let loggerError as File.Error:
        print("⚠️ \(loggerError.localizedDescription)")
    default: print("⚠️ \(error.localizedDescription)")
    }
    #endif
}
