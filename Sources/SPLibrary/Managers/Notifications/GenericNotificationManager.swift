//  Created by Stefano Pedroli on 30/11/2019.

import Foundation
import UIKit
import UserNotifications

public typealias PermissionHandler = ((_ granted: Bool)->Void)

/// - description: The manager class that contain all methods needed for reading notification (parser, typology)
public final class GenericNotificationManager<PushNotificationType: Decodable & RawRepresentable> where PushNotificationType.RawValue == String {
    
    public typealias GenericPushNotification = PayloadPushNotification<DecodableVoid, PushNotificationType>
    
    public static func getNofiticationType(_ userInfo: [AnyHashable: Any], alertBody: String? = nil) -> PushNotificationType? {
        print(userInfo)
        if
            let notificationData = userInfo[dataKey] as? NSDictionary,
            let serviceId = notificationData["service_id"] as? String,
            let serviceType = PushNotificationType.init(rawValue: serviceId) {
            return serviceType
        } else if let text = userInfo[dataKey] as? String,
            let data = text.data(using: String.Encoding.utf8),
            let dictionary = try? JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary,
            let serviceId = dictionary["service_id"] as? String,
            let serviceType = PushNotificationType.init(rawValue: serviceId) {
            return serviceType
        } else {
            return nil
        }
    }
    
    public static func handleTypeNotification(_ userInfo: [AnyHashable: Any], customPayload: (_ type: PushNotificationType) -> Swift.Void, emptyPayload: VoidHandler) {
        if let type = getNofiticationType(userInfo) {
            customPayload(type)
        } else {
            emptyPayload()
        }
    }
    
    public static func parseNotification<T: Decodable>(myClass: T.Type, _ userInfo: [AnyHashable: Any], alertBody: String? = nil) -> PayloadPushNotification<T, PushNotificationType>? {
        if let data = try? JSONSerialization.data(withJSONObject: userInfo, options: .prettyPrinted) {
            do {
                let notification = try JSONDecoder().decode(PayloadPushNotification<T, PushNotificationType>.self, from: data)
                return notification
            } catch let error {
                print("❌ [Parsing notification error]: " + error.localizedDescription)
            }
        }
        return nil
    }
    
    public static func parseGenericNotification(_ userInfo: [AnyHashable: Any], alertBody: String? = nil) -> GenericPushNotification? {
        if let data = try? JSONSerialization.data(withJSONObject: userInfo, options: .prettyPrinted) {
            do {
                let notification = try JSONDecoder().decode(GenericPushNotification.self, from: data)
                return notification
            } catch let error {
                print("❌ [Parsing notification error]: " + error.localizedDescription)
            }
        }
        return nil
    }
}

public extension Data {
    func getCleanToken() -> String {
        return map { String(format: "%02.2hhx", arguments: [$0]) }.joined()
    }
}
