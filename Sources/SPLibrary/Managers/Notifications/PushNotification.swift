//  Created by Stefano Pedroli on 30/11/2019.

import Foundation
import UIKit

//public typealias GenericPushNotification = PushNotification<DecodableVoid, SPPushNotificationType>

var dataKey: String { return "data" }

public class PushNotification: Decodable {
    
    public var sound: String?
    public var alert: PushAlert?
    public var badge: Int?
    public var content_avaiable: BooleanBit?
    
    public var title: String? {
        return alert?.title
    }
    public var body: String? {
        return alert?.body
    }
    
    public enum CodingKeys: String, CodingKey {
        case sound, alert, badge, content_avaiable = "content-avaiable"
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        sound = try? container.decode(String.self, forKey: .sound)
        alert = try? container.decode(PushAlert.self, forKey: .alert)
        badge = try? container.decode(Int.self, forKey: .badge)
        content_avaiable = try? container.decode(BooleanBit.self, forKey: .content_avaiable)
    }
}

public class PayloadPushNotification<T, PushNotificationType: Decodable>: Decodable where T: Decodable {
    
    public var aps: PushNotification
    public var data: PushData<T, PushNotificationType>
    
    public var payload: T {
        return data.payload
    }
    
    public enum CodingKeys: String, CodingKey {
        case aps, data
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.aps = try container.decode(PushNotification.self, forKey: .aps)
        if let object = try? container.decode(PushData<T, PushNotificationType>.self, forKey: .data) {
            self.data = object
        } else {
            let stringData = try container.decode(String.self, forKey: .data)
            guard let data = stringData.data(using: String.Encoding.utf8) else { throw PushNotification.Error.encodingUTF8Error }
            let dictionary = try JSONDecoder().decode(PushData<T, PushNotificationType>.self, from: data)
            self.data = dictionary
        }
    }
}

public extension PushNotification {
    enum Error: Swift.Error {
        case encodingUTF8Error
        
        var localizedDescription: String {
            switch self {
            case .encodingUTF8Error: return "Error decoding String to data using UTF8"
            }
        }
    }
}

public struct PushAlert: Decodable {
    public var body: String?
    public var title: String?
}

public struct PushData<T, PushNotificationType: Decodable>: Decodable where T: Decodable {
    
    public var service_id: PushNotificationType
    public var payload: T
    public var notification_id: String?
    
    public enum CodingKeys: String, CodingKey {
        case service_id, payload, notification_id
    }
    
    internal init(service_id: PushNotificationType, payload: T, notification_id: String?) {
        self.service_id = service_id
        self.payload = payload
        self.notification_id = notification_id
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.service_id = try container.decode(PushNotificationType.self, forKey: .service_id)
        self.notification_id = try container.decodeIfPresent(String.self, forKey: .notification_id)
        if let object = try? container.decode(T.self, forKey: .payload) {
            self.payload = object
        } else {
            let stringData = try container.decode(String.self, forKey: .payload)
            guard let data = stringData.data(using: String.Encoding.utf8) else { throw PushNotification.Error.encodingUTF8Error }
            let dictionary = try JSONDecoder().decode(T.self, from: data)
            self.payload = dictionary
        }
    }
}
