//  Created by Stefano Pedroli on 01/02/21.

import UIKit

enum DeviceToken: API {
    
    static var mockName: String { return "deviceToken" }
    static var mockEnable: Bool { return false }
        
    static var authType: APIAuthType {
        return .authenticated
    }
    
    struct PathParameters {}
    
    static var method: HTTPMethodRequest {
        return .post
    }
    
    static func path(_ input: PathParameters?) -> String {
        return "/devices"
    }
    
    struct Input: Encodable {
        
        let device: Device
        
        struct Device: Codable {
            let id: String
            let token: String
            var extra_info: String = UIApplication.extra_info
            let user_id: String
        }
        
        init(token: String) {
            self.device = Device(id: String(), token: token, user_id: String())
        }
    }
        
    struct Output: Decodable {
        let device: Input.Device
    }
}

enum DeleteToken: API {
    
    static var mockName: String { return "deleteToken" }
    static var mockEnable: Bool { return false }
        
    static var authType: APIAuthType {
        return .authenticated
    }
    
    struct PathParameters {
        let token: String
    }
    
    static var method: HTTPMethodRequest {
        return .delete
    }
    
    static func path(_ input: PathParameters?) -> String {
        return "/devices/\(input?.token ?? String())"
    }
    
    struct Input: Encodable {}
        
    typealias Output = DecodableVoid
}
