//  Created by Stefano Pedroli on 27/11/2019.

import UIKit

public final class CacheManager {
    
    // Setter UserDefaults
    private static let defaults: UserDefaults = .standard
    private static let sharedDefaults: UserDefaults = UserDefaults(suiteName: SPLibrary.configurations.sharedUserDefaultBundleName) ?? UserDefaults.standard
    // Only Getter UserDefaults
    private static let automaticallyDefaults: UserDefaults = {
        if Bundle.main.isAppExtension {
            return UserDefaults(suiteName: SPLibrary.configurations.sharedUserDefaultBundleName) ?? UserDefaults.standard
        } else {
            return UserDefaults.standard
        }
    }()
    
    public static func set<T: Encodable>(object: T?, for key: String, shared: Bool = false) {
        guard let object = object else {
            defaults.removeObject(forKey: key)
            if shared { sharedDefaults.removeObject(forKey: key) }
            return
        }
        if let encoded = try? JSONEncoder().encode(object) {
            defaults.set(encoded, forKey: key)
            defaults.synchronize()
            if shared {
                sharedDefaults.set(encoded, forKey: key)
                sharedDefaults.synchronize()
            }
        } else {
            defaults.set(object, forKey: key)
            defaults.synchronize()
            if shared {
                sharedDefaults.set(object, forKey: key)
                sharedDefaults.synchronize()
            }
        }
    }

    public static func get<T: Decodable>(type: T.Type, for key: String) -> T? {
        if #available(iOS 13, *) {
            if let tokenData = automaticallyDefaults.data(forKey: key),
                let object = try? JSONDecoder().decode(T.self, from: tokenData) {
                return object
            }
        } else {
            if let tokenData = automaticallyDefaults.object(forKey: key) as? Data,
                let object = try? JSONDecoder().decode(T.self, from: tokenData) {
                return object
            } else if let object = automaticallyDefaults.value(forKey: key) as? T {
                return object
            }
        }
        return nil
    }
    
    public static var cache: CacheManager = .init()
    
    private let imageCache = NSCache<NSString, UIImage>()
    private let informationsCache = NSCache<NSString, NSString>()
    
    public enum CacheType: CaseIterable {
        case image
        case text
            
        public init?(object: AnyObject) {
            switch object {
            case is UIImage: self = .image
            case is String: self = .text
            default: return nil
            }
        }
    }
    
    internal func getCache(type: CacheType) -> NSCache<NSString, AnyObject> {
        switch type {
        case .image: return imageCache as! NSCache<NSString, AnyObject>
        case .text: return informationsCache as! NSCache<NSString, AnyObject>
        }
    }
    
    internal func set(object: AnyObject, forKey: String) {
        if let type = CacheType(object: object) {
            getCache(type: type).setObject(object, forKey: NSString(string: forKey))
        }
    }
    
    internal func get(type: CacheType, key: String) -> AnyObject? {
        return getCache(type: type).object(forKey: NSString(string: key))
    }
    
    public func removeObject(id: String, from: CacheType) {
        let key = NSString(string: id)
        getCache(type: from).removeObject(forKey: key)
    }
    
    public func clearCache(for type: CacheType) {
        switch type {
        case .image: imageCache.removeAllObjects()
        case .text: informationsCache.removeAllObjects()
        }
    }
    
    public func clearAllCache() {
        for item in CacheType.allCases {
            clearCache(for: item)
        }
    }
}
